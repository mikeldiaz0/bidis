//
//  ValiDasBidiResult.h
//  VDLibrary
//
//  Copyright © 2020 BBVA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <VDLibrary/ValiDasBidiCode.h>
@import Vision;

NS_ASSUME_NONNULL_BEGIN

@interface ValiDasBidiResult : NSObject

@property(nonatomic, strong, nonnull) NSString *bidiString;
@property(nonatomic) BidiType bidiType;
//@property(nonatoic, strong) NSData *

-(id)initWithVision:(VNBarcodeObservation *)result API_AVAILABLE(ios(11.0));

@end

NS_ASSUME_NONNULL_END
