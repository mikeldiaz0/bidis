//
//  ViewController.swift
//  VDDocumentCaptureApp
//
//  Created by BBVA.
//
	
import VDDocumentCapture
import MessageUI

class ViewController: UIViewController , UIPickerViewDelegate, UIPickerViewDataSource{

    @IBOutlet var lbl_resultado: UILabel!
    @IBOutlet var lbl_documento: UILabel!
    @IBOutlet var lbl_bidis: UITextView!
    @IBOutlet var btn_sdk: UIButton!
    @IBOutlet var picker: UIPickerView!
    var pickerData: [String] = [String]()
    var codigos : NSMutableArray = []
    var tipos : NSMutableArray = []
    var documento: [[String]] = [[String]]()
    var selected:Int = 0
    
    @IBAction func startDocumentCapture(_ sender: Any) {
        var configuration : [String : String] = [:]
        configuration["closebutton"] = "YES"
        configuration["secondswithoutshutterbuttonobverse"] = "1"
        VDDocumentCapture.start(withDelegate: self, andDocumentIds: [VDDOCUMENT_ID_BG_IDCard_2006, VDDOCUMENT_ID_BG_IDCard_2010], andConfiguration: configuration)
    }
    
    @IBAction func startDocumentCapture1(_ sender: Any) {
        var configuration : [String : String] = [:]
        configuration["closebutton"] = "YES"
        VDDocumentCapture.start(withDelegate: self, andDocumentIds: [/*"XX_XXXX_XX"*/VDDOCUMENT_ID_XX_Passport_YYYY], andConfiguration: configuration)
    }
    
    @IBAction func startDocumentCapture2(_ sender: Any) {
        var configuration : [String : String] = [:]
        configuration["type of document"] = "Bulgaria"
        configuration["repeat"] = "YES"
        configuration["use button"] = "YES"
        configuration["infoalertshow"] = "NO"
        configuration["showdocument"] = "YES"
        configuration["obverseflash"] = "YES"
        configuration["shutterbuttonshow"] = "YES"
        configuration["closebutton"] = "YES"
        configuration["fixedrectangle"] = "NO"
        configuration["fixedtemplate"] = "YES"
        configuration["onlyobverse"] = "YES"
        
        VDDocumentCapture.start(withDelegate: self, andDocumentIds: [VDDOCUMENT_ID_US_WV_DrivingLicense_2005, VDDOCUMENT_ID_XX_XX_XXXX, VDDOCUMENT_ID_BG_IDCard_2010], andConfiguration: configuration)
    }
    
    @IBAction func startDocumentCapture3(_ sender: Any) {
        var configuration : [String : Any] = [:]
        configuration["closebutton"] = "YES"
        configuration["infoalertshow"] = "NO"
        configuration["showdocument"] = "YES"

        VDDocumentCapture.start(withDelegate: self, andDocumentIds: [
                                    VDDOCUMENT_ID_ES_IDCard_2021,
                                    VDDOCUMENT_ID_ES_IDCard_2015,
                                    VDDOCUMENT_ID_ES_IDCard_2006,
                                    VDDOCUMENT_ID_ES_ResidencePermit_2010,
                                    VDDOCUMENT_ID_ES_ResidencePermit_2011,
                                    VDDOCUMENT_ID_ES_ResidencePermit_2020
        ],
                                andConfiguration: configuration)
    }
    
    func setBidiConfig() -> [String : String] {
        var configuration : [String : String] = [:]
        configuration["closebutton"] = "YES"
        configuration["secondswithoutshutterbuttonobverse"] = "1"
        configuration["shutterbuttonshow"] = "YES"
        
        return configuration
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Connect data:
        self.picker.delegate = self
        self.picker.dataSource = self
 
        pickerData = ["Georgia", "Texas", "Colombia", "Argentina", "Canada", "Peru","Mexico","California"]
        documento = [[VDDOCUMENT_ID_US_GA_DrivingLicense_2012],[VDDOCUMENT_ID_US_TX_DrivingLicense_2016],[VDDOCUMENT_ID_CO_IDCard_2000],[VDDOCUMENT_ID_AR_IDCard_2012], [VDDOCUMENT_ID_CA_ON_DrivingLicense_2007],[VDDOCUMENT_ID_PE_IDCard_2013],[VDDOCUMENT_ID_MX_IDCard_2014],[VDDOCUMENT_ID_US_CA_DrivingLicense_2008, VDDOCUMENT_ID_US_CA_DrivingLicense_2018]]
        
    }
    
    @IBAction func pressSelectDocument(_ sender: Any) {
        picker.isHidden = false
    }
    
    @IBAction func pressSelectSDK(_ sender: Any) {
        VDDocumentCapture.start(withDelegate:self, andDocumentIds:documento[selected], andConfiguration:setBidiConfig())
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // Number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
        
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
        
    // The data to return fopr the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    // Capture the picker view selection
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        picker.isHidden = true
        lbl_documento.text = pickerData[row]
        btn_sdk.isHidden = false
        selected = row;
        lbl_bidis.text = ""
    }
}

extension ViewController : VDDocumentCaptureProtocol {
    func vdDocumentCaptured(_ imageData: Data!, with captureType: VDCaptureType, andDocument document: [VDDocument]!) {
        print("Possible documents captured: \(document.description) of type \(captureType)")
        readMetadata(data: imageData)
    }
    
    func vdTimeWithoutPhotoTaken(_ seconds: Int32, with capture: VDCaptureType) {
        print("Too much time without capturing. Something is happening?")
    }
    
    func vdDocumentAllFinished(_ processFinished: Bool) {
        print("Document capture has finished = ",processFinished)
    }
    
    func readMetadata(data: Data!) {
        picker.isHidden=true
        if let imageSource = CGImageSourceCreateWithData(data! as CFData, nil) {
            let imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil)
            if let dict = imageProperties as? [String: Any] {
                print(dict)
                let bidis = dict.description.components(separatedBy: "bidi-code")
                if (bidis.count>1){
                    let texto = dict.description.replacingOccurrences(of: "\\", with: "", options: NSString.CompareOptions.literal, range: nil)
                    let tipo = texto.components(separatedBy: "\"")
                    for (index, item) in tipo.enumerated() {
                        if(item=="body"){
                            codigos.add(tipo[index+2])
                        } else if (item=="name"){
                            tipos.add(tipo[index+2])
                        }
                    }
                }
                lbl_resultado.text = "Se han encontrado \(bidis.count-1) bidis en el documento"
                var codes:String = ""
                for (i,cod) in codigos.enumerated(){
                    codes += "Barcode \(i+1) Tipo: \(tipos[i])\n\(cod)\n\n"
                }
                lbl_bidis.text = codes
            }
        }
    }
}
