# coding: utf-8
Pod::Spec.new do |spec|
  spec.name         = "VDDocumentCapture"
  spec.version      = "5.0.2"
  spec.summary      = "The goal of this framework is to capture a selfie image guiding the user in a self-contained and accessible system."
  spec.description  = <<-DESC
The goal of this framework is to capture four document images guiding the user in a self-contained and accessible system.

This SDK captures the document images with the device rear camera.

This SDK retrieves this information:

- Document type.
- Document obverse (front side) image without flash.
- Document obverse image with flash.
- Document reverse (back side) image without flash.

Some permissions are needed and have to be written in the info.plist of the app using the framework:

- Camera.

In order to improve the user experience, the camera takes the document photo automatically (without touching the screen), this is done due to its automatic document recognition.
DESC
  spec.homepage     = "https://globaldevtools.bbva.com/bitbucket/projects/CELLSNATIVE/repos/library-bbva-veridas-document-capture-code-ios/browse"
  spec.license      = "Propietary"
  spec.author       = { "Miguel Isla Urtasun" => "misla@veridas.com",
                        "Radostina Spasova Dimitrova" => "rspasova@veridas.com",
                        "Iván del Burgo Ullate" => "idelburgo@veridas.com" }

  spec.platform     = :ios, "10.0"

  spec.source       = { :http => "https://globaldevtools.bbva.com:443/artifactory/api/pods/cells-native-cocoapods/pod/pkg/" + spec.name.to_s + '/' + spec.version.to_s, :type => 'tgz' }

  spec.vendored_frameworks = 'xcFramework/' + spec.name.to_s + '.xcframework'
  spec.cocoapods_version = '>= 1.10.0'

  spec.dependency 'VDLibrary', '>= 6.0.1'
 # spec.dependency 'VDLogger', '>= 2.0.0' 
end


