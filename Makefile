repository := ssh://git@globaldevtools.bbva.com:7999/cellsnative/cli-plethora-flows.git
branch := feature/createxcframeworkflow
config := ".plethora/config.json"
tempDir := $(shell mktemp -d)

.PHONY: all build checkout install clean uninstall

all: build uninstall clean

build: 
	@echo "⏳ Building..."
	plethora createxcframeworkflow --json $(config)
	@echo "✅ Complete"

checkout:
	@echo "⏳ Checking out Plethora..."
	git clone --branch $(branch) --depth=1 $(repository) $(tempDir) && cd "$(tempDir)" && make install
	@echo "✅ Complete"

install: checkout
	@echo "⏳ Installing Plethora..."
	cd "$(tempDir)" && (make uninstall || true) && make install
	@echo "✅ Complete"

clean:
	@echo "⏳ Cleaning..."
	rm -rf $(tempDir)/* || true
	@echo "✅ Complete"

uninstall:
	@echo "⏳ Uninstalling Plethora..."
	cd "$(tempDir)" && (make uninstall || true)
	@echo "✅ Complete"