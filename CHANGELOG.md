# Changelog VDDocumentCapture

# Version 5.1.0 <small><small><small>August 2021</small></small></small>

- Added ES_IDCard_2021

# Version 5.0.2 <small><small><small>August 2021</small></small></small>

- Solved issue with closebutton showing incorrectly in iPhone 12
  
# Version 5.0.1 <small><small><small>July 2021</small></small></small>

- Built with version 2.0.1 of Imageproccessing. Including OpenCV 4.5.2

# Version 5.0.0 <small><small><small>July 2021</small></small></small>

- Now we distribute as xcframework.

# Version 4.7.0 <small><small><small>May 2021</small></small></small>

- Added new documents (CO_IDCard_2020, IT_HealthCard_2004)
- Added new configuration key "infobuttonimage"
- Fixed bug with configuration "closebuttonimage"
- Improved image integrity to assure image origin.

# Version 4.6.0 <small><small><small>January 2021</small></small></small>

- Added new documents.
- Improved MX_IDCard_2019 and ES_ResidencePermit_2020 detection.

# Version 4.5.0 <small><small><small>December 2020</small></small></small>

- Added image integrity to assure image origin.

# Version 4.4.0 <small><small><small>November 2020</small></small></small>

- Refactored the opening of the SDK so it correctly draws in all orientations.

# Version 4.3.3 <small><small><small>November 2020</small></small></small>

- Updated VDLibrary to improve AT_DrivingLicense_2004 detection.

# Version 4.3.2 <small><small><small>October 2020</small></small></small>

- Removed logger.

# Version 4.3.1 <small><small><small>October 2020</small></small></small>

- Improved SDK opening performance.

# Version 4.3.0 <small><small><small>October 2020</small></small></small>

- Added new documents.

# Version 4.2.1 <small><small><small>September 2020</small></small></small>

- Solved issues with bitcode support
- Fixed problem with text color in some buttons.
- Fixed bug in the tutorial view.

# Version 4.2 <small><small><small>June 2020</small></small></small>

- Added new documents.
- Removed one mexican document id, IFE D (MX_IDCard_2013). This document can be captured with IFE E (MX_IDCard_2014) id.
- Removed unused configurations and variables.
- Removed deprecated method to avoid the autorotation.

# Version 4.1 <small><small><small>April 2020</small></small></small>

- Added the possibility to inject a logger in the SDK.

# Version 4.0.3  <small><small><small>June 2020</small></small></small>

- Bug fixes in VDLibrary with Others document.   

# Version 4.0.1  <small><small><small>February 2020</small></small></small>

- Fixed configuration bug.  

# Version 4.0  <small><small><small>February 2020</small></small></small>

- Improvements in document detection.
- Updated available documents.
- Changed countdown behavior to increment the capture usability.  
- Fixed visual problem in tutorial view.                   

# Version 3.6.1 <small><small><small>November 2019</small></small></small>

- Fixed bug after project recreation.

# Version 3.6.0 <small><small><small>September 2019</small></small></small>

- Added new documents.
- Bug fixes.

# Version 3.5.0 <small><small><small>July 2019</small></small></small>

- Possibility to input some styled text as HTML.
- Added zoom possibility in document image visualization.
- Interface improvements, with new configurable elements.
- Bug fixes.

# Version 3.4.0 <small><small><small>April 2019</small></small></small>

- Added the possibility of working with NSStrings instead of VDDocuments.
- Validation of document view updated with "ratioButtonsValidation" configuration.
- Peruan template rotation issue solved.
- Bug fixes.

# Version 3.2.0 <small><small><small>December 2018</small></small></small>

- Improved detection of austrian documents.
- Solved rotation problem on iPhone X.
- Bug fixes.

# Version 3.1.0 <small><small><small>November 2018</small></small></small>

- Added Austrian Driving License on paper (issued in 2004).
- Added automatic ICAO standard passport capture.
- Tick image color is configurable.
- Reduced SDK size.
- Added possibility to configure an initial time without detections on each side.
- Added flip animation at the beginning of the reverse capture.
- Bug fixes.

# Version 3.0.1 <small><small><small>November 2018</small></small></small>

- Solved detection issues in iPhone X.
- Solved case for AllFinished boolean when "onlyobverse" is enabled.
- Improved countdown visualization.

# Version 3.0.0 <small><small><small>October 2018</small></small></small>

- Improved speed and detection of the document capture.
- Changed capture progress into a 3,2,1 countdown.
- Added possibility to visualize the photo captured and let the user to accept or repeat it.
- Added possibility to configure more colors and other interface elements.
- Added capture of generic document "OTHERS".
- Added interface for iPads.
- Added possibility to configure the SDK to capture only the obverse.
- Bug fixes.

# Version 2.6.0

- Improved document image capture.

# Version 2.5.0

- Added documents Peru DNI 2007 and Colombia DNI 2000.
- Alert between flash/non-flash image captures.
- Configurable initial alert.
- VDDocumentFinished returns a boolean that informs about the process been canceled by the user.

# Version 2.4.0

- The SDK notifies the app when the user needs help with the capture.
- Bug fixes.
- Performance improvement.

# Version 2.3.0

- Added Peruan document DNI 2013.
- Added possibility to configure the SDK.
- Improved detection of Argentinian documents (DNI 2009 and DNI 2012).
- Solve autoDualCameraFusionEnables issue that crashes the application when using it on a iOS &lt; 10.2.
- Around 60% SDK size reduction.
- Bug fixes.

# Version 2.2.0

- Bitcode support.
- Simulator support.

# Version 2.1.0

- Manual passport capture added.
- Argentinian 2009 automatic capture.
- Argentinian 2012 automatic capture.
- Elastic guides to help the user with the capture.

# Version 2.0.0

- No selection of the document is needed

# Version 1.0.0

- First release
