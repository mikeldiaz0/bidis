//
//  VDDocumentCapture.m
//  VDDocumentCapture
//
//  Copyright © 2017 das-nano. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VDDocumentCapture.h"
#import "VDDocumentDB.h"
#import "GenericDocumentController.h"
#import <VDLibrary/ValiDasDocumento.h>
#import <VDLibrary/ValiDas.h>
#import <VDLibrary/ValiDasPais.h>
#import "VDConfig.h"
#import "DocumentTutorialViewController.h"
#import "DocumentCommonUtils.h"
#import <VDLibrary/LogStrings.h>
//#import <VDLogger/VDLogger-Swift.h>


@interface VDDocumentCapture() <VDGenericDocumentProtocol, TutorialProtocol> {
    NSDictionary<NSString *,NSArray<NSString *> *> * _documents;
    UIViewController<VDDocumentCaptureProtocol>* _delegate;
    DocumentTutorialViewController *tutorialVC;
    NSArray<VDDocument *>* _previousDocuments;
    NSArray<VDDocument *>* _docsProvided;
}
@end


@implementation VDDocumentCapture
static VDDocumentCapture* _my = nil;
static BOOL _isStarted = false;
static UIViewController *controllerPresented;
NSMutableDictionary* _settings;
bool _processFinished = false;


//NSArray<NSNumber*>* _rectangleArray;
/*
 * @brief Array that contains not configurable keys for settings.plist
 */
NSArray *_notConfigurableKeys;

+ (NSDictionary<VDCountry *,NSArray<VDDocument *> *> *) getDocumentsForCountryAvailable
{
    return [VDDocumentDB createDB];
}

+ (void) stop {
    
    [_my stop:_processFinished];
}

+ (BOOL) isStarted{
    return _isStarted;
}

+ (NSString*) getVersion{
    return (NSString *)[[[NSBundle bundleForClass:[self class]] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
}

//+ (void) addLogger:(id<VDLoggerProtocol>_Nonnull)logger withId:(NSString* _Nonnull)loggerId{
//    [[CommonLog shared] installWithLoggerId:loggerId logger:logger];
//}


+ (UIViewController*) startWithDelegate:(UIViewController<VDDocumentCaptureProtocol> *)delegate andDocuments:(NSArray<VDDocument*>*)documents{
    if(_notConfigurableKeys == nil){
        _notConfigurableKeys = NOT_CONFIGURABLE_KEYS;
    }
#if TARGET_IPHONE_SIMULATOR
    return nil;
#else
    [self createInstance];
    //
  
    NSMutableArray *docsIds = [[NSMutableArray alloc] init];
    
    for (VDDocument *document in documents) {
        NSString* doc = [ValiDas getDocumentForLegacyId: [NSString stringWithFormat:@"%i", document.documentLegacyId]];
        [docsIds addObject: doc];
    }
    return [_my startWithDelegate:delegate andDocuments:docsIds andConfiguration:nil];
#endif
}

+ (UIViewController*) startWithDelegate:(UIViewController<VDDocumentCaptureProtocol> *)delegate andDocumentIds:(NSArray<NSString*>*)documents{
    if(_notConfigurableKeys == nil){
        _notConfigurableKeys = NOT_CONFIGURABLE_KEYS;
    }
#if TARGET_IPHONE_SIMULATOR
    return nil;
#else
    [self createInstance];
    //
    return [_my startWithDelegate:delegate andDocuments:documents andConfiguration:nil];
#endif
}

+ (UIViewController*) startWithDelegate:(UIViewController<VDDocumentCaptureProtocol> *)delegate andDocuments:(NSArray<VDDocument*>*)documents andConfiguration: (NSDictionary*) config{
    if(_notConfigurableKeys == nil){
        _notConfigurableKeys = NOT_CONFIGURABLE_KEYS;
    }
    
#if TARGET_IPHONE_SIMULATOR
    return nil;
#else
    [self createInstance];
    //
    NSMutableArray *docsIds = [[NSMutableArray alloc] init];
    
    for (VDDocument *document in documents) {
        NSString* doc = [ValiDas getDocumentForLegacyId: [NSString stringWithFormat:@"%i", document.documentLegacyId]];
        [docsIds addObject: doc];
    }
    return [_my startWithDelegate:delegate andDocuments:docsIds andConfiguration:config];
#endif
}

+ (UIViewController*) startWithDelegate:(UIViewController<VDDocumentCaptureProtocol> *)delegate andDocumentIds:(NSArray<NSString*>*)documents andConfiguration: (NSDictionary*) config
{
    if(_notConfigurableKeys == nil){
        _notConfigurableKeys = NOT_CONFIGURABLE_KEYS;
    }
    
#if TARGET_IPHONE_SIMULATOR
    return nil;
#else
    [self createInstance];
    //
    return [_my startWithDelegate:delegate andDocuments:documents andConfiguration:config];
#endif
}

/**
 @brief Method to initialize all the elements needed for the SDK to work
 */
+ (void) createInstance{
    if (UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication.statusBarOrientation)) {
        [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationPortrait) forKey:@"orientation"];
    }
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _my = [[VDDocumentCapture alloc] init];
    });
    [ValiDas initWithDelegate:self.class];
}

-(void)changeConfiguration:(NSDictionary*)config{
    if(config != nil){
        NSArray *keys = [_settings allKeys];
        for(NSString* key in keys){
            id value = [config objectForKey:key];
            if (value != nil ){
                NSObject *v = [self filterConfiguration:value forkey:key];
                if(v != nil){
                    [_settings setObject:value forKey:key];
                }
            }
        }
    }
}
-(NSObject*)filterConfiguration:(NSObject*)value forkey:(NSString*)key {
    if([_notConfigurableKeys containsObject:key]){
        return nil;
    }else if([key isEqualToString:@"obverseflash"] ||
             [key isEqualToString:@"arrows"] ||
             [key isEqualToString:@"fixedtemplate"] ||
             [key isEqualToString:@"fixedrectangle"] ||
             [key isEqualToString:@"infoalertshow"] ||
             [key isEqualToString:@"showdocument"] ||
             [key isEqualToString:@"onlyobverse"] ||
             [key isEqualToString:@"reverseflash"] ||
             [key isEqualToString:@"closebutton"]){
        if([(NSString*)value isEqual:@"YES"] || [(NSString*)value isEqual:@"NO"]){
            return value;
        }else{
            return nil;
        }
    }else if([key isEqualToString:@"secondswithoutpicture"]){
        NSCharacterSet* nonNumbers = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        //NSCharacterSet* nonNumbers = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789."] invertedSet];
        NSRange r = [(NSString*)value rangeOfCharacterFromSet: nonNumbers];
        if(r.location == NSNotFound && ((NSString*)value).length > 0){
            return value;
        }else{
            return nil;
        }
    } else if ([key isEqualToString:@"obversedetectiondelay"] || [key isEqualToString:@"reversedetectiondelay"]) {
        NSCharacterSet* numbers = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        NSRange r = [(NSString*)value rangeOfCharacterFromSet: numbers];
        if (r.location == NSNotFound && ((NSString*)value).length > 0) {
            return value;
        } else {
            return nil;
        }
    } else if([key isEqualToString:@"backgroundcolor"] ||
             [key isEqualToString:@"repeatbuttonbackgroundcolor"] ||
             [key isEqualToString:@"repeatbuttontextcolor"] ||
             [key isEqualToString:@"continuebuttonbackgroundcolor"] ||
             [key isEqualToString:@"continuebuttontextcolor"] ||
             [key isEqualToString:@"documentdetectedoklevel1color"] ||
             [key isEqualToString:@"documentdetectedoklevel2color"] ||
             [key isEqualToString:@"documentdetectedoklevel3color"] ||
             [key isEqualToString:@"documentdetectedveryclosecolor"] ||
             [key isEqualToString:@"documentdetectedveryfarcolor"] ||
             [key isEqualToString:@"capturebackgroundcolor"] ||
             [key isEqualToString:@"informativetextcolor"] ||
             [key isEqualToString:@"countdowntextcolor"] ||
             [key isEqualToString:@"passportbuttonbackgroundcolor"] ||
             [key isEqualToString:@"validationbackgroundcolor"] ||
             [key isEqualToString:@"popupvalidationbackgroundcolor"] ||
             [key isEqualToString:@"popupvalidationtextcolor"] ||
             [key isEqualToString:@"arrowcolor1"] ||
             [key isEqualToString:@"arrowcolor2"] ||
             [key isEqualToString:@"arrowcolor3"] ||
             [key isEqualToString:@"arrowcolor4"] ||
             [key isEqualToString:@"arrowcolor5"] ||
             [key isEqualToString:@"checkdocumenttextcolor"] ||
             [key isEqualToString:@"tickcirclecolor"] ||
             [key isEqualToString:@"tickcolor"] ||
             [key isEqualToString:@"flashwaitingalertbgcolor"] ||
             [key isEqualToString:@"flashwaitingalerttextcolor"] ||
             [key isEqualToString:@"shutterbuttonmessagetextcolor"] ||
             [key isEqualToString:@"shutterbuttonbackgroundcolor"] ||
             [key isEqualToString:@"shutterbuttonbordercolor"] ||
             [key isEqualToString:@"shutterbuttonmessagebackgroundcolor"] ||
             [key isEqualToString:@"tutorialbackgroundcolor"] ||
             [key isEqualToString:@"tutorialcontinuebuttoncolor"] ||
             [key isEqualToString:@"tutorialcontinuebuttontextcolor"] ||
             [key isEqualToString:@"tutorialtextcolor"] ||
             [key isEqualToString:@"tutorialtitlecolor"]
              ) {
        
        id red = (NSDictionary*)[value valueForKey:@"red"];
        id green = (NSDictionary*)[value valueForKey:@"green"];
        id blue = (NSDictionary*)[value valueForKey:@"blue"];
        id alpha = (NSDictionary*)[value valueForKey:@"alpha"];
        if (red != nil && green!= nil && blue != nil && alpha != nil) {
            return value;
        } else {
            return nil;
        }
    }else{
        if([value isKindOfClass:[NSString class]]){
            return value;
        }else{
            return nil;
        }
    }
    return nil;
}
+ (NSArray*)getConfigurationKeys{
    NSString *path = [[NSBundle bundleForClass:VDDocumentCapture.class] pathForResource:@"settings" ofType:@"plist"];
    NSMutableDictionary *settings = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
    _notConfigurableKeys = NOT_CONFIGURABLE_KEYS;
    [settings removeObjectsForKeys:_notConfigurableKeys];
    return [settings allKeys];
}



- (UIViewController*) startWithDelegate:(nonnull UIViewController<VDDocumentCaptureProtocol> *)delegate andDocuments:(NSArray<NSString *>*)documents andConfiguration:(NSDictionary*) config {
    
    [self startedLogsWithConfig:config];
    _delegate = delegate;
    
    NSMutableArray<VDDocument *>* docs = [NSMutableArray new];
    
    NSMutableArray* tmpDocs = [NSMutableArray arrayWithArray:documents];
    
    documents = [NSArray arrayWithArray:tmpDocs];
    
    for (NSString* documentName in documents) {
        VDDocument* document = [VDDocumentDB getDocumentFromString:documentName];
        if(document.documentName == nil) {
            document.documentName = documentName;
        }
        [docs addObject:document];
    }
    _docsProvided = docs;
    
    NSMutableArray *docArray = [NSMutableArray new];
  
    NSArray *availableDocs = [ValiDas getAvailableDocuments];
    
    for (NSString *docItem in documents) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id CONTAINS[cd] %@", docItem];
        NSArray *filtered = [availableDocs filteredArrayUsingPredicate:predicate];
        if (filtered.count > 0) {
            NSDictionary *documentDict = [filtered firstObject];
            ValiDasDocumento *docAdd = [[ValiDasDocumento alloc] init];
            docAdd.type = [documentDict objectForKey:@"id"];
            docAdd.name = [documentDict objectForKey:@"short_description"];
            docAdd.obverse = [[documentDict objectForKey:@"image_types"] containsObject:@"obverse"];
            docAdd.reverse = [[documentDict objectForKey:@"image_types"] containsObject:@"reverse"];
            [docArray addObject:docAdd];
        }
    }
    
    NSString *preferredsettings = @"settings-en";
    NSString *languaje = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([languaje containsString:@"-"]){
        languaje = [[languaje componentsSeparatedByString:@"-"] objectAtIndex:0];
    }
    if (config != nil && [config valueForKey:@"language"]) {
        if ([[config valueForKey:@"language"] isEqualToString:@"ES"]) {
            preferredsettings = @"settings-es";
        }
    } else if ([languaje.uppercaseString containsString:@"ES"]) {
         preferredsettings = @"settings-es";
    }
    
    NSString *path = [[NSBundle bundleForClass:VDDocumentCapture.class] pathForResource:preferredsettings ofType:@"plist"];
    _settings = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
    
//    [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                             message:[CONFIGURATED_KEYS_LOG stringByAppendingString:[NSString stringWithFormat:@"%@",config]]
//                            metadata:nil
//                            file:NSStringFromClass([self class])
//                            function:NSStringFromSelector(_cmd)
//                            line:__LINE__];
    
    [_my changeConfiguration:config];
    
    NSBundle *frameworkBundle = [NSBundle bundleForClass:VDDocumentCapture.class];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Framework" bundle:frameworkBundle];
    NSOperatingSystemVersion fullVersion = [[NSProcessInfo processInfo] operatingSystemVersion];
    int versionIOS = (int)(fullVersion.majorVersion);
    
//    [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                             message:[SO_VERSION_LOG stringByAppendingString:[@(versionIOS) stringValue]]
//                            metadata:nil
//                            file:NSStringFromClass([self class])
//                            function:NSStringFromSelector(_cmd)
//                            line:__LINE__];
    
    if ([[config objectForKey:@"rectangledetector"] isEqual:@"tight"]) {
        [ValiDas setRectangleDetectorHardMode];
    }
    
    GenericDocumentController *controller = [storyboard instantiateViewControllerWithIdentifier:@"GenericDocumentController"];
    [controller setModalPresentationStyle:UIModalPresentationFullScreen];
    [controller setDelegate:self];
    [controller setDocuments:docArray];
    [controller setConfiguration: _settings];
    controllerPresented = controller;
    
    [self presentTutorialIfNeeded];
    
    return controllerPresented;
}

- (void) startedLogsWithConfig:(NSDictionary*) config{
//    if (config != nil) {
//        if ([config objectForKey:@"userid"] != nil) {
//            [[CommonLog shared] setUserIdWithUserId:[config objectForKey:@"userid"]];
//        }
//    }
//
//    [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                                message:START_SDK_LOG
//                               metadata:nil file:NSStringFromClass([self class])
//                               function:NSStringFromSelector(_cmd) line:__LINE__];
//
//     [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                                  message:[SDK_VERSION_LOG stringByAppendingString:[DocumentCommonUtils getSDKversion]]
//                                  metadata:nil file:NSStringFromClass([self class])
//                                  function:NSStringFromSelector(_cmd) line:__LINE__];
//
//    [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                                message:[NSString stringWithFormat:DEVICE_LOG,
//                                         [DocumentCommonUtils getDeviceName],
//                                         [DocumentCommonUtils getCurrentiOSVersion]]
//                               metadata:nil file:NSStringFromClass([self class])
//                               function:NSStringFromSelector(_cmd) line:__LINE__];
//
//    [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                                      message:[BUNDLE_IDENTIFIER_LOG stringByAppendingString:
//                                      [[NSBundle mainBundle] bundleIdentifier]]
//                                      metadata:nil file:NSStringFromClass([self class])
//                                      function:NSStringFromSelector(_cmd) line:__LINE__];
}


- (void) stop:(Boolean) processFinished
{
    [_my VDAllFinished:processFinished];
}

- (void) stopsdk{
    [_my stop:false];
}

- (void) VDAllFinished:(Boolean)processFinished{
    if (_isStarted) {
        _previousDocuments = nil;
       
//        [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                                   message:[NSString stringWithFormat:ALL_FINISH_LOG, processFinished ? MSG_TRUE : MSG_FALSE]
//                                  metadata:nil
//                                  file:NSStringFromClass([self class])
//                                  function:NSStringFromSelector(_cmd)
//                                  line:__LINE__];
//
//        [[CommonLog shared] removeAll];
        dispatch_async(dispatch_get_main_queue(), ^{
            [controllerPresented.presentingViewController dismissViewControllerAnimated:NO completion:^{
                [self callDelegateAllFinished:processFinished];
            }];
        });
    }
}

-(void)callDelegateAllFinished:(BOOL)processFinished
{
    [((GenericDocumentController*)controllerPresented) finishObjects];
    
    if (_delegate && [_delegate conformsToProtocol:@protocol(VDDocumentCaptureProtocol)]) {
        if([_delegate respondsToSelector:@selector(VDDocumentAllFinished:)]){
            usleep(500000);
            
            [ValiDas setDelegate:nil];
            [ValiDas finalizeInstance];
            
            [_delegate VDDocumentAllFinished:processFinished];
        }
    }
    
    _isStarted = false;
}

#pragma mark - Helpers

-(void)presentTutorialIfNeeded
{
    if ([[_settings objectForKey:@"showtutorial"] isEqualToString:@"YES"]) {
        if (UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication.statusBarOrientation)) {
             [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationPortrait) forKey:@"orientation"];
        }
        tutorialVC = [[DocumentTutorialViewController alloc] initWithDelegate:self config:_settings andDeviceModel:[ValiDas getDeviceModel]];
        tutorialVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
        [_delegate presentViewController:tutorialVC animated:YES completion:nil];
    } else {
        [self presentDocumentUIController];
    }
    
}

-(void)presentDocumentUIController
{
    if(_delegate){
        [_delegate presentViewController:controllerPresented animated:YES completion:nil];
    }
    _isStarted = true;

}
#pragma mark - Tutorial Protocol
-(void) tutorialClosed {
    [_delegate.presentedViewController dismissViewControllerAnimated:NO completion:nil];
    [self presentDocumentUIController];
}


#pragma mark - Delegated Methods

- (void) VDDocumentCaptured:(NSData *)imageData withCaptureType:(VDCaptureType)captureType andDocumentType:(NSArray<VDDocument *> *)documentTypes
{
    NSMutableArray<VDDocument *> *arrayVDDocuments = [NSMutableArray new];
    
    if (!_previousDocuments) {
        for (VDDocument *tipoItem in documentTypes) {
            [arrayVDDocuments addObject:tipoItem];
        }
    } else {
        for (VDDocument *tipoItem in documentTypes) {
            for (VDDocument *prevItem in _previousDocuments) {
                if ([prevItem.documentName isEqualToString:tipoItem.documentName]) {
                    [arrayVDDocuments addObject:prevItem];
                    break;
                }
            }
        }
    }
    
    if ([documentTypes count] == 0) {
        arrayVDDocuments = [NSMutableArray arrayWithArray:_docsProvided];
    }
    
    BOOL isOnlyObverse = (arrayVDDocuments.count == 1 && !arrayVDDocuments.firstObject.hasReverse) || [[_settings valueForKey:@"onlyobverse"] isEqualToString:@"YES"];
    
    if (captureType == REVERSE_WITHOUT_FLASH || captureType == REVERSE_WITH_FLASH || isOnlyObverse ) {
        _processFinished = true;
    }
    
    _previousDocuments = documentTypes;
    
    if (_delegate) {
        if ([_delegate conformsToProtocol:@protocol(VDDocumentCaptureProtocol)]) {
            if([_delegate respondsToSelector:@selector(VDDocumentCaptured:withCaptureType:andDocument:)]){
                [_delegate VDDocumentCaptured:imageData withCaptureType:captureType andDocument:arrayVDDocuments];
            }
        }
    }
}

- (void)VDTimeWithoutPhotoTaken:(int)seconds withCaptureType:(VDCaptureType)capture {
//     [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                             message:[TIME_WITHOUT_PHOTO_TAKEN_LOG stringByAppendingString:[@(seconds) stringValue]]
//                            metadata:nil
//                            file:NSStringFromClass([self class])
//                            function:NSStringFromSelector(_cmd)
//                            line:__LINE__];
//    
    [_delegate VDTimeWithoutPhotoTaken:seconds withCaptureType:capture];
}

#pragma mark - End Delegated Methods
@end
