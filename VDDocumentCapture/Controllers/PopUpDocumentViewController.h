//
//  PopUpDocumentViewController.h
//  VDDocumentCapture
//
//  Copyright © 2018 das-nano. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopUpDocumentViewController : UIViewController

-(id)initWithMessage:(NSString*)message config:(NSDictionary*)config;

@end
