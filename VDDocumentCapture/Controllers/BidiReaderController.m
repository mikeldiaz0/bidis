//
//  BidiReaderController.m
//  VDDocumentCapture
//
//  Copyright © 2020 das-nano. All rights reserved.
//

#import "BidiReaderController.h"
#import "Constants.h"

@interface BidiReaderController()

@property(nonatomic) BOOL reading;
@property(nonatomic) BOOL searchFinished;
@property(nonatomic, strong) NSMutableArray *rotations;
@property(nonatomic, strong) NSDictionary *settings;

@end

@implementation BidiReaderController

-(id)init
{
    self = [super init];
    
    if (self) {
        [ValiDas initWithDelegate:self.class];
        [self setSearchFinishedValue:NO];
    }
    
    return self;
}

-(BOOL)allCodesRead
{
    return self.searchFinished || self.noBidiCodesToSearch;
}

-(BOOL)areAllCodesReadForReadDocument:(NSArray*)docs side:(SideType)side
{
    if (self.allCodesRead) {
        return YES;
    }
    
    if (docs.count == 1) {
        NSString *detectedDoc = docs.firstObject;
        NSArray *bidiCodes = [ValiDas getBidiCodesForDocument:detectedDoc side:side];
        if (bidiCodes.count > 0) {
            NSMutableArray *detectedDocCombination = [self getCombinationForBidiCodes:bidiCodes];
            NSMutableArray *readCombination = [self getReadCombination];
            
            [self setSearchFinishedValue:[readCombination isEqualToArray:detectedDocCombination]];
            
            return _searchFinished;
        }
        
        return YES; //No bidi codes for this doc in this side -> search finished
    }
    
    return self.allCodesRead;
}

-(BOOL)isSearchFinished
{
    return self.searchFinished;
}

-(void)setSearchFinished
{
    [self setSearchFinishedValue:YES];
}

-(void)setConfiguration:(NSDictionary *)configuration
{
    self.settings = configuration;
}

-(void)setBidiCodesForDocuments:(NSArray<NSArray<ValiDasBidiCode*>*>*)bidiCodes
{
    self.noBidiCodesToSearch = NO;
    
    if (bidiCodes.count == 0) {
        self.noBidiCodesToSearch = YES;
        return;
    }
    
    [self setSearchFinishedValue:NO];
    
    NSMutableArray *allBidiTypes = [[NSMutableArray alloc] init];
    self.rotations = [[NSMutableArray alloc] init];
        
    [bidiCodes enumerateObjectsUsingBlock:^(NSArray* obj, NSUInteger idx, BOOL *stop) {
        [allBidiTypes addObjectsFromArray:[obj valueForKeyPath:@"type"]];
        [self.rotations addObjectsFromArray:[obj valueForKeyPath:@"rotation"]];
    }];
    
    self.bidiTypesToSearch = [[[NSSet setWithArray:allBidiTypes] allObjects] sortedArrayUsingSelector:@selector(compare:)];
    self.combinationsToSearch = [[NSMutableArray alloc] init];
    self.bidiCodesRead = [[NSMutableArray alloc] init];
    self.allResults = [[NSMutableArray alloc] init];
    
    for (NSArray<ValiDasBidiCode*> *codes in bidiCodes) {
        NSMutableArray *combination = [self getCombinationForBidiCodes:codes];
        
        if (![self.combinationsToSearch containsObject:combination]) {
            [self.combinationsToSearch addObject:combination];
        }
    }
    
    [self.bidiTypesToSearch enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [self.bidiCodesRead addObject:[[NSMutableArray alloc] init]];
    }];
}

-(NSMutableArray*)getCombinationForBidiCodes:(NSArray<ValiDasBidiCode*>*)codes
{
    NSMutableArray *combination = [[NSMutableArray alloc] init];
    
    for (NSNumber *type in self.bidiTypesToSearch) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.type == %@", type];
        NSArray *numCodes = [codes filteredArrayUsingPredicate:predicate];
        [combination addObject:@(numCodes.count)];
    }
    
    return combination;
}


-(void)searchForBidiCodesInImage:(UIImage*)image {
    if (![self allCodesRead] && !self.reading) {
        @autoreleasepool {
            self.reading = YES;
            NSArray<ValiDasBidiResult*> *res = [ValiDas searchBidiCodes:image hints:self.bidiTypesToSearch rotations:self.rotations];
            [self addBidiCodesRead:res];
            [self checkIfAnyCombinationIsCompleted];
            self.reading = NO;
        }
    }
}

-(NSArray<ValiDasBidiResult*>*)getResults
{
    return self.allResults;
}

-(void)cleanResults
{
    [self.allResults removeAllObjects]; // = [[NSArray alloc] init];
}


#pragma mark - Private funcs

-(void)addBidiCodesRead:(NSArray*)bidiResults
{
    for (ValiDasBidiResult *result in bidiResults) {
      /*  if (_allResults == nil){
            self.allResults = [[NSMutableArray alloc] init];
        }*/
        int index = (int)[self.bidiTypesToSearch indexOfObject:@(result.bidiType)];
        
        if (index >= 0 && result.bidiString.length > 0) {
            NSMutableArray *results = [self.bidiCodesRead objectAtIndex:index];
            
        if (![results containsObject:result.bidiString]) {
        //    if (![_allResults containsObject:result]) {
                [results addObject:result.bidiString];
                [self.bidiCodesRead replaceObjectAtIndex:index withObject:results];
                [self.allResults addObject:result];
            }
        }
    }
}

-(void)checkIfAnyCombinationIsCompleted
{
    NSMutableArray *readCombination = [self getReadCombination];
    
    for (int i = 0; i < self.bidiTypesToSearch.count; i++) {
        NSMutableIndexSet *indexToDelete = [[NSMutableIndexSet alloc] init];
        
        for (NSArray *combination in self.combinationsToSearch) {
            if ([[combination objectAtIndex:i] intValue] < [[readCombination objectAtIndex:i] intValue]) {
                [indexToDelete addIndex:[self.combinationsToSearch indexOfObject:combination]];
            }
        }
        
        [self.combinationsToSearch removeObjectsAtIndexes:indexToDelete];
        
        if (self.combinationsToSearch.count == 1 && [self.combinationsToSearch containsObject:readCombination]) {
            [self setSearchFinishedValue:YES];
            break;
        }
    }
}

-(NSMutableArray*)getReadCombination
{
    NSMutableArray *codesRead = [NSMutableArray arrayWithCapacity:self.bidiCodesRead.count];
    
    [self.bidiCodesRead enumerateObjectsUsingBlock:^(NSArray* obj, NSUInteger idx, BOOL *stop) {
        [codesRead addObject:@(obj.count)];
    }];
    
    return codesRead;
}

-(void)setSearchFinishedValue:(BOOL)value
{
    self.searchFinished = value;
}

@end
