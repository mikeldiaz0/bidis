//
//  LoadingViewController.h
//  VDDocumentCapture
//
//  Copyright © 2019 das-nano. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingViewController : UIViewController

-(id)initWithConfig:(NSDictionary*)config title:(NSString*)title withSize:(float)size animated:(BOOL)animated;
-(void)closeView;

@end
