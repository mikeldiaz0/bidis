//
//  DocumentValidatorViewController.m
//  VDDocumentCapture
//
//  Copyright © 2018 das-nano. All rights reserved.
//

#import "DocumentValidatorViewController.h"
#import "PopUpDocumentViewController.h"
#import "VDConfig.h"
#import "DocumentUtils.h"
#import "DocumentImageViewController.h"
#import <VDLibrary/LogStrings.h>
//#import <VDLogger/VDLogger-Swift.h>
 
@interface DocumentValidatorViewController ()

@property(nonatomic) VDCaptureType captureType;
@property(nonatomic, strong) NSData *imageData;
@property(nonatomic, strong) NSDictionary *config;
@property(nonatomic, strong) UIViewController<DocumentValidatorProtocol>* delegate;

@property(nonatomic, strong) IBOutlet UILabel *labelInfo;
@property(nonatomic, strong) IBOutlet UIButton *buttonCancel;
@property(nonatomic, strong) IBOutlet UIButton *buttonAccept;
@property(nonatomic, strong) IBOutlet UIImageView *imageDocument;
@property(nonatomic, strong) IBOutlet NSLayoutConstraint *imageWidth;
@property(nonatomic, strong) IBOutlet NSLayoutConstraint *buttonCancelWidth;
@property(nonatomic, strong) IBOutlet NSLayoutConstraint *buttonAcceptWidth;
@property (weak, nonatomic) IBOutlet UIButton *infoButton;

@end

@implementation DocumentValidatorViewController

-(id)initWithConfiguration:(NSDictionary*)config delegate:(UIViewController<DocumentValidatorProtocol>*) delegate imageData:(NSData *)imageData
{
    self = [super initWithNibName:@"DocumentValidatorViewController" bundle:[NSBundle bundleForClass:DocumentValidatorViewController.class]];
    
    if (self) {
        self.config = config;
        self.delegate = delegate;
        self.imageData = imageData;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pressImage)];
    [self.imageDocument addGestureRecognizer:tapGesture];
    self.imageDocument.userInteractionEnabled = YES;
    
    [self.infoButton setImage:[DocumentUtils getImageFromFilename:[self.config objectForKey:@"infobuttonimage"]] forState:UIControlStateNormal];
}

-(void)viewWillAppear:(BOOL)animated
{
//    [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                                             message:VIEW_WILL_APPEAR_LOG
//                                             metadata:nil file:NSStringFromClass([self class])
//                                             function:NSStringFromSelector(_cmd)
//                                             line:__LINE__];

    [super viewWillAppear:animated];
    
    if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
        self.view.transform = CGAffineTransformMakeRotation(M_PI_2);
    }
    
    self.imageWidth.constant = self.view.frame.size.width - 40;
    self.imageDocument.image = [UIImage imageWithData:self.imageData];
    
    [self configureButtons];
    [self setColors];
    [self setTexts];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
//    [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                            message:VIEW_WILL_DISAPPEAR_LOG
//                            metadata:nil file:NSStringFromClass([self class])
//                            function:NSStringFromSelector(_cmd)
//                            line:__LINE__];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Orientation

-(BOOL)shouldAutorotate
{
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}


#pragma mark - Buttons Configuration

-(void)configureButtons
{
    [self changeButtonConstraints];
    [self setToButton:self.buttonCancel imageNamed:[_config objectForKey:@"repeatbuttonicon"]];
    [self setToButton:self.buttonAccept imageNamed:[_config objectForKey:@"continuebuttonicon"]];
}

-(void)setToButton:(UIButton*)button imageNamed:(NSString*)imgName
{
    UIImage *img = [DocumentUtils getImageFromFilename:imgName];
    
    if (img) {
        button.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [button setImage:[DocumentUtils resizeImage:img toSmallerSize:CGSizeMake(25, 25)] forState:UIControlStateNormal];
    }
}

-(void)changeButtonConstraints
{
    float ratio = self.imageDocument.image.size.width/self.imageDocument.image.size.height;
    self.imageWidth.constant = self.imageDocument.frame.size.height*ratio;
    self.imageDocument.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addConstraint:[self getButtonConstraintFrom:self.buttonCancelWidth]];
    [self.view addConstraint:[self getButtonConstraintFrom:self.buttonAcceptWidth]];
    [self.view removeConstraint:self.buttonCancelWidth];
    [self.view removeConstraint:self.buttonAcceptWidth];
}

-(NSLayoutConstraint*)getButtonConstraintFrom:(NSLayoutConstraint*)constraint
{
    float radioButtonsValidation = [[_config objectForKey:@"ratioButtonsValidation"] floatValue];
    float multiplier = 1.0/((radioButtonsValidation != 0)? radioButtonsValidation : 2);
    return [NSLayoutConstraint constraintWithItem:constraint.firstItem attribute:constraint.firstAttribute relatedBy:constraint.relation toItem:constraint.secondItem attribute:constraint.secondAttribute multiplier:multiplier constant:constraint.constant];
}


#pragma mark - Colors and texts

-(void)setColors
{
    self.imageDocument.layer.borderWidth = 1.0f;
    self.imageDocument.layer.borderColor = UIColor.whiteColor.CGColor;
    
    self.labelInfo.backgroundColor = [UIColor clearColor];
    self.buttonAccept.backgroundColor = [DocumentUtils transformRGBColor:[self.config objectForKey:@"continuebuttonbackgroundcolor"]];
    self.buttonCancel.backgroundColor = [DocumentUtils transformRGBColor:[self.config objectForKey:@"repeatbuttonbackgroundcolor"]];
    
    self.labelInfo.textColor = [DocumentUtils transformRGBColor:[self.config objectForKey:@"checkdocumenttextcolor"]];
    
    [self.buttonAccept setTitleColor:[DocumentUtils transformRGBColor: [self.config objectForKey:@"continuebuttontextcolor"]] forState:UIControlStateNormal];
    [self.buttonCancel setTitleColor:[DocumentUtils transformRGBColor: [self.config objectForKey:@"repeatbuttontextcolor"]] forState:UIControlStateNormal];
    
    self.view.backgroundColor = [DocumentUtils transformRGBColor:[self.config objectForKey:@"validationbackgroundcolor"]];
}

-(void)setTexts
{
    self.labelInfo.font = [UIFont fontWithName:[_config objectForKey:@"familyName"] size:17];
    self.buttonAccept.titleLabel.font = [UIFont fontWithName:[self.config objectForKey:@"familyName"] size:17];
    self.buttonCancel.titleLabel.font = [UIFont fontWithName:[self.config objectForKey:@"familyName"] size:17];
    
    [DocumentUtils convertToHTMLString:[_config objectForKey:@"checkdocumenttext"] inLabel:self.labelInfo];
    [self.buttonAccept setTitle:[self.config objectForKey:@"continuebuttontext"] forState:UIControlStateNormal];
    [self.buttonCancel setTitle:[self.config objectForKey:@"repeatbuttontext"] forState:UIControlStateNormal];
}


#pragma mark - Actions

-(IBAction)pressInfo
{
//    [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                                   message:SHOW_TUTORIAL_LOG
//                                   metadata:nil file:NSStringFromClass([self class])
//                                   function:NSStringFromSelector(_cmd)
//                                   line:__LINE__];

    PopUpDocumentViewController *popUpVC = [[PopUpDocumentViewController alloc] initWithMessage:[self.config objectForKey:@"userinfo"] config:_config];
    [DocumentUtils presentPopUpModal:popUpVC inController:self animated:YES];
}

-(IBAction)pressRepeat
{
//    [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                                message:REPEAT_LOG
//                                metadata:nil file:NSStringFromClass([self class])
//                                function:NSStringFromSelector(_cmd)
//                                line:__LINE__];
    
    [self.delegate imageValidated:NO];
}

-(IBAction)pressContinue
{
//    [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                                message:CONTINUE_LOG
//                                metadata:nil file:NSStringFromClass([self class])
//                                function:NSStringFromSelector(_cmd)
//                                line:__LINE__];

    [self.delegate imageValidated:YES];
}

-(void)pressImage
{
    if (self.imageDocument.image) {
        DocumentImageViewController *imageVC = [[DocumentImageViewController alloc] initWithConfiguration:self.config image:self.imageDocument.image];
        [DocumentUtils presentPopUpModal:imageVC inController:self animated:YES];        
    }
}

@end
