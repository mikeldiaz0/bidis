//
//  DocumentImageViewController.m
//  VDDocumentCapture
//
//  Copyright © 2019 das-nano. All rights reserved.
//

#import "DocumentImageViewController.h"
#import "DocumentUtils.h"
#import "VDConfig.h"

@interface DocumentImageViewController () <UIScrollViewDelegate>

@property(nonatomic, strong) UIImage *image;
@property(nonatomic, strong) NSDictionary *config;

@property(nonatomic, strong) IBOutlet UIButton *buttonClose;
@property(nonatomic, strong) IBOutlet UIImageView *imageViewPic;
@property(nonatomic, strong) IBOutlet UIScrollView *scrollViewPic;

@end

@implementation DocumentImageViewController

-(id)initWithConfiguration:(NSDictionary*)config image:(UIImage*)image
{
    self = [super initWithNibName:@"DocumentImageViewController" bundle:[NSBundle bundleForClass:DocumentImageViewController.class]];
    
    if (self) {
        self.config = config;
        self.image = image;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [DocumentUtils transformRGBColor:[self.config objectForKey:@"validationbackgroundcolor"]];
    [self.buttonClose setImage:[DocumentUtils getImageFromFilename:[self.config objectForKey:@"closebuttonimage"]] forState:UIControlStateNormal];
    
    self.scrollViewPic.minimumZoomScale = 1.0;
    self.scrollViewPic.maximumZoomScale = 3.0;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
        self.view.transform = CGAffineTransformMakeRotation(M_PI_2);
    }
    
    self.imageViewPic.image = self.image;
}


#pragma mark - Orientation

-(BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}


#pragma mark - UIScrollViewDelegate

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageViewPic;
}


#pragma mark - Actions

-(IBAction)pressClose
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
