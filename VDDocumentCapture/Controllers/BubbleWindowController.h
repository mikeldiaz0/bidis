//
//  BubbleWindowController.h
//  VDDocumentCapture
//
//  Copyright © 2018 das-nano. All rights reserved.
//

#ifndef BubbleWindowController_h
#define BubbleWindowController_h


#endif /* BubbleWindowController_h */

#import <UIKit/UIKit.h>

@interface BubbleWindowController : UIView /*Controller*/

-(id)initWithConfiguration:(NSDictionary*)config focusPoint:(CGPoint)focuspoint screenSize:(CGSize)size andTextSize:(CGFloat)textsize;
-(void)setHiddenView;
-(void)setVisibleView;
@end
