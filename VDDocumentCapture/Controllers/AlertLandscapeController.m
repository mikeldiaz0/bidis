//
//  AlertLandscapeController.m
//  VDDocumentCapture
//
//  Copyright © 2018 das-nano. All rights reserved.
//

#import "AlertLandscapeController.h"

@interface AlertLandscapeController ()

@end

@implementation AlertLandscapeController

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.view.transform = CGAffineTransformMakeRotation(M_PI_2);
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.view setHidden:YES];
}

- (BOOL) shouldAutorotate {
    return NO;
}

@end
