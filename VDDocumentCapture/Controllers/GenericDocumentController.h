//
//  ControladorGeneric.h
//  vali-das
//
//  Copyright © 2017 das-nano. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <VDLibrary/ValiDasConstantes.h>
#import "VDEnums.h"
#import <VDLibrary/ValiDasDocumento.h>
#import "DocumentValidatorViewController.h"
#import "VDConfig.h"
#import "BubbleWindowController.h"
#import "VDDocument.h"
#import "LoadingViewController.h"
#import <VDLibrary/LogStrings.h>

/**
 @brief Protocol that needs to be implemented to be notified with the delegated methods
 */
@protocol VDGenericDocumentProtocol <NSObject>

@required
/**
 @brief Delegate method which will notify when each of the document images is captured
 @details The image is compressed as a jpeg with 50% of quality compression.
 @params imageData (NSData*) Contains the data of the document image captured.
 @params captureType (VDCaptureType) The type of capture with which the image has been taken.
 */
- (void)VDDocumentCaptured:(NSData*)imageData withCaptureType:(VDCaptureType)captureType andDocumentType: (NSArray<VDDocument *>*) documentTypes;
- (void) stopsdk;
/**
 @brief Delegate method which will notify when pass more than X seconds without taking the photo.
 */
- (void) VDTimeWithoutPhotoTaken:(int )seconds withCaptureType:(VDCaptureType)capture;

@end
/**
 @typedef state
 @brief Class states definitions.
 */
typedef enum {
    SEARCHING_OBVERSE,                  /**< Searching obverse. */
    ANALIZING_OBVERSE_WITHOUT_FLASH,    /**< Analyzing obverse non-flash image. */
    ANALIZING_OBVERSE_WITH_FLASH,       /**< Analyzing obverse flash image. */
    SEARCHING_REVERSE,                  /**< Searching reverse. */
    ANALIZING_REVERSE_WITHOUT_FLASH,    /**< Analyzing reverse non-flash image. */
    ANALIZING_REVERSE_WITH_FLASH,       /**< Analyzing reverse flash image. */
    ANALIZING_PASSPORT,                 /**< Analyzing passport. */
    FINISHED_ANALYSIS,                   /**< Analyse finished */
    INVALID_STATE
}state;

/**
 @class GenericDocumentController
 @brief Class that captures images to validate a document.
 @details Guides for each document will be shown as an aid to the user to capture perfect focused and centered images. This guide include: countour of the document than changes color, help-texts and warnings.
    This class implements two protocols: AVCaptureVideoDataOutputSampleBufferDelegate and ProtocoloValiDas. The former receives every frame captured with the camera and the latter receives events from ValiDasLibrary.
 */
@interface GenericDocumentController : UIViewController <DocumentValidatorProtocol>
{
    int _frameNumber;
    CGSize _resolutionSet;
    state _restartState;
    float _progress;
    
    /**
     @brief Instance to which notify with the delegated methods
     @details Must implement the protocol VDGenericDocumentProtocol
     */
    UIViewController<VDGenericDocumentProtocol>* _delegate;
    
    /**
     @brief View in which the video capture will be recorded.
     */
    UIView* _captureView;
    
    /**
     @brief Capture session of the rear camera.
     */
    AVCaptureSession* _captureSession;
    
    /**
     @brief Capture device for the rear camera.
     */
    AVCaptureDevice* _captureDevice;
    
    /**
     @brief Video output for the rear camera.
     */
    AVCaptureVideoDataOutput* _video;
    
    /**
     @brief Video preview layer for the rear camera.
     */
    AVCaptureVideoPreviewLayer* _previewLayer;
    
    /**
     @brief Variable of the class states.
     */
    state _state;
    
    /**
     @brief Obverse without flash image data.
     */
    NSData *_obverseWithoutFlashImage;
    
    /**
     @brief Obverse with flash image data.
     */
    NSData *_obverseWithFlashImage;
    
    /**
     @brief Reverse without flash image data.
     */
    NSData *_reverseWithoutFlashImage;
    
    /**
     @brief Reverse with flash image data.
     */
    NSData *_reverseWithFlashImage;
    
    /**
     @brief View which contains the detection rectangle (not visible in release mode)
     */
    UIView *_myBox;
    
    /**
     @brief Arrows to help the user locate the document.
     */
    UIView *_myArrows;
    
    /**
     @brief Layer with the rectangle which circumscribe the document.
     */
    CALayer* _rectangle;
    
    /**
     @brief Variable to store the previous distance of the document.
     */
    PatternProximity _previousDistance;
    
    /**
     @brief Label with util information for the user.
     */
    UILabel* _informativeText;
    
    UILabel* _informativeTextNumber;
    ClosenessState _previousState;
    
    /**
     @brief Reusable alert which is shown each time the CPU is doing long time computations.
     */
    LoadingViewController *_loadingView;
    
    /**
     @brief Variable to store the Path of the document obversewithoutFlash
     */
    NSString* _obverseWithoutFlashPath;
    /**
     @brief Variable to store the Path of the document obverse with Flash
     */
    NSString* _obverseWithFlashPath;
    /**
     @brief Variable to store the Path of the document reverse without Flash
     */
    NSString* _reverseWithoutFlashPath;
    
    /**
     @brief Variable to store the Path of the document reverse with Flash
     */
    NSString* _reverseWithFlashPath;
    
    /**
     @brief Reference rect where to locate the document.
     */
    CGRect _referenceRect;
    
    /**
     @brief Document indication in the rectangle.
     */
    NSString* _documentIndication;
    
    /**
     @brief Variable to store the documentType its going to be searched
     */
    NSString *_documentType;
    NSArray<ValiDasDocumento*>* _arrayDocumentType;
    NSArray<ValiDasDocumento*>* _documentsSent;
    NSArray<NSString*>* _possibleDocuments;
    
    NSDictionary* _config;
    UIButton* _closebutton;
    NSTimer *_secondLevelHelpTimer;
    NSTimer *_countDownTimer;
    
    NSTimer *_shutterButtonTimer;
    NSTimer *_bubbleWindowTimer;

    UIButton *_shutterButton;
    BubbleWindowController *_shutterButtonInformativeWindow;
    BOOL _bubbleWindowIsShowing;
    BOOL _shutterButtonTimeIsFinished;
    BOOL _pictureWithButton;
    /**
     * True if detection can be done, false otherwise.
     */
    Boolean _canDetect;
    
    /**
     * True if flip animation is already done.
     */
    Boolean _alreadyFlipped;
}

- (void) setDelegate:(id) delegate;
- (void) setDocuments: (NSArray<ValiDasDocumento*>*) documentType;
- (void) setConfiguration: (NSDictionary*) config;
- (void) finishObjects;
- (BOOL) getObverseFlashConfig;
- (BOOL) getReverseFlashConfig;
//- (void) patternDistance:(PatternProximity)distancia;
- (void) updateUIImageReverse:(NSString*) docType;
- (void) updateUIImage:(NSString*) docType;
- (void) drawRect: (CGRect) rectIn fromImage:(CGSize) imageSize;
- (CGRect) resizeRectFrom4_3to16_9:(CGRect) region size43:(CGSize) size43 andSize169:(CGSize) size169;
- (void) showTick;
- (void) dismissAlertViewWithError: (int) error;
- (void) showActivityIndicatorWithTitle: (NSString *) title andRoulette: (BOOL) roulette;
- (void) showError:(NSUInteger)errorCode;
- (void) documentTypeFound:(NSArray<NSString *> *)docType;
- (UIColor *) getColorByDistance: (ClosenessState) distance;
-(void) hiddeShutterButtonAndBubbleWindow;
- (BOOL) shouldCaptureReverse;
- (void) startAutomaticCountDownWithState: (NSTimer*) sender;
- (void) changeFirstStateToAnalizing;
- (void) documentDetectedAndStartingCapture;
- (void) selectionOfprogressToLog:(float)progress;

#pragma mark - Step actions
-(void)callToDelegateAndRestartTimeWithImage:(NSData*)image captureType:(int)type;
-(void)callToDelegateWithImage:(NSData*)image captureType:(int)type;
-(NSMutableArray<ValiDasDocumento*>*)getArrayDocuments;
-(void)capturedReverseWithFlash;
-(void)capturedReverseWithNoFlash;
-(void)capturedObverseWithFlash;
-(void)capturedObverseWithNoFlash;
-(void)capturedPassport;

#pragma mark - DocumentValidatorProtocol
-(void)showValidationViewWithImage:(NSData*)image;

@end
