//
//  ControladorGeneric.m
//  vali-das
//
//  Copyright © 2017 das-nano. All rights reserved.
//

//#import "DNLog.h"
#import "GenericDocumentController.h"
#import "VDDocumentCapture.h"
#import "DocumentUtils.h"
#import "VDEnums.h"
#import <VDLibrary/ValiDas.h>
#import <UIKit/UIKit.h>
#import "DocumentUtils.h"
#import "BackgroundImageUtils.h"
#import "AlertLandscapeController.h"
#import "BubbleWindowController.h"
#import "PopUpDocumentViewController.h"
#import <VDLibrary/FirebaseTagProtocol.h>
#import "VDDocumentDB.h"
#import "Constants.h"
#import "BidiReaderController.h"
#import <VDLibrary/ValiDasPayloadBidiCode.h>
#import <VDLibrary/CustomConfiguration.h>
#import <VDLibrary/IntegrityCalculations.h>

//#import <VDLogger/VDLogger-Swift.h>

#ifndef DRAW_RECT
//#define DRAW_RECT
#endif

#define kBidiOpportunities 0
#define imageFixSize 2500

@interface GenericDocumentController () <AVCaptureVideoDataOutputSampleBufferDelegate, AVCapturePhotoCaptureDelegate,  ProtocoloValiDas, FirebaseTagProtocol> {
    BidiReaderController *_bidiReader;
    int _bidiOpportunities;
    BOOL _mustResetBidiReader;
    dispatch_queue_t _bidiThread;
    dispatch_queue_t cameraThread;
    dispatch_queue_t frameProcessingThread;
    BOOL _flashEnabled;
    BOOL processShouldFinish;
}
@property(nonatomic, strong) AVCapturePhotoOutput *imageCapture;
@end


@implementation GenericDocumentController

bool shouldLogProgress = true;

/**
 * Size to resize images taken.
 */
static CGSize RESIZE_SIZE;

/**
 * Quality to compres the JPG.
 */
static float QUALITY = 0.50;

/**
 * Duration of the flip animation,
 */
static float FLIP_DURATION = 1.0;
static float COUNTDOWN_DURATION_MULTIPLIER = 0.8;
static NSString* const AT_IDCard_2002 = @"AT_IDCard_2002";
static NSString* const AT_IDCard_2010 = @"AT_IDCard_2010";
static NSString* const MX_IDCard_2014 = @"MX_IDCard_2014";
static NSString* const MX_IDCard_2019 = @"MX_IDCard_2019";
static bool hasDisappeared = false;

/**
 @name Overriden methods
 */
///@{
#pragma mark - Overriden methods
/**
 @see https://developer.apple.com/library/ios/documentation/UIKit/Reference/UIViewController_Class/#//apple_ref/occ/instm/UIViewController/viewDidLoad
 */
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initThreads];
    [self initializePrivateVariables];
}

/**
 @see https://developer.apple.com/library/ios/documentation/UIKit/Reference/UIViewController_Class/#//apple_ref/occ/instm/UIViewController/viewWillAppear:
 */
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (processShouldFinish) {
        return;
    }
    
//        [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                            message:VIEW_WILL_APPEAR_LOG
//                            metadata:nil file:NSStringFromClass([self class])
//                            function:NSStringFromSelector(_cmd)
//                            line:__LINE__];
    
    [self customViewWillAppear];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    hasDisappeared = true;

    [self stopTimer];
    [self stopShutterButtonTimer];
    
//    [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                            message:VIEW_WILL_DISAPPEAR_LOG
//                            metadata:nil file:NSStringFromClass([self class])
//                            function:NSStringFromSelector(_cmd)
//                            line:__LINE__];
}

- (void)willEnterForeground {
    if (!_documentType) {
        _arrayDocumentType = _documentsSent;
    }
    [self customViewWillAppear];
}

- (void) didEnterBackground {
    [self stopSession];
    [self stopTimer];
    [self stopShutterButtonTimer];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}
/**
 @see https://developer.apple.com/library/ios/documentation/UIKit/Reference/UIViewController_Class/#//apple_ref/occ/instm/UIViewController/prefersStatusBarHidden
 */
- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}
/// @}

/**
 @name Private methods
 */
///@{
#pragma mark - Private methods
- (void) initializePrivateVariables {
    RESIZE_SIZE = CGSizeMake(3264, 2448); //  8Mpx camera
    
    _mustResetBidiReader = YES; // Bidi detection is disabled
    _bidiOpportunities = kBidiOpportunities;
    
    _frameNumber = 0;
    _restartState = SEARCHING_OBVERSE;
    _state = FINISHED_ANALYSIS;

    hasDisappeared = false;
}

- (void) initThreads {
    dispatch_queue_attr_t concurrentAttribute = dispatch_queue_attr_make_with_qos_class(
        DISPATCH_QUEUE_CONCURRENT, QOS_CLASS_USER_INITIATED, -1
    );
    dispatch_queue_attr_t serialAttribute = dispatch_queue_attr_make_with_qos_class(DISPATCH_QUEUE_SERIAL, QOS_CLASS_USER_INITIATED, -1
    );
    self->_bidiThread = dispatch_queue_create("veridas.document.bidi.queue", concurrentAttribute);
    self->cameraThread = dispatch_queue_create("veridas.document.camera.queue", serialAttribute);
    self->frameProcessingThread = dispatch_queue_create("veridas.document.frame.queue", serialAttribute);
}

- (void) startSession {
    // Do not put in the sync or a thread block will be caused!!!!
    [self stopSession];
    dispatch_sync(self->cameraThread, ^{
        if (self->_captureSession) {
            if (!self->_captureSession.isRunning) {
                [NSTimer scheduledTimerWithTimeInterval:0.3 repeats:NO block:^(NSTimer *timer) {
                    [self->_captureSession startRunning];
                }];
            }
        }
    });
}

- (void) stopSession {
    dispatch_sync(self->cameraThread, ^{
        if (self->_captureSession) {
            if (self->_captureSession.isRunning) {
                [self->_captureSession stopRunning];
            }
        }
    });
}

- (void)createCloseButton {
    dispatch_async(dispatch_get_main_queue(), ^{
        float buttonSize = (IS_IPAD ? PROPORTION_CLOSE_BUTTON_IPAD : PROPORTION_CLOSE_BUTTON)*self.view.bounds.size.width;
        float xButton = self.view.bounds.size.width - buttonSize - 5;
        float yButton = 10;
        float width = buttonSize/1.2;
        float height = buttonSize/1.2;

        if (@available( iOS 11.0, * )) {
            if ([[[UIApplication sharedApplication] keyWindow] safeAreaInsets].bottom > 0) {
                // iPhone with notch, add margin to save it
                yButton = [[[UIApplication sharedApplication] keyWindow] safeAreaInsets].top;
            }
        }
        self->_closebutton =  [[UIButton alloc] initWithFrame:CGRectMake(xButton, yButton, width, height)];
        
        [self->_closebutton setImage:[DocumentUtils getImageFromFilename:[self->_config objectForKey:@"closebuttonimage"]] forState:UIControlStateNormal];
        [self->_closebutton addTarget:self action:@selector(stopsdk) forControlEvents:UIControlEventTouchUpInside];
        
        if ([[self->_config objectForKey:@"closebutton"] isEqualToString:@"YES"]) {
            [self.view addSubview:self->_closebutton];
            [self.view bringSubviewToFront:self->_closebutton];
        }
    });
}

- (void) createShutterButton {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([[self->_config objectForKey:@"shutterbuttonshow"] isEqualToString:@"YES"]) {
            self->_shutterButton = [[UIButton alloc] init];
            bool isAvailableSize = self.view.frame.size.width * 0.20 < MAX_SHUTTER_BUTTON_SIZE;
            int sizeButton = isAvailableSize ? self.view.frame.size.width * 0.20 : MAX_SHUTTER_BUTTON_SIZE;
            int offset = 10;
            int x = self.view.frame.size.height - ((self.view.frame.size.height - self->_referenceRect.size.height) / 2) -  (sizeButton / 2 + offset);
            int y = self.view.frame.size.width / 2;
            [self->_shutterButton setFrame:CGRectMake(0, 0, sizeButton, sizeButton)];
            [self->_shutterButton setCenter:CGPointMake(y, x) ];
            self->_shutterButton.layer.cornerRadius = 0.5 * self->_shutterButton.bounds.size.width;
            self->_shutterButton.layer.borderWidth = self->_shutterButton.bounds.size.width * 0.1;
            self->_shutterButton.layer.borderColor = [DocumentUtils transformRGBColor:[self->_config objectForKey:@"shutterbuttonbordercolor"]].CGColor;
            [self->_shutterButton setBackgroundColor:[DocumentUtils transformRGBColor:[self->_config objectForKey:@"shutterbuttonbackgroundcolor"]]];
            [self->_shutterButton addTarget:self action:@selector(takePictureWithButton) forControlEvents:UIControlEventTouchUpInside];
            [self createBubbleWindowWithFocusPoint:CGPointMake(y - sizeButton / 2, x - sizeButton / 2)];
        }
    });
}

- (void) restartWaitingShutterButton {
    if ([[self->_config objectForKey:@"shutterbuttonshow"] isEqualToString:@"YES"]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!self->_pictureWithButton) {
                [self hiddeShutterButtonAndBubbleWindow];
                [self setVisibleInformativeText];
                self->_shutterButtonTimeIsFinished = false;
                [self stopShutterButtonTimer];
                if (self->_state == SEARCHING_OBVERSE) {
                    self->_shutterButtonTimer = [NSTimer scheduledTimerWithTimeInterval:[[self->_config objectForKey:@"secondswithoutshutterbuttonobverse"] intValue] target:self selector:@selector(shutterButtonTimeIsFinished) userInfo:nil repeats:NO];
                } else {
                    self->_shutterButtonTimer = [NSTimer scheduledTimerWithTimeInterval:[[self->_config objectForKey:@"secondswithoutshutterbuttonreverse"] intValue] target:self selector:@selector(shutterButtonTimeIsFinished) userInfo:nil repeats:NO];
                }
            } else {
                [self setHiddenShutterButtonInformativeWindow];
                [self showShutterButton];
            }
            [self->_bubbleWindowTimer invalidate];
        });
    }
}

- (void) hiddeShutterButtonAndBubbleWindow {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self->_shutterButton setHidden:true];
        [self->_shutterButtonInformativeWindow setHiddenView];
        self->_bubbleWindowIsShowing = false;
    });
}

- (void) showShutterButton {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self->_shutterButton setHidden:false];
        [self.view addSubview:self->_shutterButton];
        [self.view bringSubviewToFront:self->_shutterButton];
    });
 
}

- (void) showBubbleWindow {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!self->_bubbleWindowIsShowing && !self->_pictureWithButton) {
            self->_informativeText.hidden = YES;
            [self->_shutterButtonInformativeWindow setVisibleView];
            [self.view addSubview: self->_shutterButtonInformativeWindow];
            [self.view bringSubviewToFront:self->_shutterButtonInformativeWindow];
            [self bubbleWindowIsShowing];
            self->_bubbleWindowTimer = [NSTimer scheduledTimerWithTimeInterval:[[self->_config objectForKey:@"secondswithshutterbuttonmessage"] intValue] target:self selector:@selector(setHiddenShutterButtonInformativeWindow) userInfo:nil repeats:NO];
        } else {
            [self bubbleWindowIsNotShowing];
            [self setVisibleInformativeText];
        }
    });
}

- (void) setHiddenShutterButtonInformativeWindow{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self->_bubbleWindowIsShowing) {
            [self->_shutterButtonInformativeWindow setHiddenView];
            [self bubbleWindowIsNotShowing];
        }
        [self setVisibleInformativeText];
    });
}

- (void) shutterButtonTimeIsFinished{
    _shutterButtonTimeIsFinished = true;
}

- (void) createBubbleWindowWithFocusPoint:(CGPoint) focusPoint {
    dispatch_async(dispatch_get_main_queue(), ^{
        float textsize = IS_IPAD ? TEXT_SIZE_LABEL : TEXT_SIZE_LABEL * self.view.frame.size.height/667.0;
        self-> _shutterButtonInformativeWindow = [[BubbleWindowController alloc] initWithConfiguration:self->_config focusPoint:focusPoint screenSize:self.view.frame.size andTextSize:textsize];
    });
}

- (void) setVisibleInformativeText {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!self->_bubbleWindowIsShowing) {
            self->_informativeText.hidden = NO;
            self->_informativeText.alpha = 1.0;
        }
    });
}
- (void) bubbleWindowIsNotShowing {
    _bubbleWindowIsShowing = false;
}
- (void) bubbleWindowIsShowing {
    _bubbleWindowIsShowing = true;
}

- (void) finishObjects {
    _obverseWithoutFlashImage = nil;
    _obverseWithFlashImage = nil;
    _reverseWithoutFlashImage = nil;
    _reverseWithFlashImage = nil;
    _captureSession = nil;
    _captureDevice = nil;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self->_previewLayer) {
            [self->_previewLayer removeFromSuperlayer];
            self->_previewLayer = nil;
        }
        if (self->_myArrows) {
            [self->_myArrows removeFromSuperview];
            self->_myArrows = nil;
        }
        if (self->_captureView) {
            [self->_captureView removeFromSuperview];
            self->_captureView = nil;
        }
        if (self->_rectangle) {
            self->_rectangle.contents = nil;
            [self->_rectangle removeFromSuperlayer];
            self->_rectangle = nil;
        }
    });
    if (_loadingView) {
        _loadingView = nil;
    }
    
    _video = nil;
    _imageCapture = nil;
}

- (void)VDTimeWithoutPhotoTaken {
    VDCaptureType capturetype;
    if (_state == SEARCHING_OBVERSE) {
        capturetype = VD_OBVERSE_WITHOUT_FLASH;
        _restartState = _state;
    } else {
        capturetype = VD_REVERSE_WITHOUT_FLASH;
        _restartState = _state;
    }
    [_delegate VDTimeWithoutPhotoTaken:[[_config objectForKey:@"secondswithoutpicture"] intValue] withCaptureType:capturetype];
}

-(void)restartTimer {
    [self stopTimer];
    _secondLevelHelpTimer = [NSTimer scheduledTimerWithTimeInterval:[[_config objectForKey:@"secondswithoutpicture"] doubleValue] target:self selector:@selector(VDTimeWithoutPhotoTaken) userInfo:nil repeats:YES];
}

- (void) stopTimer {
    if (_secondLevelHelpTimer) {
        [_secondLevelHelpTimer invalidate];
        _secondLevelHelpTimer = nil;
    }
}

- (void) stopShutterButtonTimer {
    if (_shutterButtonTimer) {
        [_shutterButtonTimer invalidate];
        _shutterButtonTimer = nil;
    }
}

-(void)stopsdk {
//    [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                                message:CLOSE_BUTTON_LOG
//                                metadata:nil file:NSStringFromClass([self class])
//                                function:NSStringFromSelector(_cmd)
//                                line:__LINE__];
    _canDetect = false;
    
    [self stopTimer];
    [self stopShutterButtonTimer];
    
    [_delegate stopsdk];
}

- (void) showPermissionError {
//    [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                               message:PERMISIONS_REFUSED_LOG
//                               metadata:nil file:NSStringFromClass([self class])
//                               function:NSStringFromSelector(_cmd)
//                               line:__LINE__];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[_config objectForKey:@"permissionrefusedtitle"] message:[_config objectForKey:@"permissionrefused"] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * openSettings = [UIAlertAction actionWithTitle:[_config objectForKey:@"permissionrefusedbutton"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];}];
    [alert addAction:openSettings];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alert animated:YES completion:nil];
    });
}

- (void) customViewWillAppear {
    [self registerNotitifiacations];
    [self dismissLoadingAlert];
    [self clearAllViewsAndLayers];
    [self askForCameraPermission];
    [self resetBidiReaderIfNeeded];
    [self configureInitialInformativeTextAndIndicator];
}

- (void) resetBidiReaderIfNeeded {
    if (_mustResetBidiReader) {
        [self setBidiCodesToRead];
    }
}

- (void) configureInitialInformativeTextAndIndicator {
    if (_restartState == SEARCHING_OBVERSE) {
        _documentIndication = [self selectObverseText];
    } else if (_restartState == SEARCHING_REVERSE) {
        if ([self shouldCaptureReverse]) {
            _documentIndication = [_config objectForKey:@"reversenotfoundtext"];
        }
    }
}

- (NSString *) selectObverseText {
    if ([ValiDas isDocumentTypePassport:_documentType]) {
        return [_config objectForKey:@"passportnotfoundtext"];
    } else {
        return [_config objectForKey:@"obversenotfoundtext"];
    }
}

- (void) clearAllViewsAndLayers {
    dispatch_async(dispatch_get_main_queue(), ^{
        for (UIView* view in self.view.subviews) {
            [view removeFromSuperview];
        }
        
        for (CALayer* layer in [self.view.layer.sublayers copy]) {
            [layer removeFromSuperlayer];
        }
    });
}

- (void) askForCameraPermission {
    NSString *mediaType = AVMediaTypeVideo;
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    if (authStatus == AVAuthorizationStatusAuthorized) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showInfoAlert];
        });
    } else if (authStatus == AVAuthorizationStatusDenied) {
        [self showPermissionError];
    } else if (authStatus == AVAuthorizationStatusNotDetermined) {
        [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
            if (granted) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self showInfoAlert];
                });
            } else {
                [self showPermissionError];
            }
        }];
    }
}

- (void) registerNotitifiacations {
    NSNotificationCenter* defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    [defaultCenter removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [defaultCenter addObserver:self selector:@selector(willEnterForeground)
                                    name:UIApplicationWillEnterForegroundNotification
                                    object:nil];
    [defaultCenter addObserver:self selector:@selector(didEnterBackground)
                                    name:UIApplicationDidEnterBackgroundNotification
                                    object:nil];
}

- (void) showInfoAlert {
    if (_restartState == SEARCHING_REVERSE || (_restartState == SEARCHING_OBVERSE && hasDisappeared)) {
        hasDisappeared = false;
        [self startCaptureMode];
        return;
    }
    
    AlertLandscapeController *alert;
    if ([ValiDas isDocumentTypePassport:_documentType]) {
        alert = [AlertLandscapeController alertControllerWithTitle:[_config objectForKey:@"infoalerttitlepass"] message:[_config objectForKey:@"infoalertpass"] preferredStyle:UIAlertControllerStyleAlert];
    } else {
        alert = [AlertLandscapeController alertControllerWithTitle:[_config objectForKey:@"infoalerttitle"] message:[_config objectForKey:@"infoalert"] preferredStyle:UIAlertControllerStyleAlert];
    }
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:[_config objectForKey:@"positivebuttontitle"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self startCaptureMode];
    }];
    [alert addAction:okAction];
    
    if ([[_config objectForKey:@"infoalertshow"] isEqualToString:@"YES"]) {
        [self presentViewController:alert animated:NO completion:nil];
    } else {
        [self startCaptureMode];
    }
}

- (void) startCaptureMode {
    [self startCapture];
    [self restartCaptureView];
    [self restartTimer];
    [self restartWaitingShutterButton];
}

/**
 @brief Method to show an error message.
 @details Additionaly an animation is done when "OK" is pushed.
 @param errorCode (NSUInteger) Error code to show.
 */
- (void) showError:(NSUInteger)errorCode {
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            self->_informativeText.alpha = 0.0;
        } completion:nil];
        
        UIAlertController* errorController;
        
        if (errorCode == -1) {
            errorController = [UIAlertController alertControllerWithTitle: nil message:[NSString stringWithFormat:@"Existen brillos continuados en la imagen. Por favor, intente realizar el proceso en otra localización."] preferredStyle:UIAlertControllerStyleAlert];
        } else {
            errorController = [UIAlertController alertControllerWithTitle: nil message:[NSString stringWithFormat:@"No se ha podido validar el documento. Por favor, verifique que las condiciones de luz sean adecuadas."] preferredStyle:UIAlertControllerStyleAlert];
        }
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            errorController.view.transform = CGAffineTransformMakeRotation(M_PI_2);
            errorController.view.hidden = true;
            
            if ((self->_state == ANALIZING_OBVERSE_WITHOUT_FLASH) || (self->_state == ANALIZING_OBVERSE_WITH_FLASH)) {
                self->_state = SEARCHING_OBVERSE;
            }
            
            if ((self->_state == ANALIZING_REVERSE_WITHOUT_FLASH) || (self->_state == ANALIZING_REVERSE_WITH_FLASH)) {
                self->_state = SEARCHING_REVERSE;
            }
            
            [UIView animateWithDuration:1.0 delay:0.5 options:UIViewAnimationOptionCurveEaseIn animations:^{
                self->_informativeText.alpha = 1.0;
            } completion:nil];
            
            [self patternDistance:PATTERN_NOT_FOUND];
            [self startSession];
        }];
        
        [errorController addAction:okAction];
        errorController.view.hidden = true;
        
        [self presentViewController:errorController animated:YES completion:^{
            errorController.view.transform = CGAffineTransformMakeRotation(M_PI_2);
            errorController.view.hidden = false;
        }];
    });
}

- (void)showBidiError {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *errorController = [UIAlertController alertControllerWithTitle:nil message:[self->_config objectForKey:@"bidierrormessage"] preferredStyle:UIAlertControllerStyleAlert];
        errorController.view.hidden = YES;
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self stopSession];
            self->_mustResetBidiReader = NO;
            [self customViewWillAppear];
        }];
        
        self->_bidiOpportunities--;
        [errorController addAction:okAction];
        [self presentViewController:errorController animated:YES completion:^{
            errorController.view.transform = CGAffineTransformMakeRotation(M_PI_2);
            errorController.view.hidden = NO;
        }];
    });
}

/**
 @brief Method to show an alert with title and an activity indicator.
 @details This alert cannot be closed by the user.
 @param title (NSString *) Title to show in the alert.
 @param roulette (BOOL) True if a roulette is going to be shown.
 */
- (void)showActivityIndicatorWithTitle: (NSString *) title andRoulette: (BOOL) roulette {
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            self->_informativeText.alpha = 0.0;
        } completion:nil];
        
        [self removeTick];
        float textsize = IS_IPAD ? TEXT_SIZE_LABEL : TEXT_SIZE_LABEL * self.view.frame.size.height/667.0;
        self->_loadingView = [[LoadingViewController alloc] initWithConfig:self->_config title:title withSize:textsize animated:roulette];
//        _loadingView.view.frame = CGRectMake(0, 0, 200, 200);
        
        [DocumentUtils presentPopUpModal:self->_loadingView inController:self animated:NO];
    });
}

/**
 @brief Method to close the alert created with title and roulette
 @details This method is called delegated from ValiDas Library. If an error has ocurred another alert will be shown.
 @param error (int) Error caused by the app.
 */
- (void)dismissAlertViewWithError: (int) error {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self->_loadingView) {
            [self->_loadingView closeView];
            [self obtainingDataCompletionWithError:error];
        } else {
            if (error != -1) {
                [self showError:error];
            } else {
                [self dismissAlertWithoutError];
            }
        }
        [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            self->_informativeText.alpha = 1.0;
        } completion:nil];
    });
}

-(void)obtainingDataCompletionWithError:(int)error {
    self->_loadingView = nil;
    
    if (error != -1) {
        [self showError:error];
    } else {
        if (_state == ANALIZING_REVERSE_WITHOUT_FLASH || _state == ANALIZING_PASSPORT) {
            [VDDocumentCapture stop];
            [[NSNotificationCenter defaultCenter] removeObserver:self];
        } else if (_state == ANALIZING_OBVERSE_WITH_FLASH) {
            _state = SEARCHING_REVERSE;
            _restartState = _state;
            [self startSession];
        }
    }
    
    [self patternDistance:PATTERN_ERROR];
}

-(void)dismissAlertWithoutError {
    if (_state == ANALIZING_PASSPORT) {
        [VDDocumentCapture stop];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    } else if (_state == ANALIZING_OBVERSE_WITH_FLASH) {
        _state = SEARCHING_REVERSE;
        _restartState = _state;
        [self startSession];
    }
}

/**
 @brief Method to dismiss loading alert
 */
- (void)dismissLoadingAlert
{
    if (_loadingView) {
        [_loadingView closeView];
    }
}
/// @}


#pragma mark - Setters

-(void)setBidiCodesToRead
{
    if (![_bidiReader isSearchFinished]) {
        NSMutableArray *allBidiCodes = [[NSMutableArray alloc] init];
        
        for (ValiDasDocumento *doc in _documentsSent) {
            NSArray *bidiCodes = [ValiDas getBidiCodesForDocument:doc.type side:_restartState == SEARCHING_OBVERSE ? OBVERSE : REVERSE];
            
            if (bidiCodes) {
                [allBidiCodes addObject:bidiCodes];
            }
        }
        
        [_bidiReader setBidiCodesForDocuments:allBidiCodes];
    }
}

- (void) setConfiguration:(NSDictionary*) config{
    _config = config;
    
    _bidiReader = [[BidiReaderController alloc] init];
    [_bidiReader setConfiguration:_config];
}

- (void) setDocuments:(NSArray<ValiDasDocumento*>*) tipoDoc {
    _arrayDocumentType = tipoDoc;
    _documentsSent = tipoDoc;
    [self fillPossibleDocumentsWithDocumentsSent];
    if ([_arrayDocumentType count] == 1) {
        _documentType = [_arrayDocumentType objectAtIndex:0].type;
    }
}

- (void) setDelegate:(id)delegado{
    _delegate = delegado;
}

- (BOOL) getObverseFlashConfig {
    return [[_config objectForKey:@"obverseflash"] isEqualToString:@"YES"];
}

- (BOOL) getReverseFlashConfig {
    return [[_config objectForKey:@"reverseflash"] isEqualToString:@"YES"];
}

- (BOOL) getArrowsConfig {
    return [[_config objectForKey:@"arrows"] isEqualToString:@"YES"];
}

/**
 @name Private methods
 */
///@{
#pragma mark - Private methods

/**
 @brief Method to initiate the capture of the rear camera.
 @details This method loads, configures and opens the rear camera. It also associates the preview of the capture to a view for its correct visualization. It does not just configure for the preview but for the capture with and without flash and fixes the focus.
 */
- (void)startCapture {
    dispatch_sync(self->cameraThread, ^{
        self->_captureSession = [[AVCaptureSession alloc] init];

        if (self->_captureDevice) {
            self->_captureDevice = nil;
        }
        self->_captureDevice = [AVCaptureDevice defaultDeviceWithDeviceType:AVCaptureDeviceTypeBuiltInWideAngleCamera mediaType:AVMediaTypeVideo position:AVCaptureDevicePositionBack];
        if (self->_captureDevice) {
            NSError* error = nil;
            AVCaptureDeviceInput* captureInput = [AVCaptureDeviceInput deviceInputWithDevice:self->_captureDevice error:&error];
            if (!error) {
                self->_video = [AVCaptureVideoDataOutput new];
                self->_video.videoSettings = @{ (NSString *) kCVPixelBufferPixelFormatTypeKey : @(kCVPixelFormatType_32BGRA) };
                [self->_video setAlwaysDiscardsLateVideoFrames:true];
                [self->_video setSampleBufferDelegate:self queue: self->frameProcessingThread];
                
                [self initImageCapture];
                if ([self->_captureSession canAddInput:captureInput]) {
                    [self->_captureSession addInput:captureInput];
                }
                if ([self->_captureSession canAddOutput:self->_video]) {
                    [self->_captureSession addOutput:self->_video];
                }
                
                if ([self->_captureSession canAddOutput:self->_imageCapture]) {
                    [self->_captureSession addOutput:self->_imageCapture];
                }
                
               // self->_captureSession.sessionPreset = AVCaptureSessionPresetPhoto;
                self->_captureSession.sessionPreset = AVCaptureSessionPreset3840x2160;
                
                [self fixPreviewSize];
                [self fixCameraFocus];
            }
        }
    });
}

- (void) initImageCapture {
    self.imageCapture = [[AVCapturePhotoOutput alloc] init];
    self.imageCapture.highResolutionCaptureEnabled = YES;
}

/**
 @brief Method to draw the view elements
 @details Creates and draw in the view (visible or not) the necessary elements for better usability. Not all parameters are configured, there are some of them dinamic and it is better to adjust them in other methods.
 */
- (void) draw {
    dispatch_async(dispatch_get_main_queue(), ^{
        self->_rectangle = [[CALayer alloc] init];
        self->_rectangle.frame = self->_referenceRect;
        self->_rectangle.backgroundColor = [UIColor clearColor].CGColor;
        
        if ([ValiDas isDocumentTypeAtDrivingLicense2004:self->_documentType]) {
            self->_rectangle.cornerRadius = 0;
        } else {
            self->_rectangle.cornerRadius = 30;
        }
        self->_rectangle.opacity = 1.0;
        self->_rectangle.borderColor = [UIColor whiteColor].CGColor;
        self->_rectangle.borderWidth = [[self->_config objectForKey:@"fixedrectangle"] isEqualToString:@"NO"] ? 0 : 8;
        [self.view.layer addSublayer:self->_rectangle];
        
        UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:self->_referenceRect cornerRadius:self->_rectangle.cornerRadius];
        UIBezierPath *circlePath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, self->_captureView.frame.size.width, self->_captureView.frame.size.height) cornerRadius:0];
        [path appendPath:circlePath];
        [path setUsesEvenOddFillRule:YES];
        
        CAShapeLayer *fillLayer = [CAShapeLayer layer];
        fillLayer.path = path.CGPath;
        fillLayer.fillRule = kCAFillRuleEvenOdd;
        fillLayer.fillColor = [DocumentUtils transformRGBColor:[self->_config objectForKey:@"capturebackgroundcolor"]].CGColor;
        fillLayer.opacity = 0.8;
        [self.view.layer addSublayer:fillLayer];
        
        int h = self.view.frame.size.height / 2 - 60; // To avoid overlapping button
        int x = self->_captureView.frame.size.width / 2 - (h / 2);
        int width = [ValiDas isDocumentTypePassport:self->_documentType] ? 100 : 60;
        self->_informativeText = [[UILabel alloc] initWithFrame:CGRectMake(x, self->_captureView.frame.size.height / 2 - (width / 2), h, width)];
        self->_informativeText.alpha = 0.0;
        self->_informativeText.hidden = YES;
        self->_informativeText.textAlignment = NSTextAlignmentCenter;
        self->_informativeText.textColor = [DocumentUtils transformRGBColor:[self->_config objectForKey:@"informativetextcolor"]];
        self->_informativeText.backgroundColor = [DocumentUtils transformRGBColor:[self->_config objectForKey:@"backgroundcolor"]];
        self->_informativeText.lineBreakMode = NSLineBreakByWordWrapping;
        self->_informativeText.numberOfLines = 0;
        float textsize = IS_IPAD ? TEXT_SIZE_LABEL : TEXT_SIZE_LABEL * self.view.window.frame.size.height/667.0;
        self->_informativeText.font = [UIFont fontWithName:[self->_config objectForKey:@"familyName"] size:textsize];
        self->_informativeText.transform = CGAffineTransformMakeRotation(M_PI_2);
        [DocumentUtils convertToHTMLString:self->_documentIndication inLabel:self->_informativeText];
                
        self->_informativeTextNumber = [[UILabel alloc] initWithFrame:CGRectMake(self->_captureView.frame.size.width / 2 - 160, self->_captureView.frame.size.height / 2 - 50, 320, 100)];
        self->_informativeTextNumber.textAlignment = NSTextAlignmentCenter;
        self->_informativeTextNumber.text = @"";
        self->_informativeTextNumber.textColor = [DocumentUtils transformRGBColor:[self->_config objectForKey:@"countdowntextcolor"]];
        self->_informativeTextNumber.backgroundColor = [UIColor clearColor];
        self->_informativeTextNumber.alpha = 0.0;
        self->_informativeTextNumber.lineBreakMode = NSLineBreakByWordWrapping;
        self->_informativeTextNumber.numberOfLines = 0;
        float textsizeNum = INITIAL_SIZE_COUNTDOWN * self.view.window.frame.size.height / 667.0;
        [self->_informativeTextNumber setFont:[UIFont fontWithName:@"AvenirNext-DemiBold" size:textsizeNum]];
        self->_informativeTextNumber.transform = CGAffineTransformMakeRotation(M_PI_2);
        self->_informativeTextNumber.hidden = YES;
        [self.view addSubview:self->_informativeTextNumber];
        
        [self patternDistance:PATTERN_NOT_FOUND];
        [self startAnimations];
    });
}

/**
 @brief Method to make the animations of the elements to improve usability and user experience during the app.
 */
- (void) startAnimations {
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:1.0 delay:0.5 options:UIViewAnimationOptionCurveEaseIn animations:^{
            self->_informativeText.alpha = 1.0;
            self->_informativeText.transform = CGAffineTransformMakeRotation(M_PI_2);
        } completion:nil];
    });
}

/**
 @brief Method to restart the capture view
 */
- (void) restartCaptureView {
    [ValiDas setDelegate:self];
    dispatch_async(dispatch_get_main_queue(), ^{
        self->_state = self->_restartState;
        
        if (self->_captureView) {
            if ([self->_captureView isDescendantOfView:self.view]) {
                [self->_captureView removeFromSuperview];
            }
        }
        
        self->_captureView = [[UIView alloc] initWithFrame:self.view.frame];
        [self.view addSubview:self->_captureView];
        
        self->_referenceRect = [ValiDas getReferenceRegion:[ValiDas getStandardTypeForDocument:self->_documentType]];
        
        CGFloat width = self->_captureView.frame.size.width;
        CGFloat height = self->_captureView.frame.size.height;
        
        self->_previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:self->_captureSession];
        self->_previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        self->_previewLayer.frame = CGRectMake(0, 0, width, height);
        
        [self->_captureView.layer addSublayer:self->_previewLayer];
        
        [self draw];
        
        [self createShutterButton];
        [self createCloseButton];
        [self startSession];
        [self dismissLoadingAlert];
        [self startDocumentDelay];
        NSString *message = [NSString stringWithFormat:CAPTURE_PREVIEW_SIZE_LOG, self->_previewLayer.frame.size.width, self->_previewLayer.frame.size.height, self->_captureView.frame.size.width, self->_captureView.frame.size.height];
                   
//            [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                                message:message
//                                metadata:nil
//                                file:NSStringFromClass([self class])
//                                function:NSStringFromSelector(_cmd)
//                                line:__LINE__];
    });
}

/**
 * Starts document delayed detection.
 */
- (void) startDocumentDelay {
    NSString* key = @"obversedetectiondelay";
    _canDetect = false;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self->_rectangle.borderColor = [UIColor whiteColor].CGColor;
        self->_rectangle.contents = nil;
        if ([self->_informativeText isDescendantOfView:self.view]) {
            [self->_informativeText removeFromSuperview];
        }
        [self.view addSubview:self->_informativeText];
        [self.view bringSubviewToFront:self->_informativeText];
    });
    
    if (_restartState == SEARCHING_REVERSE || _restartState == ANALIZING_REVERSE_WITHOUT_FLASH
        || _restartState == ANALIZING_REVERSE_WITH_FLASH) {
        key = @"reversedetectiondelay";
        if ([self shouldCaptureReverse]) {
            if (!_alreadyFlipped) {
                _alreadyFlipped = true;
                [self flip];
            } else {
                if ([self->_arrayDocumentType count] == 1) {
                    [self updateUIImageReverse:self->_documentType];
                }
            }
        } else {
            // Do not start the timer.
            return;
        }
    } else {
        if ([self->_arrayDocumentType count] == 1) {
            [self updateUIImage:self->_documentType];
        }
    }
    
    [NSTimer scheduledTimerWithTimeInterval:[[_config objectForKey:key] intValue] target:self selector:@selector(documentDelayedFinished:) userInfo:nil repeats:NO];
}

/**
 * Delayed timer final selector.
 */
- (void) documentDelayedFinished: (NSTimer *) timer {
    _canDetect = true;
}

/**
 * Flips the rectangle when needed.
 */
- (void) flip {
    dispatch_async(dispatch_get_main_queue(), ^{
        CATransform3D transform = CATransform3DMakeRotation(M_PI, 1, 0, 0);
        transform.m34 = 1.0 / 350;
        
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform"];
        [animation setToValue:[NSValue valueWithCATransform3D:transform]];
        animation.duration = FLIP_DURATION;
        
        self->_rectangle.doubleSided = YES;
        self->_rectangle.zPosition = self->_rectangle.frame.size.width;
        [CATransaction begin];
        [CATransaction setCompletionBlock:^{
            if ([self->_arrayDocumentType count] == 1) {
                [self updateUIImageReverse:self->_documentType];
            }
//            self->_rectangle.zPosition = 0;
            self->_rectangle.zPosition = self->_rectangle.frame.size.width;
//            [self.view bringSubviewToFront:_informativeText];
        }];
        [self->_rectangle addAnimation:animation forKey:@"transform"];
        
        [CATransaction commit];
    });
}
    
/**
 @brief Method to fix the focus of the camera
 @details The focus is only fixed in case the device has access to it.
 */
- (void)fixCameraFocus {
    BOOL lock = [_captureDevice lockForConfiguration:nil];
    if (lock) {
        if ([_captureDevice isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus]) {
            [_captureDevice setFocusMode:AVCaptureFocusModeContinuousAutoFocus];
        }
    }
    [_captureDevice unlockForConfiguration];
}

- (void) fixPreviewSize {
    BOOL lock = [_captureDevice lockForConfiguration:nil];
    if (lock) {
        NSArray *supportedFormats = [_captureDevice formats];
        if (supportedFormats.count == 0) {
            [_captureDevice unlockForConfiguration];
            return;
        }
        for (AVCaptureDeviceFormat *format in supportedFormats) {
            CMVideoDimensions dimensions = CMVideoFormatDescriptionGetDimensions((CMVideoFormatDescriptionRef)[format formatDescription]);
            float resolution = (float)dimensions.width / dimensions.height;
            if (dimensions.width >= imageFixSize && fabs(resolution - 4.0 / 3) < 0.1) {
               // NSLog(@"tamaño 2500");
                [_captureDevice setActiveFormat:format];
                break;
            }
        }
        
    }
    [_captureDevice unlockForConfiguration];
}

/**
 @brief Method used to compute the color to show in the frame that surrounds the face.
 @details In this case the HSV color space has been selected because it was easier to compute the different colors. Nevertheless, any other color space (i.e. RGB) can be used.
 @param distance (CercaniaPatron) Distance to the face.
 @return (UIColor *) Proper color to the distance of the face.
 */
- (UIColor *) getColorByDistance: (ClosenessState) distance {
    NSDictionary *ovalcolor;
    UIColor *actualColor;
    switch (distance) {
        case NOTFOUND:
            return COLOR_ERROR;
        case STARTED_CORRECT_DETECTION:
            ovalcolor = [_config objectForKey:@"documentdetectedoklevel3color"];
            actualColor = [DocumentUtils transformRGBColor:ovalcolor];
            return actualColor;
        case HALF_CORRECT_DETECTION:
            ovalcolor = [_config objectForKey:@"documentdetectedoklevel2color"];
            actualColor = [DocumentUtils transformRGBColor:ovalcolor];
            return actualColor;
        case FULL_CORRECT_DETECTION:
            ovalcolor = [_config objectForKey:@"documentdetectedoklevel1color"];
            actualColor = [DocumentUtils transformRGBColor:ovalcolor];
            return actualColor;
        case FAR_DETECTION:
            ovalcolor = [_config objectForKey:@"documentdetectedveryfarcolor"];
            actualColor = [DocumentUtils transformRGBColor:ovalcolor];
            return actualColor;
        case CLOSE_DETECTION:
            ovalcolor = [_config objectForKey:@"documentdetectedveryclosecolor"];
            actualColor = [DocumentUtils transformRGBColor:ovalcolor];
            return actualColor;
        case INVISIBLE:
            return COLOR_ERROR;
    }
}

-(UIColor*)getUIColorFormConfigKey:(NSString*)key
{
    return [DocumentUtils transformRGBColor:[self->_config objectForKey:key]];
}

- (BOOL) areVariousDocuments {
    return ([_arrayDocumentType count] > 1);
}


#pragma mark - Detection Actions

/**
 @brief Method to capture the obverse with flash image.
 */
- (void)takePicture: (BOOL) flash {
    [self setFlash:flash];
    AVCapturePhotoSettings* settings = [self configureCapture];
    
    dispatch_sync(self->cameraThread, ^{
        [self.imageCapture capturePhotoWithSettings:settings delegate:self];
    });
}

-(void)documentObverseDetected {
//    [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                                message:DOCUMENT_OBVERSE_DETECTED
//                                metadata:nil file:NSStringFromClass([self class])
//                                function:NSStringFromSelector(_cmd)
//                                line:__LINE__];
    
    [self documentDetectedAndStartingCapture];
}

-(void)documentReverseDetected {
//    [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                                  message:DOCUMENT_REVERSE_DETECTED
//                                  metadata:nil file:NSStringFromClass([self class])
//                                  function:NSStringFromSelector(_cmd)
//                                  line:__LINE__];

    [self documentDetectedAndStartingCapture];
}

- (void)passportDetected {
//    [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                                    message:DOCUMENT_PASSPORT_DETECTED
//                                    metadata:nil file:NSStringFromClass([self class])
//                                    function:NSStringFromSelector(_cmd)
//                                    line:__LINE__];
    
    [self documentDetectedAndStartingCapture];
}

- (void) documentDetectedAndStartingCapture {
    _pictureWithButton = false;
    [self changeFirstStateToAnalizing];
    dispatch_async(dispatch_get_main_queue(), ^{
         [NSTimer scheduledTimerWithTimeInterval:0
                     target:self
                     selector:@selector(startAutomaticCountDownWithState:)
                                        userInfo:@(COUNTDOWN_3) repeats:NO];
    });
}
/**
 @brief Method to set the flash mode on or off. This method has to be called from cameraThread.
 @returns (AVCapturePhotoSettings *) The settings of the camera with the desired flash mode.
 */
- (AVCapturePhotoSettings *)configureCapture {
    [self configureImageRotation];
    AVCapturePhotoSettings *photoSettings = [AVCapturePhotoSettings photoSettings];
    
    if (self->_flashEnabled) {
        if ([self.imageCapture.supportedFlashModes containsObject:@(AVCaptureFlashModeOn)]) {
            photoSettings.flashMode = AVCaptureFlashModeOn; //WITH FLASH
        }
    } else {
        photoSettings.flashMode = AVCaptureFlashModeOff; //WITHOUT FLASH
    }
    
    if (photoSettings.availablePreviewPhotoPixelFormatTypes.count > 0) {
        photoSettings.previewPhotoFormat = @{ (NSString *)kCVPixelBufferPixelFormatTypeKey : photoSettings.availablePreviewPhotoPixelFormatTypes[0] }; // The first format in the array is the preferred format
    }
    
    photoSettings.autoStillImageStabilizationEnabled = YES;
    if (@available(iOS 10.2, *)) {
        photoSettings.autoDualCameraFusionEnabled = NO;
    }
    photoSettings.highResolutionPhotoEnabled = YES;
    return photoSettings;
}

- (void) configureImageRotation {
    dispatch_sync(self->cameraThread, ^{
        AVCaptureConnection* captureConection = [self.imageCapture connectionWithMediaType:AVMediaTypeVideo];
        if ([captureConection isVideoOrientationSupported]) {
            [captureConection setVideoOrientation:AVCaptureVideoOrientationLandscapeRight];
        }
    });
}

-(void) takePictureWithoutFlash {
    [self showTick];
    if (self->_state == ANALIZING_OBVERSE_WITHOUT_FLASH && [self getObverseFlashConfig]) {
        [self showActivityIndicatorWithTitle:[self->_config objectForKey:@"flashwaitingalert"] andRoulette:true];
    }
    [self takePicture:false];
}

-(void) changeFirstStateToAnalizing {
    [self hiddeShutterButtonAndBubbleWindow];
    if (_state == SEARCHING_OBVERSE) {
        if ([ValiDas isDocumentTypePassport:_documentType]) {
            _state = ANALIZING_PASSPORT;
        } else {
            _state = ANALIZING_OBVERSE_WITHOUT_FLASH;
        }
    }
    if (_state == SEARCHING_REVERSE) {
        _state = ANALIZING_REVERSE_WITHOUT_FLASH;
    }
}

-(void) takePictureWithButton {
//    [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                                message:SHUTTER_BUTTON_LOG
//                                metadata:nil file:NSStringFromClass([self class])
//                                function:NSStringFromSelector(_cmd)
//                                line:__LINE__];
    
    _pictureWithButton = true;
    [self changeFirstStateToAnalizing];
    [self takePictureWithoutFlash];
}

-(void) fillPossibleDocumentsWithDocumentsSent {
    NSMutableArray* setPossibleDocs = [NSMutableArray new];
    for (ValiDasDocumento *doc in _documentsSent) {
        [setPossibleDocs addObject:doc.type];
    }
    _possibleDocuments = [[NSArray alloc] initWithArray:setPossibleDocs];
}


/// @}

/**
 @name Métodos delegados
 Métodos implementados gracias a los protocolos a los cuales se adhiere esta clase. La documentación de estos métodos está localizada en sus propias clases.
 */
///@{
#pragma mark - Delegated Methods
/**
 @see https://developer.apple.com/library/ios/documentation/AVFoundation/Reference/AVCaptureVideoDataOutputSampleBufferDelegate_Protocol/#//apple_ref/occ/intfm/AVCaptureVideoDataOutputSampleBufferDelegate/captureOutput:didOutputSampleBuffer:fromConnection:
 */
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {
     if (_canDetect) {
         // Se analiza la imagen en busca de documento.
         UIImage* image = [DocumentUtils imageFromSampleBuffer:sampleBuffer];
         CGRect region = CGRectNull;
         
         if (_state == SEARCHING_OBVERSE) {
             region = [ValiDas searchDocumentsWithDocs:_arrayDocumentType analysisType:OBVERSE_WITHOUT_FLASH image:image andSearchType:DOCUMENT];
         } else if (_state == SEARCHING_REVERSE) {
             region = [ValiDas searchDocumentsWithDocs:_arrayDocumentType analysisType:REVERSE_WITHOUT_FLASH image:image andSearchType:DOCUMENT];
         }
         
         if (!CGRectIsNull(region)) {
             [self searchBidiCodesInImage:image];
         }
         
         [self drawRegion:region withSize:image.size];
     }
 }



- (void)captureOutput:(AVCapturePhotoOutput *)captureOutput didFinishProcessingPhotoSampleBuffer:(CMSampleBufferRef)rawSampleBuffer previewPhotoSampleBuffer:(CMSampleBufferRef)previewPhotoSampleBuffer resolvedSettings:(AVCaptureResolvedPhotoSettings *)resolvedSettings bracketSettings:(AVCaptureBracketedStillImageSettings *)bracketSettings error:(NSError *)error {
    if (error != nil) {
        return;
    }
    
    if (_state == ANALIZING_OBVERSE_WITHOUT_FLASH) {
        if (!_flashEnabled) {
            _obverseWithoutFlashImage = [self getResizedJpgImageForSampleBuffer:rawSampleBuffer andPreviewPhotoSampleBuffer:previewPhotoSampleBuffer];
            
            [self capturedObverseWithNoFlash];
        } else {
            [self stopSession];
            [_loadingView closeView];
            _obverseWithFlashImage = [self getResizedJpgImageForSampleBuffer:rawSampleBuffer andPreviewPhotoSampleBuffer:previewPhotoSampleBuffer];
            [self capturedObverseWithFlash];
        }
    } else if (_state == ANALIZING_REVERSE_WITHOUT_FLASH) {
        if (!_flashEnabled) {
            _reverseWithoutFlashImage = [self getResizedJpgImageForSampleBuffer:rawSampleBuffer andPreviewPhotoSampleBuffer:previewPhotoSampleBuffer];
            [self capturedReverseWithNoFlash];
        } else {
            _reverseWithFlashImage = [self getResizedJpgImageForSampleBuffer:rawSampleBuffer andPreviewPhotoSampleBuffer:previewPhotoSampleBuffer];
            [self capturedReverseWithFlash];
        }
    } else if (_state == ANALIZING_PASSPORT) {
        [self stopSession];
        _obverseWithoutFlashImage = [self getResizedJpgImageForSampleBuffer:rawSampleBuffer andPreviewPhotoSampleBuffer:previewPhotoSampleBuffer];
        [self capturedPassport];
    }
}

-(void)searchBidiCodesInImage:(UIImage*)image {
    dispatch_async(_bidiThread, ^{
        [self->_bidiReader searchForBidiCodesInImage:image];
    });
}

-(void)drawRegion:(CGRect)region withSize:(CGSize)size {
    if (++_frameNumber % 3 == 0) {
        _frameNumber = 0;
        [self drawRect:region fromImage:size];
    }
}

- (CGRect) resizeRectFrom4_3to16_9:(CGRect) region size43:(CGSize) size43 andSize169:(CGSize) size169{
    size169 = CGSizeMake(size169.height, size169.width);
    region = CGRectMake(size43.width * region.origin.x, size43.height * region.origin.y, size43.width * region.size.width, size43.height * region.size.height);
    float xRatio = size43.width / size169.width;
    float diffY = fabs((size43.height / xRatio) - size169.height) / 2;
    region = CGRectMake((region.origin.x / xRatio),
                        (region.origin.y / xRatio) - diffY,
                        region.size.width / xRatio,
                        region.size.height / xRatio);
    return region;
}

- (void) drawRect: (CGRect) rectIn fromImage:(CGSize) imageSize{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self->_myBox) {
            if ([self->_myBox isDescendantOfView:self.view]) {
                [self->_myBox removeFromSuperview];
            }
        }
        if (self->_myArrows) {
            if ([self->_myArrows isDescendantOfView:self.view]) {
                [self->_myArrows removeFromSuperview];
            }
        }
        
        if (CGRectIsInfinite(rectIn) || CGRectIsEmpty(rectIn)) {
            return;
        }
        
        CGRect reg = [self resizeRectFrom4_3to16_9:rectIn size43:imageSize andSize169:self->_captureView.frame.size];

        int offset = 0;
        if ((fabs((imageSize.width / imageSize.height) - 16.0 / 9)) <= 0.05) {
            offset = -30;
        }
        
        CGRect rect = CGRectMake(self->_captureView.frame.size.width - (reg.size.height) - (reg.origin.y) + offset, reg.origin.x, reg.size.height, reg.size.width);
        
        if (!CGRectIsNull(rect)) {
#ifdef DRAW_RECT
            self->_myBox = [[UIView alloc] initWithFrame:rect];
            self->_myBox.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
            self->_myBox.layer.borderColor = [UIColor blueColor].CGColor;
            self->_myBox.layer.borderWidth = 2;
            [self->_captureView addSubview:self->_myBox];
#endif
            if (self->_previousDistance == PATTERN_EXTREMELY_FAR) {
                if ([self getArrowsConfig] && ![self isNonIntelligentGroupOfDocumets]) {
                    self->_myArrows  = [[UIView alloc] initWithFrame:self.view.frame];
                    [self getCornerArrowsFrom:rect To:self->_rectangle.frame withBorderRadius:self->_rectangle.cornerRadius];
                    [self->_captureView addSubview:self->_myArrows];
                }
            }
        }
    });
}

-(BOOL) isNonIntelligentGroupOfDocumets{
    if (_state == SEARCHING_REVERSE) {
        return [ValiDas isThereSomeNonIntelligentDocument:_arrayDocumentType analysisType:REVERSE_WITHOUT_FLASH];
    } else if (_state == SEARCHING_OBVERSE){
        return [ValiDas isThereSomeNonIntelligentDocument:_arrayDocumentType analysisType:OBVERSE_WITHOUT_FLASH];
    }
    return true;
}

- (void) getCornerArrowsFrom:(CGRect)rect1 To:(CGRect)rect2 withBorderRadius:(CGFloat) borderRadius
{
    UIColor *color1 = [self getUIColorFormConfigKey:@"arrowcolor1"], *color2 = [self getUIColorFormConfigKey:@"arrowcolor2"];
    UIColor *color3 = [self getUIColorFormConfigKey:@"arrowcolor3"], *color4 = [self getUIColorFormConfigKey:@"arrowcolor4"], *color5 = [self getUIColorFormConfigKey:@"arrowcolor5"];
    ArrowColors colors = {color1.CGColor, color2.CGColor, color3.CGColor, color4.CGColor, color5.CGColor};
    NSArray *arrows = [DocumentUtils getCornerArrowsFrom:rect1 To:rect2 withBorderRadius:borderRadius arrowColors:colors];
    
    [_myArrows.layer addSublayer:[arrows objectAtIndex:0]];
    [_myArrows.layer addSublayer:[arrows objectAtIndex:1]];
    [_myArrows.layer addSublayer:[arrows objectAtIndex:2]];
    [_myArrows.layer addSublayer:[arrows objectAtIndex:3]];
    [_myArrows.layer addSublayer:[arrows objectAtIndex:4]];
}

- (void) showTick {
    [self setClosenessState:INVISIBLE];
    
    UIImage* tick = [self createTick];

    dispatch_async(dispatch_get_main_queue(), ^{
        UIImageView* tickImageView = [[UIImageView alloc] initWithImage:tick];
        tickImageView.frame = CGRectMake(0, 0, self.view.bounds.size.height / 10, self.view.bounds.size.height / 10);
        tickImageView.center = CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height / 2);
        tickImageView.tag = TICK_TAG;
        
        [self.view addSubview:tickImageView];
        [self.view bringSubviewToFront:tickImageView];
    });
}

- (void)removeTick {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[self.view viewWithTag:TICK_TAG] removeFromSuperview];
    });
}

- (UIImage *) createTick {
    __block UIImage* final;
    dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        UIImage* circle = [UIImage imageWithContentsOfFile:[[NSBundle bundleForClass:self.class] pathForResource:@"circle.png" ofType:nil]];
        UIColor* circleColor = [self getUIColorFormConfigKey:@"tickcirclecolor"];
        circle = [self modifyImage:circle withColor:circleColor];
        
        UIImage* tick = [UIImage imageWithContentsOfFile:[[NSBundle bundleForClass:self.class] pathForResource:@"tick.png" ofType:nil]];
        UIColor* tickColor = [self getUIColorFormConfigKey:@"tickcolor"];
        tick = [self modifyImage:tick withColor:tickColor];
        
        CGSize size = circle.size;
        CGRect rect = CGRectMake(0, 0, size.width, size.height);
        
        UIGraphicsBeginImageContext(size);
        
        [circle drawInRect:rect];
        [tick drawInRect:rect];
        
        final = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
    });
    return final;
}

- (UIImage *) modifyImage: (UIImage *) image withColor: (UIColor *) color {
    CGSize size = image.size;
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    
    CGFloat red, green, blue, alpha;
    [color getRed:&red green:&green blue:&blue alpha:&alpha];
    
    UIGraphicsBeginImageContext(size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(context, red, green, blue, alpha);
    CGContextFillRect(context, rect);
    CGContextTranslateCTM(context, 0, size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextSetBlendMode(context, kCGBlendModeDestinationIn);
    CGContextDrawImage(context, rect, image.CGImage);
    
    UIImage* finalImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return finalImage;
}

#pragma mark - End Capture methods

-(void) stopCapture {
    [self stopSession];
    dispatch_async(dispatch_get_main_queue(), ^{
        [VDDocumentCapture stop];
        hasDisappeared = false;
    });
}

- (BOOL) shouldCaptureReverse {
    BOOL hasReverse = true;
    if (_possibleDocuments.count > 0) {
        hasReverse = [VDDocumentDB someDocumentHasReverse:_possibleDocuments];
    } else {
        if (_documentsSent.count == 1) {
            hasReverse = [_documentsSent objectAtIndex:0].reverse;
        }
    }
    return hasReverse && [[_config objectForKey:@"onlyobverse"] isEqualToString:@"NO"];
}

-(void)analysisFinished:(ResultType)result {
    _previousDistance = PATTERN_ERROR;
    
    if (_state == ANALIZING_OBVERSE_WITHOUT_FLASH) {
        if (result == ANALYSIS_OK) {
            _state = ANALIZING_OBVERSE_WITH_FLASH;
            [self analysisFinished:ANALYSIS_OK];
        } else {
            [self dismissAlertViewWithError:(int)result];
        }
    } else if (_state == ANALIZING_OBVERSE_WITH_FLASH) {
        if (result == ANALYSIS_OK) {
            if (![self shouldCaptureReverse]) {
                [self stopCapture];
            } else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [DocumentUtils convertToHTMLString:[self->_config objectForKey:@"reversenotfoundtext"] inLabel:self->_informativeText];
                    [self dismissAlertViewWithError:-1];
                });
            }
        } else {
            [self dismissAlertViewWithError:(int)result];
        }
    } else if (_state == ANALIZING_REVERSE_WITHOUT_FLASH) {
        if (result == ANALYSIS_OK) {
            _rectangle.contents = nil;
            [self dismissAlertViewWithError:-1];
            
        } else {
            [self dismissAlertViewWithError:(int)result];
        }
    }
}

-(void)patternDistance:(PatternProximity)proximity {
    if (_previousDistance != proximity) {
        ClosenessState state = NOTFOUND;
        if (proximity == PATTERN_NOT_FOUND || proximity == PATTERN_ERROR) {
            state = NOTFOUND;
        } else if (proximity == PATTERN_VERY_CLOSE) {
            state = FULL_CORRECT_DETECTION;
        } else if (proximity == PATTERN_MID_PROXIMITY) {
            state = HALF_CORRECT_DETECTION;
        }else if (proximity == PATTERN_FAR){
            state = STARTED_CORRECT_DETECTION;
        } else if (proximity == PATTERN_EXTREMELY_CLOSE) {
            state = CLOSE_DETECTION;
        } else if (proximity == PATTERN_EXTREMELY_FAR) {
            state = FAR_DETECTION;
        }
        [self setClosenessState:state];
        _previousDistance = proximity;
        _previousState = state;
    }
    [self showAndHiddeShutterButton:proximity];
}

-(void) showAndHiddeShutterButton:(PatternProximity)distancia {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([[self->_config objectForKey:@"shutterbuttonshow"] isEqualToString:@"YES"]) {
            if (distancia == PATTERN_VERY_CLOSE) {
                if (self->_shutterButtonTimeIsFinished && !self->_shutterButton.isHidden) {
                    [self hiddeShutterButtonAndBubbleWindow];
                }
            }
            if (distancia != PATTERN_VERY_CLOSE && distancia != PATTERN_MID_PROXIMITY && [self isSearching]) {
                if (self->_shutterButtonTimeIsFinished && self->_shutterButton.isHidden) {
                    [self showShutterButton];
                    [self showBubbleWindow];
                }
            }
        }
    });
}

-(BOOL) isSearching{
    if (_state == SEARCHING_OBVERSE || _state == SEARCHING_REVERSE) {
        return true;
    }
    return false;
}

- (void) startAutomaticCountDownWithState:(NSTimer*) sender{
    NSString* text = @"";
    int countdownIntValue = [sender.userInfo intValue];
    float seconds = _state == (SEARCHING_OBVERSE || _state == ANALIZING_OBVERSE_WITHOUT_FLASH || _state == ANALIZING_OBVERSE_WITH_FLASH) ? [CustomConfiguration getSecondsForObverseCountdown:_possibleDocuments] : [CustomConfiguration getSecondsForReverseCountdown:_possibleDocuments];
    seconds /= 3; // The previous value are the seconds for the whole capture.
    
    switch (countdownIntValue) {
        case COUNTDOWN_1:
            text = @"1";
            [NSTimer scheduledTimerWithTimeInterval:seconds
                                            target:self
                                            selector:@selector(takePictureWithoutFlash)
                                            userInfo:nil repeats:NO];
            break;
        case COUNTDOWN_2:
            text = @"2";
            [NSTimer scheduledTimerWithTimeInterval:seconds
                                    target:self
                                    selector:@selector(startAutomaticCountDownWithState:)
                                    userInfo:@(COUNTDOWN_1) repeats:NO];
            break;
        case COUNTDOWN_3:
            text = @"3";
            [NSTimer scheduledTimerWithTimeInterval:seconds
                                             target:self
                                           selector:@selector(startAutomaticCountDownWithState:)
                                           userInfo:@(COUNTDOWN_2) repeats:NO];
            break;
        default:
            break;
    }
     dispatch_async(dispatch_get_main_queue(), ^{
         if (self->_informativeTextNumber.isHidden) {
             self->_informativeTextNumber.hidden = false;
         }
         if (!self->_informativeText.isHidden) {
             self->_informativeText.hidden = true;
         }
         self->_informativeTextNumber.text = text;
         self->_informativeTextNumber.transform = CGAffineTransformScale(CGAffineTransformRotate(CGAffineTransformIdentity, M_PI_2), 1.25, 1.25);
         self->_informativeTextNumber.alpha = 0.0;
         [UIView animateWithDuration:seconds * COUNTDOWN_DURATION_MULTIPLIER animations:^{
             self->_informativeTextNumber.transform = CGAffineTransformScale(CGAffineTransformRotate(CGAffineTransformIdentity, M_PI_2), 2.0, 2.0);
             self->_informativeTextNumber.alpha = 1.0;
         } completion:^(BOOL finished) {}];
     });
    
}


- (void) setClosenessState:(ClosenessState) state {
    CABasicAnimation* animacion = [CABasicAnimation animationWithKeyPath:@"borderColor"];
    UIColor* colorAnterior = [self getColorByDistance:_previousState];
    UIColor* colorActual = [self getColorByDistance:state];
    animacion.fromValue = (id)colorAnterior.CGColor;
    animacion.toValue = (id)colorActual.CGColor;
    animacion.duration = 1;
    
    NSString* text = @"";
    
    if (_previousState != state) {
        switch (state) {
            case FAR_DETECTION:
                text = [_config objectForKey:@"documentveryfar"];
                break;
            case CLOSE_DETECTION:
                text = [_config objectForKey:@"documentveryclose"];
                break;
            case NOTFOUND:
                if (_canDetect) {
                    if (_state == SEARCHING_OBVERSE) {
                        text = [self selectObverseText];
                    } else if (_state == SEARCHING_REVERSE) {
                        text = [_config objectForKey:@"reversenotfoundtext"];
                    }
                }
                break;
            case INVISIBLE:
                 text = @"";
                break;
            case FULL_CORRECT_DETECTION:
                break;
            case HALF_CORRECT_DETECTION:
                break;
            case STARTED_CORRECT_DETECTION:
                break;
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        if (![ValiDas isDocumentTypeAtDrivingLicense2004:self->_documentType] || (state != FAR_DETECTION && state != CLOSE_DETECTION)){
            [self->_rectangle addAnimation:animacion forKey:@"borderColor"];
            self->_rectangle.borderColor = colorActual.CGColor;
        }
      
        if (state == INVISIBLE) {
            self->_informativeTextNumber.hidden = YES;
            self->_informativeText.hidden = YES;
        } else {
            if (![ValiDas isDocumentTypeAtDrivingLicense2004:self->_documentType] || (state != FAR_DETECTION && state != CLOSE_DETECTION)) {
                 if (![text isEqualToString:@""]) {
                     [DocumentUtils convertToHTMLString:text inLabel:self->_informativeText];
                 }
                self->_informativeTextNumber.hidden = YES;
                [self setVisibleInformativeText];
                
                self->_informativeText.transform = CGAffineTransformRotate(CGAffineTransformIdentity, M_PI_2);
                [self->_informativeText layoutIfNeeded];
                [self.view bringSubviewToFront:self->_informativeText];
             }
        }
    });
}

- (NSArray<NSString *>*) addSomeDocuments: (NSArray<NSString *>*) documentIds toList: (NSArray<NSString*>*) originalList andSide: (state) side {
    NSMutableArray<NSString *>* finalList = [NSMutableArray new];
    
    [finalList addObjectsFromArray:documentIds];
    
    for (NSString* documentId in documentIds) {
        if ([documentId isEqual: AT_IDCard_2002] &&
            ![finalList containsObject:AT_IDCard_2010] &&
            [originalList containsObject:AT_IDCard_2010]) {
            [finalList addObject:AT_IDCard_2010];
        } else if ([documentId isEqual: AT_IDCard_2010] &&
                   ![finalList containsObject:AT_IDCard_2002] &&
                   [originalList containsObject:AT_IDCard_2002]) {
            [finalList addObject:AT_IDCard_2002];
        }

        if (side == SEARCHING_OBVERSE || side == ANALIZING_OBVERSE_WITHOUT_FLASH || side == ANALIZING_OBVERSE_WITH_FLASH) {
            if ([documentId isEqual: MX_IDCard_2014] &&
                       ![finalList containsObject:MX_IDCard_2019] &&
                       [originalList containsObject:MX_IDCard_2019]) {
                [finalList addObject:MX_IDCard_2019];
            } else if ([documentId isEqual: MX_IDCard_2019] &&
                       ![finalList containsObject:MX_IDCard_2014] &&
                       [originalList containsObject:MX_IDCard_2014]) {
                [finalList addObject:MX_IDCard_2014];
            }
        }
    }

    return finalList;
}

- (void) documentTypeFound:(NSArray<NSString *> *)docType {
    if (_restartState != SEARCHING_REVERSE) {
        if ([self areVariousDocuments]) {
            if (docType != NULL) {
                _possibleDocuments = [self addSomeDocuments:docType toList:_possibleDocuments andSide:_restartState];
            }
            _documentType = [docType objectAtIndex:0];
            [self updateUIImage:_documentType];
        }
    } else {
         if ([self areVariousDocuments]) {
             _possibleDocuments = [self addSomeDocuments:docType toList:_possibleDocuments andSide:_restartState];
            _documentType = [docType objectAtIndex:0];
            [self updateUIImageReverse:_documentType];
         }
    }
}

-(void)setFlash:(BOOL)flash {
    _flashEnabled = flash;
}

- (void) updateUIImageReverse:(NSString*)docType {
    if ([[_config objectForKey:@"fixedtemplate"] isEqualToString:@"YES"]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self->_rectangle.contents = [BackgroundImageUtils getUIImageReverse:docType];
        });
    }
}

- (void) updateUIImage:(NSString*)docType {
    if ([[_config objectForKey:@"fixedtemplate"] isEqualToString:@"YES"]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self->_rectangle.contents = [BackgroundImageUtils getUIImageObverse:docType];
        });
    }
}


- (void) fpsCalculated:(int)fps {
}

/// @}


#pragma mark - Step methods

-(NSMutableArray<ValiDasDocumento*>*)getArrayDocuments {
    NSMutableArray<ValiDasDocumento*> *reverseDocuments = [NSMutableArray new];
    
    for (ValiDasDocumento* docItem in _arrayDocumentType) {
        for (NSString* possibleItem in _possibleDocuments) {
            if ([docItem.type isEqualToString:possibleItem]) {
                [reverseDocuments addObject:docItem];
            }
        }
    }
    return reverseDocuments;
}

-(NSData*)getResizedJpgImageForSampleBuffer:(CMSampleBufferRef)rawSampleBuffer andPreviewPhotoSampleBuffer:(CMSampleBufferRef)previewPhotoSampleBuffer {
    
    NSString* artifactName = _state == ANALIZING_REVERSE_WITH_FLASH || _state == ANALIZING_REVERSE_WITHOUT_FLASH ? ARTIFACT_CLASS_REVERSE : ARTIFACT_CLASS_OBVERSE;
    
    NSData* fullImage = [AVCapturePhotoOutput JPEGPhotoDataRepresentationForJPEGSampleBuffer:rawSampleBuffer previewPhotoSampleBuffer:previewPhotoSampleBuffer];
    
    UIImage *resizedImage = [DocumentUtils resizeImage:[UIImage imageWithData:fullImage] toSmallerSize:RESIZE_SIZE];
    
    NSData* compressedData = UIImageJPEGRepresentation(resizedImage, QUALITY);
    NSData* compressedWithExtraData = [self addExtraDataToImage: compressedData andArtifactName: artifactName];
    
    return [self addMetadataToImage: compressedWithExtraData withArtifactName:artifactName];
}


-(void)callToDelegateWithImage:(NSData*)image captureType:(int)type {
    _canDetect = false;
    NSMutableArray<VDDocument *>* documents = [NSMutableArray new];
    for (NSString* document in _possibleDocuments) {
        [documents addObject:[VDDocumentDB getDocumentFromString:document]];
    }
    
//        [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                                    message:[POSSIBLE_DOCUMENTS_LOG stringByAppendingString:[NSString stringWithFormat:@"%@",_possibleDocuments]]
//                                   metadata:nil
//                                   file:NSStringFromClass([self class])
//                                   function:NSStringFromSelector(_cmd)
//                                   line:__LINE__];

    
    [_delegate VDDocumentCaptured:image withCaptureType:type andDocumentType:documents];
}

- (NSData *) addExtraDataToImage: (NSData *) data andArtifactName: (NSString *) name {
    NSData* extraData = [IntegrityCalculations hashStringWithInput:[IntegrityCalculations toData:name] andAlgorithm:SHA_256];
    NSMutableData* mutableData = [data mutableCopy];
    [mutableData appendData:extraData];
    return mutableData;
}


-(NSData*)addMetadataToImage:(NSData*)imageData withArtifactName: (NSString *) name {
    Artifact* artifact = [Artifact new];
    artifact.imageData = imageData;
    artifact.nameClass = name;
    UserAgent* agent = [UserAgent new];
    agent.name = kUserAgentSDKDocument;
    agent.version = [VDDocumentCapture getVersion];
    NSMutableArray *payloads = [[NSMutableArray alloc] init];
    
    for (ValiDasBidiResult *result in _bidiReader.getResults) {
        ValiDasPayloadBidiCode *payload = [[ValiDasPayloadBidiCode alloc] initWithElement:result];
        [payloads addObject:payload];
    }
    
    if (_bidiReader.getResults.count > 0) {
       // [_bidiReader cleanResults];
    }
    
    return [ValiDas addVeridasMetadataWithArtifact:artifact payloads:payloads userAgent:agent];
}

-(void)callToDelegateAndRestartTimeWithImage:(NSData*)image captureType:(int)type {
    [self callToDelegateWithImage:image captureType:type];
    [self restartTimer];
}

-(void)capturedObverseWithNoFlash {
//    [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                                message:DOCUMENT_OBVERSE_CAPTURED_LOG
//                                metadata:nil file:NSStringFromClass([self class])
//                                function:NSStringFromSelector(_cmd)
//                                line:__LINE__];
    
    if ([self getObverseFlashConfig] && _state != ANALIZING_PASSPORT) {
        [self takePicture:true];
    } else {
        [self showValidationViewWithImage:_obverseWithoutFlashImage];
    }
}

-(void)validatedObverseWithNoFlash {
    [self callToDelegateAndRestartTimeWithImage:_obverseWithoutFlashImage captureType:VD_OBVERSE_WITHOUT_FLASH];
    _state = ANALIZING_OBVERSE_WITH_FLASH;
    
    _arrayDocumentType = [self getArrayDocuments];
    if (_arrayDocumentType.count == 0) {
        _arrayDocumentType = _documentsSent;
    } //TODO: Maybe delete
    
    [self analysisFinished:ANALYSIS_OK];
}

-(void)capturedObverseWithFlash {
    
//    [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                                message:DOCUMENT_OBVERSE_WITH_FLASH_CAPTURED_LOG
//                                metadata:nil file:NSStringFromClass([self class])
//                                function:NSStringFromSelector(_cmd)
//                                line:__LINE__];
    [self showValidationViewWithImage:_obverseWithoutFlashImage];
}

-(void)validatedObverseWithFlash {
    [self callToDelegateAndRestartTimeWithImage:_obverseWithoutFlashImage captureType:VD_OBVERSE_WITHOUT_FLASH];
    [self callToDelegateAndRestartTimeWithImage:_obverseWithFlashImage captureType:VD_OBVERSE_WITH_FLASH];
    
    _arrayDocumentType = [self getArrayDocuments];
    if (_arrayDocumentType.count == 0) {
        _arrayDocumentType = _documentsSent;
    } //TODO: Maybe delete
    
    [self stopSession];
    [self analysisFinished:ANALYSIS_OK];
}

-(void)capturedReverseWithNoFlash {
//    [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                                message:DOCUMENT_REVERSE_CAPTURED_LOG
//                                metadata:nil file:NSStringFromClass([self class])
//                                function:NSStringFromSelector(_cmd)
//                                line:__LINE__];
    if ([self getReverseFlashConfig]) {
        [self takePicture:true];
    } else {
        [self showValidationViewWithImage:_reverseWithoutFlashImage];
    }
}

-(void)validatedReverseWithNoFlash {
    [self stopCapture];
    [self callToDelegateAndRestartTimeWithImage:_reverseWithoutFlashImage captureType:VD_REVERSE_WITHOUT_FLASH];
}

-(void)capturedReverseWithFlash
{
//    [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                                message:DOCUMENT_REVERSE_WITH_FLASH_CAPTURED_LOG
//                                metadata:nil file:NSStringFromClass([self class])
//                                function:NSStringFromSelector(_cmd)
//                                line:__LINE__];
    
    [self showValidationViewWithImage:_reverseWithoutFlashImage];
}

-(void)validatedReverseWithFlash {
    [self stopCapture];
    [self callToDelegateAndRestartTimeWithImage:_reverseWithoutFlashImage captureType:VD_REVERSE_WITHOUT_FLASH];
    [self callToDelegateAndRestartTimeWithImage:_reverseWithFlashImage captureType:VD_REVERSE_WITH_FLASH];
    [self analysisFinished:ANALYSIS_OK];
}

-(void)capturedPassport {
    [self showValidationViewWithImage:_obverseWithoutFlashImage];
}

-(void)validatedPassport {
    [self callToDelegateWithImage:_obverseWithoutFlashImage captureType:VD_OBVERSE_WITHOUT_FLASH];
    _state = ANALIZING_PASSPORT;
    [self analysisFinished:ANALYSIS_OK];
    [self stopCapture];
}


#pragma mark - DocumentValidatorProtocol

-(void)showValidationViewWithImage:(NSData*)image {
    if ([self presentedViewController]) {
        [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    }
    
    [self checkReadBidisAndShowValidation:image];
//    [self performSelector:@selector(checkReadBidisAndShowValidation:) withObject:image afterDelay:0];
}

-(void)checkReadBidisAndShowValidation:(NSData*)image {
    BOOL areAllCodesRead = [_bidiReader areAllCodesReadForReadDocument:_possibleDocuments side:_restartState == SEARCHING_OBVERSE ? OBVERSE : REVERSE];
    
    if (!areAllCodesRead && _bidiOpportunities > 0) {
        [self removeTick];
        [self showBidiError];
        return;
    }
    
    if ([[_config objectForKey:@"showdocument"] isEqualToString:@"YES"]) {
        DocumentValidatorViewController *validatorVC = [[DocumentValidatorViewController alloc] initWithConfiguration:_config delegate:self imageData:image];
        [validatorVC setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:validatorVC animated:YES completion:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self removeTick];
                self->_rectangle.contents = nil;
            });
        }];
    } else {
        [self removeTick];
        [self processValidation:YES];
        [self viewWillAppear:NO]; // This line fixs the issue when showdocument is NO
        //[self customViewWillAppear];
    }
    if (_pictureWithButton) {
        [self fillPossibleDocumentsWithDocumentsSent];
    }
}

- (void)imageValidated:(BOOL)validated {
    [self processValidation:validated];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:NO completion:nil];
    });
}

-(void)processValidation:(BOOL)validated {
    switch (_state) {
        case ANALIZING_OBVERSE_WITHOUT_FLASH:
            if (![self shouldCaptureReverse]) {
                processShouldFinish = validated;
            }
            if (![self getObverseFlashConfig]) {
                if (validated) {
                    _restartState = SEARCHING_REVERSE;
                    [self validatedObverseWithNoFlash];
                }
            } else {
                if (validated) {
                    _restartState = SEARCHING_REVERSE;
                    [self validatedObverseWithFlash];
                } else {
                    _restartState = SEARCHING_OBVERSE;
                    [self takePicture:false];
                }
            }
            [self restartWaitingShutterButton];
            break;
        case ANALIZING_REVERSE_WITHOUT_FLASH:
            processShouldFinish = validated;
            if (![self getReverseFlashConfig]) {
                if (validated) {
                    [self validatedReverseWithNoFlash];
                } else {
                    _restartState = SEARCHING_REVERSE;
                }
            } else {
                if (validated) {
                    [self validatedReverseWithFlash];
                } else {
                    [self takePicture:false];
                }
            }
            [self restartWaitingShutterButton];
            break;
        default:
            if ([ValiDas isDocumentTypePassport:_documentType]) {
                processShouldFinish = validated;
                if (validated) {
                    [self validatedPassport];
                } else {
                    [self setFlash:NO];
                }
            }
            [self restartWaitingShutterButton];
            break;
    }
}

#pragma mark - FirebaseTagProtocol

-(void)iterationDetectionMode:(detectorMode)mode {
}

-(void)typeDocumentDetected:(NSString*)type {
}

-(void)rectangleDetected:(BOOL)detected {
}

-(void)documentDetectionProgress:(float)progress {
    [self selectionOfprogressToLog:progress];
    
    _progress = progress;
}

- (void) selectionOfprogressToLog:(float)progress {
    if ((progress > 0) && (shouldLogProgress)) {
//         [[CommonLog shared] logWithLevel:LOGGER_DEBUG
//                                message:[DOCUMENT_PROGRESS_LOG stringByAppendingString:[NSString stringWithFormat:@"%f",progress]]
//                               metadata:nil
//                               file:NSStringFromClass([self class])
//                               function:NSStringFromSelector(_cmd)
//                               line:__LINE__];
    }
    shouldLogProgress = !shouldLogProgress;
}

@end
