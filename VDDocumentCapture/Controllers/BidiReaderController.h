//
//  BidiReaderController.h
//  VDDocumentCapture
//
//  Copyright © 2020 das-nano. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <VDLibrary/ValiDas.h>

NS_ASSUME_NONNULL_BEGIN

@interface BidiReaderController : NSObject

@property(nonatomic) BOOL noBidiCodesToSearch;
@property(nonatomic, strong) NSMutableArray *bidiCodesRead;
@property(nonatomic, strong) NSArray<NSNumber*> *bidiTypesToSearch;
@property(nonatomic, strong) NSMutableArray *combinationsToSearch;
@property(nonatomic, strong) NSMutableArray<ValiDasBidiResult*> *allResults;

-(BOOL)allCodesRead;
-(BOOL)areAllCodesReadForReadDocument:(NSArray*)docs side:(SideType)side;
-(BOOL)isSearchFinished;
-(void)setSearchFinished;
-(void)setSearchFinishedValue:(BOOL)value;

/**
@brief This function sets the framework configuration to use inside this class.
@param configuration (NSDictionary*) framework configuration.
*/
-(void)setConfiguration:(NSDictionary*)configuration;

/**
@brief This function analyze the bidi codes to read.
@param bidiCodes (NSArray<NSArray<ValiDasBidiCode*>*>*) array for each document with the ValiDasBidiCode to search for.
*/
-(void)setBidiCodesForDocuments:(NSArray<NSArray<ValiDasBidiCode*>*>*)bidiCodes;

/**
@brief This function searchs bidi codes for all the possible documents set.
@param image (UIImage*) the image to search for bidi codes.
*/
-(void)searchForBidiCodesInImage:(UIImage*)image;

/**
@brief This function returns an array of ValiDasBidiResult read.
*/
-(NSArray<ValiDasBidiResult*>*)getResults;

/**
@brief This function cleans all saved results.
*/
-(void)cleanResults;

@end

NS_ASSUME_NONNULL_END
