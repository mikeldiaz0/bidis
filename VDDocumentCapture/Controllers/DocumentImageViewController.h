//
//  DocumentImageViewController.h
//  VDDocumentCapture
//
//  Copyright © 2019 das-nano. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DocumentImageViewController : UIViewController

-(id)initWithConfiguration:(NSDictionary*)config image:(UIImage*)image;

@end
