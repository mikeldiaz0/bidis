//
//  PopUpDocumentViewController.m
//  VDDocumentCapture
//
//  Copyright © 2018 das-nano. All rights reserved.
//

#import "PopUpDocumentViewController.h"
#import "VDConfig.h"
#import "DocumentUtils.h"

@interface PopUpDocumentViewController ()

@property(nonatomic, strong) NSString *message;
@property(nonatomic, strong) NSDictionary *config;
@property(nonatomic, strong) IBOutlet UILabel *labelMessage;
@property(nonatomic, strong) IBOutlet UIView *viewContainer;

@end

@implementation PopUpDocumentViewController

-(id)initWithMessage:(NSString*)message config:(NSDictionary*)config
{
    self = [super initWithNibName:@"PopUpDocumentViewController" bundle:[NSBundle bundleForClass:PopUpDocumentViewController.class]];
    
    if (self) {
        self.message = message;
        self.config = config;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.labelMessage.font = [UIFont fontWithName:[self.config objectForKey:@"familyName"] size:15];
    [DocumentUtils convertToHTMLString:self.message inLabel:self.labelMessage];
    self.labelMessage.textColor = [DocumentUtils transformRGBColor:[self.config objectForKey:@"popupvalidationtextcolor"]];
    self.viewContainer.backgroundColor = [DocumentUtils transformRGBColor:[self.config objectForKey:@"popupvalidationbackgroundcolor"]];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
        self.view.transform = CGAffineTransformMakeRotation(M_PI_2);
    }
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
