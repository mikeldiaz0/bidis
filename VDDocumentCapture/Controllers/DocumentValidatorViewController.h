//
//  DocumentValidatorViewController.h
//  VDDocumentCapture
//
//  Copyright © 2018 das-nano. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VDEnums.h"

@protocol DocumentValidatorProtocol <NSObject>

@required
-(void)imageValidated:(BOOL)validated;

@end

@interface DocumentValidatorViewController : UIViewController

-(id)initWithConfiguration:(NSDictionary*)config delegate:(UIViewController<DocumentValidatorProtocol>*)delegate imageData:(NSData*)imageData;

@end
