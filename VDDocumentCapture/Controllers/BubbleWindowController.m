//
//  BubbleWindowController.m
//  VDDocumentCapture
//
//  Copyright © 2018 das-nano. All rights reserved.
//
#import "BubbleWindowController.h"
#import "VDConfig.h"
#import "DocumentUtils.h"
#import "Constants.h"

@interface BubbleWindowController ()

@property(nonatomic, strong) NSString *message;
@property(nonatomic, strong) NSDictionary *config;
@end
UILabel *labelMessage;
UIView *viewContainer;
CGRect rectangle;
#define MAX_HEIGHT 130;
#define MAX_WIDTH 355;

@implementation BubbleWindowController

-(id)initWithConfiguration:(NSDictionary*)config focusPoint:(CGPoint)focuspoint screenSize:(CGSize)size andTextSize:(CGFloat)textsize
{
    self = [super init];//[super initWithNibName:@"BubbleWindowController" bundle:[NSBundle bundleForClass:BubbleWindowController.class]];
    
    if (self) {
        self.config = config;
        self.message = [self.config objectForKey:@"shutterbuttonmessage"];
        CGSize finalSize = [self calculateWindowSize:size];

        viewContainer = [[UIView alloc] initWithFrame:CGRectMake(0  , 0 , finalSize.width, finalSize.height)];
        [viewContainer setCenter:CGPointMake(focuspoint.x + finalSize.height*1.6 ,focuspoint.y - finalSize.width*0.4)];
        rectangle = viewContainer.frame;
        labelMessage = [[UILabel alloc] initWithFrame:CGRectMake(+10, +10, finalSize.width-20, finalSize.height-20)];
        labelMessage.textAlignment = NSTextAlignmentCenter;
        labelMessage.numberOfLines = 0;
        labelMessage.adjustsFontSizeToFitWidth = YES;
        labelMessage.font = [UIFont fontWithName:[_config objectForKey:@"familyName"] size:textsize];
        labelMessage.lineBreakMode = NSLineBreakByWordWrapping;
        labelMessage.textColor = [DocumentUtils transformRGBColor:[self.config objectForKey:@"shutterbuttonmessagetextcolor"]];
        [DocumentUtils convertToHTMLString:self.message inLabel:labelMessage];
        
        [viewContainer.layer addSublayer:[self bubbleLayer]];
        [viewContainer addSubview:labelMessage];
        
        viewContainer.backgroundColor = [UIColor clearColor];
        viewContainer.layer.borderWidth = 0;
        if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
            viewContainer.transform = CGAffineTransformMakeRotation(M_PI_2);
        }
        [self addSubview:viewContainer];
       
    }
    return self;
}

-(void) setHiddenView{
    if(!self.isHidden){
        [self setHidden:true];
    }
}
-(void) setVisibleView{
    if(self.isHidden){
        [self setHidden:false];
    }
}

-(CGSize) calculateWindowSize:(CGSize)size{
    bool isAvailableWidth = size.height * 0.37 < MAX_WIDTH;
    bool isAvailableHeight = size.width * 0.24 < MAX_HEIGHT;
    int width = isAvailableWidth ? size.height * 0.37 : MAX_WIDTH;
    int height = isAvailableHeight ? size.width * 0.24 : MAX_HEIGHT;
    return CGSizeMake( width,height);
    
}

- (CALayer *)bubbleLayer
{
    CALayer *bubbleLayer = [CALayer layer];
   
    bubbleLayer.frame =  CGRectMake(0, 0, rectangle.size.width, rectangle.size.height);
    
    UIBezierPath *path = [self bubblePathWithRoundedCornerRadius];
    
    CAShapeLayer *fillLayer = [[CAShapeLayer alloc] init];
    [fillLayer setPath:path.CGPath];
    [fillLayer setFillColor:[DocumentUtils transformRGBColor:[self.config objectForKey:@"shutterbuttonmessagebackgroundcolor"]].CGColor];
    [fillLayer setStrokeColor:nil];
    fillLayer.borderWidth = 0;

    [bubbleLayer addSublayer:fillLayer];
    return bubbleLayer;
}

- (UIBezierPath *)bubblePathWithRoundedCornerRadius
{
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    int arrowPosition = rectangle.size.width - 30;
    CGPoint arrowMiddleBase = CGPointMake(arrowPosition, rectangle.size.height);
    int arrowWidth = rectangle.size.width*0.28;
    int arrowHeight = rectangle.size.height*0.30;
    // Start at the arrow
    [path moveToPoint:CGPointMake(arrowMiddleBase.x - arrowWidth / 2, arrowMiddleBase.y)];
    [path addLineToPoint:CGPointMake(arrowMiddleBase.x, arrowMiddleBase.y + arrowHeight)];
    [path addLineToPoint:CGPointMake(arrowMiddleBase.x , arrowMiddleBase.y )];
    // Rectangle
    [path addLineToPoint:CGPointMake(rectangle.size.width, rectangle.size.height)];
    [path addLineToPoint:CGPointMake(rectangle.size.width, 0)];
    [path addLineToPoint:CGPointMake(0, 0)];
    [path addLineToPoint:CGPointMake(0 , rectangle.size.height)];
    [path closePath];
    return path;
}


@end

