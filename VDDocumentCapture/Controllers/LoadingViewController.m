//
//  LoadingViewController.m
//  VDDocumentCapture
//
//  Copyright © 2019 das-nano. All rights reserved.
//

#import "LoadingViewController.h"
#import "DocumentUtils.h"
#import "VDConfig.h"
#import "UIImage+animatedGIF.h"

@interface LoadingViewController ()

@property(nonatomic) BOOL isAnimated;
@property BOOL isAnimating;
@property BOOL isGif;
@property(nonatomic, strong) NSString *alertTitle;
@property(nonatomic, strong) NSDictionary *config;
@property float titleSize;


@property(nonatomic, strong) IBOutlet UILabel *labelText;
@property(nonatomic, strong) IBOutlet UIImageView *imageViewAnimation;
@property (weak, nonatomic) IBOutlet UIView *background;

@end

@implementation LoadingViewController

-(id)initWithConfig:(NSDictionary*)config title:(NSString*)title withSize:(float)size animated:(BOOL)animated
{
    self = [super initWithNibName:@"LoadingViewController" bundle:[NSBundle bundleForClass:LoadingViewController.class]];
    
    if (self) {
        self.isAnimated = animated;
        self.config = config;
        self.alertTitle = title;
        self.titleSize = size;
        
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    self.background.backgroundColor = [DocumentUtils transformRGBColor:[self.config objectForKey:@"flashwaitingalertbgcolor"]];
    
    if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
        self.view.transform = CGAffineTransformMakeRotation(M_PI_2);
    }
    
    self.labelText.textAlignment = NSTextAlignmentCenter;
    self.labelText.lineBreakMode = NSLineBreakByWordWrapping;
    self.labelText.numberOfLines = 0;
    self.labelText.font = [UIFont fontWithName:[self.config objectForKey:@"familyName"] size:self.titleSize];
    self.labelText.textColor = [DocumentUtils transformRGBColor:[self.config objectForKey:@"flashwaitingalerttextcolor"]];

    [DocumentUtils convertToHTMLString:self.alertTitle inLabel:self.labelText];
    if (self.isAnimated) {
        NSString* fileName = [self.config objectForKey:@"flashwaitingalertanimationimage"];
        UIImage *animationImage;
        if ([fileName containsString:@".gif"]) {
            self.isGif = true;
            NSString *path = [DocumentUtils getPathFromFilename:[NSString stringWithFormat:@"%@", fileName]];
            NSURL *url = [NSURL fileURLWithPath:path];
            animationImage = [UIImage animatedImageWithAnimatedGIFURL:url];
        } else {
            animationImage = [DocumentUtils getImageFromFilename:fileName];
        }
        
        if (animationImage) {
            self.imageViewAnimation.image = animationImage;
        } else {
            [self removeImage];
        }
    } else {
        [self removeImage];
    }
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.isAnimated) {
        if (_imageViewAnimation.image) {
            self.isAnimating = true;
            [self animateSpinner];
        }
    }
}

- (void) animateSpinner {
    if (self.isGif) {
        return;
    }
    [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
        self.imageViewAnimation.transform = CGAffineTransformRotate(self.imageViewAnimation.transform, M_PI);
    } completion:^(BOOL finished) {
        if (self.isAnimating) {
            [self animateSpinner];
        }
    }];
}

- (void) removeImage {
    self.isAnimating = false;
    [self.imageViewAnimation.layer removeAllAnimations];
    [self.imageViewAnimation removeConstraints:self.imageViewAnimation.constraints];
    self.imageViewAnimation.frame = CGRectZero;
    self.imageViewAnimation.bounds = CGRectZero;
    self.imageViewAnimation.image = nil;
}

-(void)closeView {
    self.isAnimating = false;
    [self.imageViewAnimation.layer removeAllAnimations];
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
