//
//  TutorialViewController.h
//  VDDocumentCapture
//
//  Copyright © 2019 das-nano. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TutorialProtocol

@optional
-(void)tutorialClosed;

@end

@interface DocumentTutorialViewController : UIViewController

-(id)initWithDelegate:(id<TutorialProtocol>)delegate config:(NSDictionary*)config andDeviceModel:(NSString*)model;

@end
