//
//  TutorialViewController.m
//  VDDocumentCapture
//
//  Copyright © 2019 das-nano. All rights reserved.
//

#import "DocumentTutorialViewController.h"
#import "DocumentUtils.h"
#import "UIImage+animatedGIF.h"
static  int const TOP_MARGIN_OFFSET_WITH_NOTCH = 10;
static  int const TOP_MARGIN_OFFSET_TO_TEXT_WITHOUT_NOTCH = 25;
static  int const TOP_MARGIN_OFFSET_TO_GIF_WITHOUT_NOTCH = 20;

@interface DocumentTutorialViewController ()
@property(nonatomic, strong) NSDictionary *config;
@property(nonatomic, strong) id<TutorialProtocol>delegate;


@property(nonatomic, strong) IBOutlet UILabel *labelTitle;
@property(nonatomic, strong) IBOutlet UILabel *labelText;
@property(nonatomic, strong) IBOutlet UIImageView *imageGif;
@property(nonatomic, strong) IBOutlet UIButton *buttonContinue;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topGifConstraint;


@end

@implementation DocumentTutorialViewController

-(id)initWithDelegate:(id<TutorialProtocol>)delegate config:(NSDictionary*)config andDeviceModel:(NSString*)model
{
    self = [super initWithNibName:@"DocumentTutorialViewController" bundle:[NSBundle bundleForClass:DocumentTutorialViewController.class]];
 
    if (self) {
        self.config = config;
        self.delegate = delegate;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setColors];
    [self setTexts];
    [self setGif];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Orientation

- (BOOL) shouldAutorotate {
    return NO;
}


- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}


#pragma mark - Actions

- (IBAction) pressContinue {
    
    [self.delegate tutorialClosed];
}


#pragma mark - UI Config

- (void) setColors {
    self.view.backgroundColor = [self getUIColorFromConfigKey:@"tutorialbackgroundcolor"];
    
    self.buttonContinue.backgroundColor = [DocumentUtils transformRGBColor:[self.config objectForKey:@"tutorialcontinuebuttoncolor"]];
    [self.buttonContinue setTitleColor:[DocumentUtils transformRGBColor: [self.config objectForKey:@"tutorialcontinuebuttontextcolor"]] forState:UIControlStateNormal];
    
    [self.labelTitle setTextColor:[self getUIColorFromConfigKey:@"tutorialtitlecolor"]];
    [self.labelText setTextColor:[self getUIColorFromConfigKey:@"tutorialtextcolor"]];
}

- (void) setTexts {
    self.labelText.font = [UIFont fontWithName:[self.config objectForKey:@"familyName"] size:18];
    self.labelTitle.font = [UIFont fontWithName:[self.config objectForKey:@"familyName"] size:20];
    self.buttonContinue.titleLabel.font = [UIFont fontWithName:[self.config objectForKey:@"familyName"] size:17];

    [DocumentUtils convertToHTMLString:[self.config objectForKey:@"tutorialtext"] inLabel:self.labelText];
    [DocumentUtils convertToHTMLString:[self.config objectForKey:@"tutorialtitle"] inLabel:self.labelTitle];
    [self.buttonContinue setTitle:[[self.config objectForKey:@"tutorialcontinuebuttontext"] uppercaseString] forState:UIControlStateNormal];
}

- (void) setGif {
    NSString *path = [DocumentUtils getPathFromFilename:[NSString stringWithFormat:@"%@", [self.config objectForKey:@"tutorialgif"]]];
    if (path) {
        NSURL *url = [NSURL fileURLWithPath:path];
        self.imageGif.image = [UIImage animatedImageWithAnimatedGIFURL:url];
        float topOffset = [DocumentUtils getNochtSize] != 0 ? [DocumentUtils getNochtSize] : TOP_MARGIN_OFFSET_TO_GIF_WITHOUT_NOTCH;
        self.topGifConstraint.constant = topOffset;
    } else {
        [self removeGif];
    }
}

- (void) removeGif {
    [self.imageGif removeConstraints:self.imageGif.constraints];
    self.imageGif.frame = CGRectZero;
    self.imageGif.bounds = CGRectZero;
    self.imageGif.image = nil;
    [self addTopConstraint];
}

- (void) addTopConstraint {
    float topOffset = [DocumentUtils getNochtSize] != 0 ? [DocumentUtils getNochtSize] + TOP_MARGIN_OFFSET_WITH_NOTCH : TOP_MARGIN_OFFSET_TO_TEXT_WITHOUT_NOTCH;
    
    NSLayoutConstraint *topTextConstraint = [NSLayoutConstraint
                                                           constraintWithItem:self.labelTitle
                                                           attribute:NSLayoutAttributeTop
                                                           relatedBy:NSLayoutRelationEqual
                                                           toItem:self.view
                                                           attribute:NSLayoutAttributeTop
                                                           multiplier:1.0
                                                           constant:topOffset];
    [self.view addConstraint:topTextConstraint];
}


- (UIColor*) getUIColorFromConfigKey:(NSString*)key {
    return [DocumentUtils transformRGBColor:[self.config objectForKey:key]];
}

@end
