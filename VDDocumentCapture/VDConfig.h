//
//  VDConfig.h
//  VDDocumentCapture
//
//  Copyright © 2017 das-nano. All rights reserved.
//

//Macros
#define IS_IPAD [[[UIDevice currentDevice] model].lowercaseString containsString:@"ipad"]

//Common
#define PROPORTION_PASSPORT_BUTTON (IS_IPAD ? 0.1 : 0.1563)
#define PROPORTION_CLOSE_BUTTON 0.09
#define PROPORTION_CLOSE_BUTTON_IPAD 0.04
#define TICK_TAG 2018
#define ARTIFACT_CLASS_OBVERSE @"document-obverse"
#define ARTIFACT_CLASS_REVERSE @"document-reverse"

// Sizes countdown view
#define INITIAL_SIZE_COUNTDOWN 80.0
#define TEXT_SIZE_LABEL 20.0
#define SIZE_MESSAGE 20.0

// Sizes
#define MAX_SHUTTER_BUTTON_SIZE 95;

//#define COLOR_ERROR [UIColor colorWithHue:0 saturation:0 brightness:1.0 alpha:1.0]
#define COLOR_ERROR [UIColor colorWithRed:255/255 green:255/255 blue:255/255 alpha:1.0]

//#define COLOR_FAR [UIColor colorWithHue:180/360.0 saturation:0.25 brightness:1.0 alpha:1.0]
#define COLOR_FAR [UIColor colorWithRed:191.25/255 green:255/255 blue:255/255 alpha:1.0]

//#define COLOR_MIDDLE [UIColor colorWithHue:160/360.0 saturation:0.5 brightness:1.0 alpha:1.0]
#define COLOR_MIDDLE [UIColor colorWithRed:127.5/255 green:255/255 blue:212.5/255 alpha:1.0]

//#define COLOR_CLOSE [UIColor colorWithHue:140/360.0 saturation:0.75 brightness:1.0 alpha:1.0]
#define COLOR_CLOSE [UIColor colorWithRed:63.75/255 green:255/255 blue:127.5/255 alpha:1.0]

//#define COLOR_VERYCLOSE [UIColor colorWithHue:120/360.0 saturation:1.0 brightness:1.0 alpha:1.0]
#define COLOR_VERYCLOSE [UIColor colorWithRed:0/255 green:255/255 blue:0/255 alpha:1.0]

#define COLOR_EXTREM_CLOSE [UIColor colorWithRed:252/255.0 green:50/255.0 blue:30/255.0 alpha:1.0]

//#define COLOR_EXTREM_FAR [UIColor colorWithHue:213/360.0 saturation:0.95 brightness:0.64 alpha:1.0]
#define COLOR_EXTREM_FAR [UIColor colorWithRed:8/255.0 green:78/255.0 blue:163/255.0 alpha:1.0]

#define NOT_CONFIGURABLE_KEYS [NSArray arrayWithObjects: @"countdowntextcolor", nil];

#define ARTIFACT_CLASS_OBVERSE @"document-obverse"
#define ARTIFACT_CLASS_REVERSE @"document-reverse"

typedef enum{
    FULL_CORRECT_DETECTION,
    HALF_CORRECT_DETECTION,
    STARTED_CORRECT_DETECTION,
    INVISIBLE,
    FAR_DETECTION,
    CLOSE_DETECTION,
    NOTFOUND
} ClosenessState;

typedef enum{
    COUNTDOWN_1,
    COUNTDOWN_2,
    COUNTDOWN_3
} CountDownState;




