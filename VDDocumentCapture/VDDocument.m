//
//  VDDocument.m
//  VDDocumentCapture
//
//  Copyright © 2017 das-Nano. All rights reserved.
//

#import "VDDocument.h"

@implementation VDDocument
- (id) copyWithZone:(NSZone *)zone {
    VDDocument* copy = [[[self class] allocWithZone:zone] init];
    
    if (copy) {
        copy.documentCountry = _documentCountry;
        copy.documentName = _documentName;
        copy.shortDescription = _shortDescription;
        copy.documentType = _documentType;
        copy.documentLegacyId = _documentLegacyId;
        copy.groupIds = _groupIds;
        copy.hasObverse = _hasObverse;
        copy.hasReverse = _hasReverse;
        copy.geographicalArea = _geographicalArea;
    }
    
    return copy;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"Name: %@, country: %@ and enum: %i", _documentName, _documentCountry, _documentLegacyId];
}

@end
