//
//  VDCountry.m
//  VDDocumentCapture
//
//  Copyright © 2017 das-Nano. All rights reserved.
//

#import "VDCountry.h"

@implementation VDCountry

#pragma mark - Protocol methods
- (id) copyWithZone:(NSZone *)zone {
    VDCountry* copy = [[[self class] allocWithZone:zone] init];
    
    if (copy) {
        copy.documentCountry = _documentCountry;
        copy.countryName = _countryName;
    }
    
    return copy;
}

- (BOOL)isEqual:(id)other {
    if (other == self) {
        return YES;
    } else {
        return ((VDCountry *)other).documentCountry == self.documentCountry && ((VDCountry *)other).countryName == self.countryName;
    }
}

- (NSUInteger)hash {
    // TODO: check this line. No idea
    return [self.countryName hash] ^ self.documentCountry;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"Name: %@ and enum: %i", _countryName, _documentCountry];
}
#pragma mark - End protocol methods
@end
