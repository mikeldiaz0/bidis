//
//  DNObjectDetectionConfig.h
//  ImageProcessing
//
//  Created by Mikel Sánchez Yoldi on 11/1/16.
//  Copyright © 2016 das-nano. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DNObjectDetectionConfig : NSObject

/**
 Ruta del archivo cascade
 @author Mikel Sánchez
 */
@property (nonatomic, retain) NSString *cascadePath;

/**
 Factor de escala
 @author Mikel Sánchez
 */
@property (nonatomic, assign) double escale;

/**
 Valor de vecinos
 @author Mikel Sánchez
 */
@property (nonatomic, assign) NSUInteger neighbours;

/**
 Valor de anchura mínima
 @author Mikel Sánchez
 */
@property (nonatomic, assign) NSUInteger minSizeWidth;

/**
 Valor de altura mínima
 @author Mikel Sánchez
 */
@property (nonatomic, assign) NSUInteger minSizeHeight;

/**
 Valor de anchura máxima
 @author Mikel Sánchez
 */
@property (nonatomic, assign) NSUInteger maxSizeWidth;

/**
 Valor de altura máxima
 @author Mikel Sánchez
 */
@property (nonatomic, assign) NSUInteger maxSizeHeight;

@end
