//
//  DNTemplateMatchingObjects.h
//  ImageProcessing
//
//  Created by Miguel Isla Urtasun on 18/2/16.
//  Copyright © 2016 das-nano. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Clase que estructura el resultado del matching.
 @author Miguel Isla.
 */
@interface FinderMatchingConfig : NSObject

/**
 @brief Number of images (levels) in the pyramid, large values allow more variation in size of the object but increase time computing.
 @author Sonia Goñi.
 @version 2.0.0
 */
@property (nonatomic) NSInteger rangePyr;

/**
 @brief (0..1) scale factor between images of the pyramid, large values get fine tuning.
 @author Sonia Goñi.
 @version 2.0.0
 */
@property (nonatomic) double pyrFactor;

/**
 @brief Integer value that represents with 1 that gray transformation must be apply. 0 in other case.
 @details Working with gray images reduce time computing.
 @author Sonia Goñi.
 @version 2.0.0
 */
@property (nonatomic) NSUInteger gray;

/**
 @brief Factor to crop large images in the pyramid to reduce time computing.
 @author Sonia Goñi.
 @version 2.0.0
 */
@property (nonatomic) float factorROI;

/**
 @brief Minimun factor image of pyramid vs template from which start cropping images.
 @author Sonia Goñi.
 @version 2.0.0
 */
@property (nonatomic) float minFactorImgTempl;

/**
 @brief Threshold 1 to get matching.
 @author Sonia Goñi.
 @version 2.0.0
 */
@property (nonatomic) double thMaxValue;

/**
 @brief Threshold 2 to get matching.
 @author Sonia Goñi.
 @version 2.0.0
 */
@property (nonatomic) NSUInteger thArea;

/**
 @brief 
 @author Sonia Goñi.
 @version 2.0.0
 */
@property (nonatomic) NSUInteger prevMargin;

/**
 @brief
 @author Sonia Goñi.
 @version 2.0.0
 */
@property (nonatomic) BOOL useCLAHE;

@end

