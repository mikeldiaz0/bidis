//
//  BackgroundImageUtils.h
//  VDDocumentCapture
//
//  Copyright © 2018 das-nano. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <VDLibrary/ValiDasConstantes.h>

@interface BackgroundImageUtils : NSObject

/**
 @brief Returns the obverse CGImage for a document type to set.
 */
+(id)getUIImageObverse:(NSString*)docType;

/**
 @brief Returns the reverse CGImage for a document type to set.
 */
+(id)getUIImageReverse:(NSString*)docType;

@end
