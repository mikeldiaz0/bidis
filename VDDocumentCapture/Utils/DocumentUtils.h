//
//  DocumentUtils.h
//  VDDocumentCapture
//
//  Copyright © 2018 das-nano. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

typedef struct {
    CGColorRef arrowColor1;
    CGColorRef arrowColor2;
    CGColorRef arrowColor3;
    CGColorRef arrowColor4;
    CGColorRef arrowColor5;
} ArrowColors;

@interface DocumentUtils : NSObject

/**
 @brief Creats an UIImage from a CMSampleBufferRef.
 @param sampleBuffer (CMSampleBufferRef) Buffer with the image info.
 @return (UImage *) Image with the info.
 */
+(UIImage*)imageFromSampleBuffer:(CMSampleBufferRef)sampleBuffer;

/**
 @brief Returns an UIImage from a filename. It searchs the image in framework bundle or in the main bundle of the app.
 @param filename (NSString*) filename.
 */
+(UIImage*)getImageFromFilename:(NSString*)filename;

+(UIColor*)transformRGBColor:(NSDictionary*)color;
/**
 * Resizes image to selected width.
 */
+ (UIImage *)resizeImage:(UIImage*)image toWidth:(float) newWidth;
/**
 * Resizes image to selected height.
 */
+ (UIImage *)resizeImage:(UIImage*)image toHeight:(float) newHeight;
/**
 * Resizes image to selected size.
 */
+(UIImage*)resizeImage:(UIImage*)image toSmallerSize:(CGSize)newSize;

+(UIBezierPath*)getArrowFrom:(CGPoint)start To:(CGPoint)end ArrowNumber:(int)arrowNumber;
+(CGFloat)computeDistanceBetween:(CGPoint)origin And:(CGPoint)final;
+(NSArray*)getCornerArrowsFrom:(CGRect)rect1 To:(CGRect)rect2 withBorderRadius:(CGFloat)borderRadius arrowColors:(ArrowColors)colors;

/**
 @brief Presents a modal view as a pop up.
 */
+(void)presentPopUpModal:(UIViewController*)modal inController:(UIViewController*)controller animated:(BOOL)animated;

/**
 @brief Converts string to HTML string with the current label format.
 @param str (NSString) Str with html.
 */
+(void)convertToHTMLString:(NSString*)str inLabel:(UILabel*)label;

+(NSString*)getPathFromFilename:(NSString*)filename;

/**
 @brief Method that checks if the current device has notch.
 */
+ (BOOL) checkIfNotchExist;
+ (float) getNochtSize;
@end
