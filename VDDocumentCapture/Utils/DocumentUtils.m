//
//  DocumentUtils.m
//  VDDocumentCapture
//
//  Copyright © 2018 das-nano. All rights reserved.
//

#import "DocumentUtils.h"
#import "VDConfig.h"
#import "Constants.h"

@implementation DocumentUtils

+ (UIImage *)imageFromSampleBuffer:(CMSampleBufferRef) sampleBuffer {
    
    // Get a CMSampleBuffer's Core Video image buffer for the media data
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    // Lock the base address of the pixel buffer
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    
    // Get the number of bytes per row for the pixel buffer
    void *baseAddress = CVPixelBufferGetBaseAddress(imageBuffer);
    
    // Get the number of bytes per row for the pixel buffer
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    // Get the pixel buffer width and height
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    
    // Create a device-dependent RGB color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // Create a bitmap graphics context with the sample buffer data
    //CGContextRef context = CGBitmapContextCreate(baseAddress, width, height, 8, bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    CGContextRef context = CGBitmapContextCreate(baseAddress, width, height, 8,
                                                 bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    // Create a Quartz image from the pixel data in the bitmap graphics context
    CGImageRef quartzImage = CGBitmapContextCreateImage(context);
    // Unlock the pixel buffer
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    
    // Free up the context and color space
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    // Create an image object from the Quartz image
    UIImage *image = [UIImage imageWithCGImage:quartzImage];
    
    // Release the Quartz image
    CGImageRelease(quartzImage);
    
    return (image);
    
}

+(UIImage*)getImageFromFilename:(NSString*)filename
{
    NSString *path = [[NSBundle bundleForClass:self.class] pathForResource:filename ofType:nil];
    path = path ? path : [[NSBundle mainBundle] pathForResource:filename ofType:nil];
    return [UIImage imageWithContentsOfFile:path];
}

+(UIColor*)transformRGBColor:(NSDictionary*)color
{
    return [UIColor colorWithRed:([(NSString*) [color objectForKey:@"red"] floatValue]/255) green:([(NSString*) [color objectForKey:@"green"] floatValue]/255) blue:([(NSString*) [color objectForKey:@"blue"] floatValue]/255) alpha:[(NSString*) [color objectForKey:@"alpha"] floatValue]];
}

+ (UIImage *)resizeImage:(UIImage*)image toWidth:(float) newWidth {
    float newHeight = newWidth * image.size.height / image.size.width;
    CGSize size = CGSizeMake(newWidth, newHeight);
    
    return [self resizeImage:image toSize:size];
}

+ (UIImage *)resizeImage:(UIImage*)image toHeight:(float) newHeight {
    float newWidth = newHeight * image.size.width / image.size.height;
    CGSize size = CGSizeMake(newWidth, newHeight);
    
    return [self resizeImage:image toSize:size];
}

+ (UIImage *)resizeImage:(UIImage*)image toSmallerSize:(CGSize)newSize {
    if (newSize.height < image.size.height || newSize.width < image.size.width) {
        return [self resizeImage:image toSize:newSize];
    }
    return image;
}

+ (UIImage *)resizeImage:(UIImage*)image toSize:(CGSize)newSize {
   UIGraphicsBeginImageContext(newSize);
   [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
   UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
   UIGraphicsEndImageContext();
   return newImage;
}

+(UIBezierPath*)getArrowFrom:(CGPoint)start To:(CGPoint)end ArrowNumber:(int)arrowNumber
{
    CGFloat length = hypotf(end.x - start.x, end.y - start.y);
    float arrowWidth = 17;
    float arrowThickness = 2;
    CGPoint points[7];
    float arrowPos = (length * arrowNumber / 5) - (arrowWidth/2);
    points[0] = CGPointMake(0+arrowPos, arrowWidth/2 - arrowThickness);
    points[1] = CGPointMake(0+arrowPos, arrowWidth / 2);
    points[2] = CGPointMake((arrowWidth/2) +arrowPos, 0);
    points[3] = CGPointMake(0+arrowPos, -arrowWidth/2);
    points[4] = CGPointMake(0+arrowPos, -arrowWidth/2+arrowThickness);
    points[5] = CGPointMake(arrowWidth/2 -arrowThickness+arrowPos ,0);
    points[6] = CGPointMake(0+arrowPos, arrowWidth/2-arrowThickness);
    
    double cosine = (end.x - start.x) / length;
    double sine = (end.y - start.y) / length;
    CGAffineTransform transformation =  (CGAffineTransform){ cosine, sine, -sine, cosine, start.x, start.y};
    CGMutablePathRef cgPath = CGPathCreateMutable();
    CGPathAddLines(cgPath, &transformation, points, sizeof points / sizeof *points);
    CGPathCloseSubpath(cgPath);
    UIBezierPath *uiPath = [UIBezierPath bezierPathWithCGPath:cgPath];
    CGPathRelease(cgPath);
    return uiPath;
}

+(NSArray*)getCornerArrowsFrom:(CGRect)rect1 To:(CGRect)rect2 withBorderRadius:(CGFloat)borderRadius arrowColors:(ArrowColors)colors
{
    UIBezierPath * bezier1 = [UIBezierPath new];
    UIBezierPath * bezier2 = [UIBezierPath new];
    UIBezierPath * bezier3 = [UIBezierPath new];
    UIBezierPath * bezier4 = [UIBezierPath new];
    UIBezierPath * bezier5 = [UIBezierPath new];
    
    float offset = borderRadius / 3.5;
    float minDistance = 15;
    float offsetX = offset;
    float offsetY = offset;
    
    // **left**
    if(rect1.origin.y > rect2.origin.y){
        offsetY = - offsetY;
    }
    
    // bot left
    if(rect1.origin.x > rect2.origin.x){
        offsetX = - offset;
    }
    
    CGPoint origin = CGPointMake(rect1.origin.x + offsetX,rect1.origin.y + offsetY);
    CGPoint final = CGPointMake(rect2.origin.x + 2*offset,rect2.origin.y + 2*offset);
    if([DocumentUtils computeDistanceBetween:origin And:final] > minDistance){
        [bezier1 appendPath:[DocumentUtils getArrowFrom:final To:origin ArrowNumber:1]];
        [bezier2 appendPath:[DocumentUtils getArrowFrom:final To:origin ArrowNumber:2]];
        [bezier3 appendPath:[DocumentUtils getArrowFrom:final To:origin ArrowNumber:3]];
        [bezier4 appendPath:[DocumentUtils getArrowFrom:final To:origin ArrowNumber:4]];
        [bezier5 appendPath:[DocumentUtils getArrowFrom:final To:origin ArrowNumber:5]];
        
    }
    // top left
    offsetX = offset;
    if(rect1.origin.x + rect1.size.width > rect2.origin.x + rect2.size.width){
        offsetX = - offset;
    }
    origin = CGPointMake(rect1.origin.x+rect1.size.width + offsetX,rect1.origin.y+ offsetY);
    final = CGPointMake(rect2.origin.x+rect2.size.width-2*offset,rect2.origin.y+2*offset);
    if([DocumentUtils computeDistanceBetween:origin And:final] > minDistance){
        [bezier1 appendPath:[DocumentUtils getArrowFrom:final To:origin ArrowNumber:1]];
        [bezier2 appendPath:[DocumentUtils getArrowFrom:final To:origin ArrowNumber:2]];
        [bezier3 appendPath:[DocumentUtils getArrowFrom:final To:origin ArrowNumber:3]];
        [bezier4 appendPath:[DocumentUtils getArrowFrom:final To:origin ArrowNumber:4]];
        [bezier5 appendPath:[DocumentUtils getArrowFrom:final To:origin ArrowNumber:5]];
    }
    
    offsetX = offset;
    offsetY = offset;
    // **right**
    if(rect1.origin.y + rect1.size.height > rect2.origin.y + rect2.size.height){
        offsetY = - offset;
    }
    // top right
    if(rect1.origin.x + rect1.size.width > rect2.origin.x + rect2.size.width){
        offsetX = - offset;
    }
    origin = CGPointMake(rect1.origin.x+rect1.size.width + offsetX,rect1.origin.y+rect1.size.height+ offsetY);
    final = CGPointMake(rect2.origin.x+rect2.size.width-2*offset,rect2.origin.y+rect2.size.height-2*offset);
    if([DocumentUtils computeDistanceBetween:origin And:final] > minDistance){
        [bezier1 appendPath:[DocumentUtils getArrowFrom:final To:origin ArrowNumber:1]];
        [bezier2 appendPath:[DocumentUtils getArrowFrom:final To:origin ArrowNumber:2]];
        [bezier3 appendPath:[DocumentUtils getArrowFrom:final To:origin ArrowNumber:3]];
        [bezier4 appendPath:[DocumentUtils getArrowFrom:final To:origin ArrowNumber:4]];
        [bezier5 appendPath:[DocumentUtils getArrowFrom:final To:origin ArrowNumber:5]];
    }
    // bot right
    offsetX = offset;
    if(rect1.origin.x > rect2.origin.x){
        offsetX = - offset;
    }
    origin = CGPointMake(rect1.origin.x + offsetX,rect1.origin.y+rect1.size.height+ offsetY);
    final = CGPointMake(rect2.origin.x+2*offset,rect2.origin.y+rect2.size.height-2*offset);
    if([DocumentUtils computeDistanceBetween:origin And:final] > minDistance){
        [bezier1 appendPath:[DocumentUtils getArrowFrom:final To:origin ArrowNumber:1]];
        [bezier2 appendPath:[DocumentUtils getArrowFrom:final To:origin ArrowNumber:2]];
        [bezier3 appendPath:[DocumentUtils getArrowFrom:final To:origin ArrowNumber:3]];
        [bezier4 appendPath:[DocumentUtils getArrowFrom:final To:origin ArrowNumber:4]];
        [bezier5 appendPath:[DocumentUtils getArrowFrom:final To:origin ArrowNumber:5]];
    }
    
    
    //UIColor *arrowColor;
    // arrow1
    CAShapeLayer * arrow1 = [CAShapeLayer new];
    arrow1.lineWidth = 1;
    arrow1.borderWidth = 1;
    arrow1.fillRule = kCAFillRuleNonZero;
    arrow1.lineCap = kCALineCapButt;
    arrow1.lineDashPattern = nil;
    arrow1.lineDashPhase = 0.0;
    arrow1.lineJoin = kCALineJoinRound;
    arrow1.path = bezier1.CGPath;
    arrow1.fillColor = colors.arrowColor1;
    arrow1.strokeColor = colors.arrowColor1;
    // arrow2
    CAShapeLayer * arrow2 = [CAShapeLayer new];
    arrow2.lineWidth = 1;
    arrow2.borderWidth = 1;
    arrow2.fillRule = kCAFillRuleNonZero;
    arrow2.lineCap = kCALineCapButt;
    arrow2.lineDashPattern = nil;
    arrow2.lineDashPhase = 0.0;
    arrow2.lineJoin = kCALineJoinRound;
    arrow2.path = bezier2.CGPath;
    arrow2.fillColor = colors.arrowColor2;
    arrow2.strokeColor = colors.arrowColor2;
    // arrow 3
    CAShapeLayer * arrow3 = [CAShapeLayer new];
    arrow3.lineWidth = 1;
    arrow3.borderWidth = 1;
    arrow3.fillRule = kCAFillRuleNonZero;
    arrow3.lineCap = kCALineCapButt;
    arrow3.lineDashPattern = nil;
    arrow3.lineDashPhase = 0.0;
    arrow3.lineJoin = kCALineJoinRound;
    arrow3.path = bezier3.CGPath;
    arrow3.fillColor = colors.arrowColor3;
    arrow3.strokeColor = colors.arrowColor3;
    // arrow 4
    CAShapeLayer * arrow4 = [CAShapeLayer new];
    arrow4.lineWidth = 1;
    arrow4.borderWidth = 1;
    arrow4.fillRule = kCAFillRuleNonZero;
    arrow4.lineCap = kCALineCapButt;
    arrow4.lineDashPattern = nil;
    arrow4.lineDashPhase = 0.0;
    arrow4.lineJoin = kCALineJoinRound;
    arrow4.path = bezier4.CGPath;
    arrow4.fillColor = colors.arrowColor4;
    arrow4.strokeColor = colors.arrowColor4;
    // arrow 5
    CAShapeLayer * arrow5 = [CAShapeLayer new];
    arrow5.lineWidth = 1;
    arrow5.borderWidth = 1;
    arrow5.fillRule = kCAFillRuleNonZero;
    arrow5.lineCap = kCALineCapButt;
    arrow5.lineDashPattern = nil;
    arrow5.lineDashPhase = 0.0;
    arrow5.lineJoin = kCALineJoinRound;
    arrow5.path = bezier5.CGPath;
    arrow5.fillColor = colors.arrowColor5;
    arrow5.strokeColor = colors.arrowColor5;
    arrow5.backgroundColor = [UIColor clearColor].CGColor;
    
    return @[arrow1, arrow2, arrow3, arrow4, arrow5];
}

+(CGFloat)computeDistanceBetween:(CGPoint)origin And:(CGPoint)final
{
    CGFloat distance = sqrtf(((final.x-origin.x)*(final.x-origin.x)) + ((final.y-origin.y)*(final.y-origin.y)));
    
    return distance;
}

+(void)presentPopUpModal:(UIViewController*)modal inController:(UIViewController*)controller animated:(BOOL)animated
{
    modal.definesPresentationContext = YES;
    modal.providesPresentationContextTransitionStyle = YES;
    modal.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    modal.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [controller presentViewController:modal animated:animated completion:nil];
}

+(void)convertToHTMLString:(NSString *)str inLabel:(UILabel *)label
{
    if (label) {
        str = [str stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
        str = [NSString stringWithFormat:@"<body style='font-family: %@; font-size: %f'>%@</body>", label.font.familyName, label.font.pointSize, str];
        
        NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
        paragraph.alignment = label.textAlignment;
        
        NSAttributedString *attrString = [[NSAttributedString alloc] initWithData:[str dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
        
        NSMutableAttributedString *newString = [[NSMutableAttributedString alloc] initWithAttributedString:attrString];
        NSRange allRange = (NSRange){0, newString.length};
        [newString addAttribute:NSForegroundColorAttributeName value:label.textColor range:allRange];
        [newString addAttribute:NSParagraphStyleAttributeName value:paragraph range:allRange];
        
        label.attributedText = newString;
    }
}


+(NSString*)getPathFromFilename:(NSString*)filename {
    NSString *path = [[NSBundle bundleForClass:self.class] pathForResource:filename ofType:nil];
    return path ? path : [[NSBundle mainBundle] pathForResource:filename ofType:nil];
}

+(BOOL) checkIfNotchExist {
    if (@available( iOS 11.0, * )) {
        if ([[[UIApplication sharedApplication] keyWindow] safeAreaInsets].bottom > 0) {
            return true;
        }
    }
    return false;
}

+(float) getNochtSize {
    if (@available( iOS 11.0, * )) {
        if ([[[UIApplication sharedApplication] keyWindow] safeAreaInsets].bottom > 0) {
            return [[[UIApplication sharedApplication] keyWindow] safeAreaInsets].bottom;
        }
    }
    return 0;
}

@end
