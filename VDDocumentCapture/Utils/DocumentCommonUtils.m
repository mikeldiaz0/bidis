//
//  DocumentCommonUtils.m
//  VDDocumentCapture
//
//  Copyright © 2019 das-nano. All rights reserved.
//

#import "DocumentCommonUtils.h"
#import <sys/utsname.h>
#import <UIKit/UIKit.h>

@implementation DocumentCommonUtils

+(NSString*)getDeviceName
{
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *deviceModel = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
       
    return deviceModel;
}

+(NSString*)getSDKversion
{
    return [[NSBundle bundleForClass:DocumentCommonUtils.class] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
}

+(NSString *)getCurrentiOSVersion
{
    return [[UIDevice currentDevice] systemVersion];
}

+(NSString*)getBundleIdentifier
{
    return [[NSBundle bundleForClass:DocumentCommonUtils.class] bundleIdentifier];
}

+(NSString*)getCurrentDateTime
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy/MM/dd HH:mm:ss.SSS";
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"US"]];
    
    return [formatter stringFromDate:[NSDate date]];
}

@end
