//
//  BackgroundImageUtils.m
//  VDDocumentCapture
//
//  Copyright © 2018 das-nano. All rights reserved.
//

#import "BackgroundImageUtils.h"
#import <VDLibrary/ValiDas.h>

@implementation BackgroundImageUtils

+(id)getUIImageObverse:(NSString*)docType
{
    NSBundle *bundle = [NSBundle bundleForClass:self.class];
    NSString *imgName = [[ValiDas getTemplateNameForDocument:docType forReverse:NO] stringByAppendingString:@".png"];
    return (id)[UIImage imageWithContentsOfFile:[bundle pathForResource:imgName ofType:nil]].CGImage;
}

+(id)getUIImageReverse:(NSString*)docType
{
    NSBundle *bundle = [NSBundle bundleForClass:self.class];
    NSString *imgName = [[ValiDas getTemplateNameForDocument:docType forReverse:YES] stringByAppendingString:@".png"];
    return (id)[UIImage imageWithContentsOfFile:[bundle pathForResource:imgName ofType:nil]].CGImage;
}

@end
