//
//  DocumentCommonUtils.h
//  VDDocumentCapture
//
//  Copyright © 2019 das-nano. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DocumentCommonUtils : NSObject

+(NSString*)getDeviceName;
+(NSString*)getSDKversion;
+(NSString*)getCurrentiOSVersion;
+(NSString*)getBundleIdentifier;
+(NSString*)getCurrentDateTime;

@end
