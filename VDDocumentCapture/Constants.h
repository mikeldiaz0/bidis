//
//  Constants.h
//  VDDocumentCapture
//
//  Copyright © 2019 das-nano. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define KEY_FIREBASE                        @"firebase"
#define KEY_FIREBASE_USER_ID                @"userid"
#define KEY_FIREBASE_APP_ID                 @"applicationid"
#define KEY_FIREBASE_PROJECT_ID             @"projectid"
#define KEY_FIREBASE_STORAGE_BUCKET         @"storagebucket"
#define KEY_FIREBASE_APP_NAME               @"firebaseappname"

#define KEY_BIDI_SEARCH_FINISHED            @"searchFinished"

#endif /* Constants_h */
