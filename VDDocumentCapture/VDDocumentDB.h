//
//  ValiDasBBDDDocumentos.h
//  VDOnBoarding
//
//  Copyright © 2016 das-nano. All rights reserved.
//

#import <Foundation/Foundation.h>
@class VDDocument;
@class VDCountry;

/**
 @brief Class that creates a DB with countris and documents available.
 */
@interface VDDocumentDB : NSObject

/**
 @brief Creates the DB.
 @return (NSDictionary<NSString *,NSArray<NSString *> *> *) Dictionary with the countries gropup id in its keys and an array of documents ids in each value.
 */
+ (NSDictionary<VDCountry *,NSArray<VDDocument *> *> *) createDB;


/**
 @brief Creates the DB.
 @return (NSDictionary *) Dictionary with the countries gropup id in its keys and an array of documents ids in each value.
 */
+ (NSDictionary *) createOriginalDB;

+ (VDDocument *) getDocumentFromString: (NSString *) name;

/**
 @brief Check if some document in a list has reverse or not.
 @return (Bool) True if any document has reverse and false otherwise.
 */
+ (BOOL) someDocumentHasReverse: (NSArray<NSString*>* ) documents;
@end
