//
//  ValiDasBBDDDocumentos.m
//  VDOnBoarding
//
//  Copyright © 2016 das-nano. All rights reserved.
//

#import "VDDocumentDB.h"
#import <VDLibrary/ValiDasDocumento.h>
#import <VDLibrary/ValiDasPais.h>
#import <VDLibrary/ValiDas.h>
#import "VDDocument.h"
#import "VDCountry.h"

@implementation VDDocumentDB

#pragma mark - Public methods

+ (NSDictionary<VDCountry*, NSArray<VDDocument *> *> *) createDB {
    NSMutableDictionary* documentAndCountryDictionary = [[NSMutableDictionary alloc] init];
    NSDictionary *availableDocs = [ValiDas getAvailableDocumentsByGroup];
    
    for (NSString *groupId in availableDocs.allKeys) {
        VDCountry* country = [VDCountry new];
        country.countryName = groupId;
        
        NSArray *docs = [availableDocs objectForKey:groupId];
        NSMutableArray* documents = [NSMutableArray new];
        [docs enumerateObjectsUsingBlock:^(id  _Nonnull doc, NSUInteger idx, BOOL * _Nonnull stop) {
            VDDocument* document = [VDDocument new];
            document.documentName = [doc valueForKey:@"id"];
            document.documentLegacyId = [[doc valueForKey:@"legacy_id"] intValue];
            document.documentType = [[doc valueForKey:@"legacy_id"] intValue];
            document.documentCountry = [[doc  valueForKey:@"properties"] valueForKey:@"country"];
            document.shortDescription = [doc valueForKey:@"short_description"];     
            [documents addObject:document];
        }];
        
        [documentAndCountryDictionary setObject:documents forKey:country];
    }
    
    return documentAndCountryDictionary;
}

+ (NSDictionary *) createOriginalDB
{
    NSMutableDictionary *diccionarioPaisDocumento = [[NSMutableDictionary alloc] init];
    NSDictionary *availableDocs = [ValiDas getAvailableDocumentsByGroup];
    
    for (NSString *groupId in availableDocs.allKeys) {
        NSArray *docs = [availableDocs objectForKey:groupId];
        ValiDasPais *country = [[ValiDasPais alloc] initWithIsoCode:groupId];
        NSMutableArray *documentsForCountry = [[NSMutableArray alloc] init];
        
        for (NSDictionary *doc in docs) {
            ValiDasDocumento *document = [ValiDasDocumento new];
            document.country = country;
            document.name = [doc objectForKey:@"short_description"];
            document.obverse = [[doc objectForKey:@"image_types"] containsObject:@"obverse"];
            document.reverse = [[doc objectForKey:@"image_types"] containsObject:@"reverse"];
            document.type = [doc objectForKey:@"id"];
            document.standardType = [doc objectForKey:@"standard_type"];
            
            [documentsForCountry addObject:document];
        }
        
        [diccionarioPaisDocumento setObject:documentsForCountry forKey:groupId];
    }
    
    return diccionarioPaisDocumento;
}

+ (VDDocument *) getDocumentFromString: (NSString *) name {
    NSDictionary *availableDocs = [ValiDas getAvailableDocumentsByGroup];
    __block VDDocument* document = [VDDocument new];
    for (NSString *groupId in availableDocs.allKeys) {
        NSArray *docs = [availableDocs objectForKey:groupId];
        [docs enumerateObjectsUsingBlock:^(id  _Nonnull doc, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([[doc valueForKey:@"id"] isEqualToString:name]) {
                document.documentName = [doc valueForKey:@"id"];
                document.documentLegacyId = [[doc valueForKey:@"legacy_id"] intValue];
                document.documentType = [[doc valueForKey:@"legacy_id"] intValue];
                document.documentCountry = [[doc  valueForKey:@"properties"] valueForKey:@"country"];
                
                NSArray * imageTypes = [doc valueForKey:@"image_types"];
                document.hasObverse = [imageTypes containsObject:@"obverse"];
                document.hasReverse = [imageTypes containsObject:@"reverse"];
                *stop = true;
            }
        }];
    }
    return document;
}

+ (BOOL) someDocumentHasReverse: (NSArray<NSString*>* ) documents {
    __block BOOL hasReverse = false;
    NSDictionary *availableDocs = [ValiDas getAvailableDocumentsByGroup];
    for (NSString *groupId in availableDocs.allKeys) {
        NSArray *docs = [availableDocs objectForKey:groupId];
        [docs enumerateObjectsUsingBlock:^(id  _Nonnull doc, NSUInteger idx, BOOL * _Nonnull stop) {
            for (NSString* possibleDocument in documents) {
                if ([[doc valueForKey:@"id"] isEqualToString:possibleDocument]) {
                    NSArray * imageTypes = [doc valueForKey:@"image_types"];
                    if([imageTypes containsObject:@"reverse"]){
                        hasReverse = true;
                    }
                }
            }
        }];
    }
    return hasReverse;
}


#pragma mark - End public methods
@end
