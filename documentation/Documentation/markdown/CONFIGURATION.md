# Configuration {#Configuration}

The SDK can be configured by providing a dictionary to the SDK. All colors are configured with a dictionary with "red","green","blue" as keys and values between 0-255 and "alpha" as key and value between 0-1. The SDK configuration dictionary may contain or not the following keys/values:

- arrowcolor1 : The color of the first guiding arrow for the document.
- arrowcolor2 : The color of the second guiding arrow for the document.
- arrowcolor3 : The color of the third guiding arrow for the document.
- arrowcolor4 : The color of the forth guiding arrow for the document.
- arrowcolor5 : The color of the fifth guiding arrow for the document.
- arrows : Whether the SDK may have to show the guiding arrows for the document or not. ("YES"/"NO")
- backgroundcolor : The background color of the informative label shown to the user.
- capturebackgroundcolor :  The background color of the capture view.
- checkdocumenttext : The text that is shown to inform the user in the revision of the document photo.
- checkdocumenttextcolor : The color of the text that is shown to inform the user in the revision of the document photo.
- closebutton : Whether the SDK may have a close button or not. ("YES"/"NO")
- closebuttonimage : Image to be shown to close the SDK.
- continuebuttonbackgroundcolor : The background color of the continue button in the document photo verification.
- continuebuttonicon : Image to be shown alongside the continue button.
- continuebuttontext : The text of the continue button in the document photo verification.
- continuebuttontextcolor : The color of the text of the continue button in the document photo verification.
- documentdetectedoklevel1color: Template and text color when document is detected in the correct position. Level 1.
- documentdetectedoklevel2color: Template and text color when document is detected in the correct position. Level 2.
- documentdetectedoklevel3color: Template and text color when document is detected in the correct position. Level 3.
- documentdetectedveryclosecolor: Template color when document is detected but it is very close.
- documentdetectedveryfarcolor: Template color when document is detected but it is very far.
- documentveryclose : The text to show when the document is located but too close to the device.
- documentveryfar : The text to show when the document is located but far from the device.
- familyName : Name of the font to be used in all SDK except for countdown text.
- fixedrectangle : Whether the SDK may have the fixed rectangle to fit with the document or not. ("YES"/"NO")
- fixedtemplate : Whether the SDK may have the fixed template to help fit the document or not. ("YES"/"NO")
- flashwaitingalert : The text of the alert between flashon-flash image captures.
- flashwaitingalertanimationimage : The image of the alert between flashon-flash image captures. It can be a png or a gif. The png indefinetily rotates by the center of the image.
- flashwaitingalertbgcolor : The background color of the alert between flashon-flash image captures.
- flashwaitingalerttextcolor : The color of the text of the alert between flashon-flash image captures.
- infoalert : The text to show at the beginning of the capture to inform the user.
- infoalertpass : The text to show at the beginning of the capture to inform the user in passport case.
- infoalertshow :  Whether the initial alert must be shown or not.("YES","NO")
- infoalerttitle : The title of the alert showed at the beginning of the capture.
- infoalerttitlepass : The title of the alert showed at the beginning of the capture in passport case.
- informativetextcolor : The color of the text of the informative label shown to the user.
- obversedetectiondelay : Seconds to wait before start detection on the obverse.
- obverseflash : Whether the SDK may have to take obverse with flash image or not. ("YES"/"NO")
- obversenotfoundtext : The text to show when the obverse is not found.
- onlyobverse :  Whether the SDK may have to take only obverse image capture.
- passportbuttonbackgroundcolor : The background color for the passport button capture.
- passportnotfoundtext : The text to show when the passport is not found. (At the moment the passport capture is manual)
- permissionrefused : The text for the alert when a permission is not allowed.
- permissionrefusedbutton : The text of the button from the alert when a permission is not allowed. This button will lead to the permission settings of the app.
- permissionrefusedtitle : The title for the alert when a permission is not allowed.
- popupvalidationbackgroundcolor : The background color of the text to show when the user hits the help button in the document photo verification.
- popupvalidationtextcolor : The color of the text to show when the user hits the help button in the document photo verification.
- positivebuttontitle : The text of the all positive buttons.
- ratioButtonsValidation : The ratio regarding the image for each of the buttons in the validation view.
- repeatbuttonbackgroundcolor : The background color of the repeat button in the document photo verification.
- repeatbuttonicon : Image to be shown alongside the repeat button.
- repeatbuttontext : The text of the repeat button in the document photo verification.
- repeatbuttontextcolor : The color of the text of the repeat button in the document photo verification.
- reversedetectiondelay : Seconds to wait before start detection on the reverse.
- reverseflash : Whether the SDK may have to take reverse with flash image or not. ("YES"/"NO")
- reversenotfoundtext : The text to show when the reverse is not found.
- secondswithoutpicture : The number of seconds that are needed without taking the photo for the SDK to notify the app.
- secondswithoutshutterbuttonobverse : The number of seconds that are consumed before showing the shutter button in the obverse case.
- secondswithoutshutterbuttonreverse : The number of seconds that are consumed before showing the shutter button in the reverse case.
- secondswithshutterbuttonmessage : The number of seconds that shutter button message will be shown.
- showdocument : Whether document photo verification section must be shown or not.("YES","NO")
- showtutorial : Whether the tutorial must be shown or not.("YES","NO")
- shutterbuttonbackgroundcolor : Color of the circle of the shutter image.
- shutterbuttonbordercolor : Color of the circle of the shutter image.
- shutterbuttonmessage : Text to be shown in the shutter message.
- shutterbuttonmessagebackgroundcolor : Color of the message of the shutter button.
- shutterbuttonmessagetextcolor : Color of the message of the shutter button.
- shutterbuttonshow : Whether the shutter button and message have to be shown or not.("YES","NO")
- tickcirclecolor : Color of the circle of the tick image.
- tickcolor : Color of the tick of the tick image.
- tutorialbackgroundcolor : Background color of the tutorial view.
- tutorialcontinuebuttoncolor : Background color of the continue button in the tutorial view.
- tutorialcontinuebuttontextcolor : Text color of the continue button in the tutorial view.
- tutorialgif : Name of the gif to be shown in the tutorial view.
- tutorialtext : Text to be shown in the tutorial view.
- tutorialtextcolor : Color of the text to be shown in the tutorial view.
- tutorialtitle : Title to be shown in the tutorial view.
- tutorialtitlecolor : Color of the title to be shown in the tutorial view.
- userinfo : The text to show when the user hits the help button in the document photo verification section.
- validationbackgroundcolor : The background color of the document photo verification section.

The default values for these keys are:
-

Key                                  | Default Value (en/es)
:-----------------------------------:|:---------------------:
"arrowcolor1"                        | red = "133", green = "202", blue = "255", alpha = "1"
"arrowcolor2"                        | red = "204", green = "239", blue = "255", alpha = "1"
"arrowcolor3"                        | red = "226", green = "241", blue = "249", alpha = "1"
"arrowcolor4"                        | red = "237", green = "244", blue = "248", alpha = "1"
"arrowcolor5"                        | red = "249", green = "250", blue = "251", alpha = "1"
"arrows"                             | "YES"
"backgroundcolor"                    | red = "0", green = "0", blue = "0", alpha = "0.66"
"capturebackgroundcolor"             | red = "125", green = "125", blue = "125", alpha = "1"
"checkdocumenttext"                  | "If the photo is clear, press CONTINUE""Si la foto se ve bien, pulsa CONTINUAR"
"checkdocumenttextcolor"             | red = "255", green = "255", blue = "255", alpha = "1"
"closebutton"                        | "NO"
"closebuttonimage"                   | "ic_closebutton.png"
"continuebuttonbackgroundcolor"      | red = "17", green = "100", blue = "102", alpha = "1"
"continuebuttonicon"                 | ""
"continuebuttontext"                 | "CONTINUE""CONTINUAR"
"continuebuttontextcolor"            | red = "255", green = "255", blue = "255", alpha = "1"
"continuebuttontextcolor"            | red = "255", green = "255", blue = "255", alpha = "1"
"documentdetectedoklevel1color"      | red = "0", green = "255", blue = "0", alpha = "1"
"documentdetectedoklevel2color"      | red = "127.5", green = "255", blue = "212.5", alpha = "1"
"documentdetectedoklevel3color"      | red = "191.25", green = "255", blue = "255", alpha = "1"
"documentdetectedveryclosecolor"     | red = "252", green = "50", blue = "30", alpha = "1"
"documentdetectedveryfarcolor"       | red = "8.16", green = "77.93", blue = "163.2", alpha = "1"
"documentveryclose"                  | "Too close! Place the device further""¡Demasiado lejos! Acerque el dispositivo"
"documentveryfar"                    | "Too far! Place the device closer""¡Demasiado cerca! Aleje el dispositivo"
"familyName"                         | "Helvetica"
"fixedrectangle"                     | "YES"
"fixedtemplate"                      | "YES"
"flashwaitingalert"                  | "Do not move. Taking flash photo.""No se mueva. Tomando la foto con flash."
"flashwaitingalertbgcolor"           | red = "0", green = "0", blue = "0", alpha = "0.7"
"flashwaitingalerttextcolor"         | red = "255", green = "255", blue = "255", alpha = "1"
"infoalert"                          | "TWO photos will be taken AUTOMATICALLY.""Se tomarán DOS fotos AUTOMÁTICAMENTE."
"infoalertpass"                      | "ONE photos will be taken AUTOMATICALLY.""Se tomará UNA foto AUTOMÁTICAMENTE."
"infoalertshow"                      | "YES"
"infoalerttitle"                     | ""
"infoalerttitlepass"                 | ""
"informativetextcolor"               | red = "255", green = "255", blue = "255", alpha = "1"
"obversedetectiondelay"              | "0"
"obverseflash"                       | "YES"
"obversenotfoundtext"                | "Fit the FRONT""Encaje la parte DELANTERA"
"onlyobverse"                        | "NO"
"passportbuttonbackgroundcolor"      | red = "204", green = "204", blue = "204", alpha = "1"
"passportnotfoundtext"               | "Adjust the Passport page""Ajuste la página del pasaporte"
"permissionrefused"                  | "The permission was rejected. You need to activate it manually: Settings > Application > Activate Permission""Ha rechazado el permiso. Tiene que activarlo manualmente en: Ajustes > Aplicación > Activar Permiso"
"permissionrefusedbutton"            | "Open settings""Abrir ajustes"
"permissionrefusedtitle"             | "Required permission""Permiso requerido"
"popupvalidationbackgroundcolor"     | red = "0", green = "0", blue = "0", alpha = "0.7"
"popupvalidationtextcolor"           | red = "255", green = "255", blue = "255", alpha = "1"
"positivebuttontitle"                | "Continue""Continuar"
"ratioButtonsValidation"             | "2"
"repeatbuttonbackgroundcolor"        | red = "17", green = "100", blue = "102", alpha = "1"
"repeatbuttonicon"                   | ""
"repeatbuttontext"                   | "REPEAT""REPETIR"
"repeatbuttontextcolor"              | red = "255", green = "255", blue = "255", alpha = "1"
"reversedetectiondelay"              | "0"
"reverseflash"                       | "NO"
"reversenotfoundtext"                | "Fit the BACKSIDE""Encaje la parte TRASERA"
"secondswithoutpicture"              | "40"
"secondswithoutshutterbuttonobverse" | "10"
"secondswithoutshutterbuttonreverse" | "10"
"secondswithshutterbuttonmessage"    | "6"
"showdocument"                       | "YES"
"showtutorial"                       | "NO"
"shutterbuttonbackgroundcolor"       | red = "233", green = "233", blue = "234", alpha = "1"
"shutterbuttonbordercolor"           | red = "204", green = "204", blue = "204", alpha = "1"
"shutterbuttonmessage"               | "Tap the button to take the photo""Presione el botón para sacar la foto"
"shutterbuttonmessagetextcolor"       | red = "255", green = "255", blue = "255", alpha = "1"
"shutterbuttonshow"                  | "YES"
"tickcirclecolor"                    | red = "72", green = "174", blue = "100", alpha = "1"
"tickcolor"                          | red = "255", green = "255", blue = "255", alpha = "1"
"tutorialbackgroundcolor"            | red = "66", green = "66", blue = "66", alpha = "1"
"tutorialcontinuebuttoncolor"        | red = "17", green = "100", blue = "102", alpha = "1"
"tutorialcontinuebuttontext"         | "Continue""Continuar"
"tutorialcontinuebuttontextcolor"    | red = "255", green = "255", blue = "255", alpha = "1"
"tutorialgif"                        | ""
"tutorialtext"                       | "• Make sure that your document does not present shines or shades. • Center your document in the frame. • The picture will be take AUTOMATICALLY.""• Asegúrese de que su documento no muestra brillos o sombras. • Centre su documento en el marco. • La fotografía se capturará de forma AUTOMÁTICA."
"tutorialtextcolor"                  | red = "255", green = "255", blue = "255", alpha = "1"
"tutorialtitle"                      | "Document Capture""Captura Documento"
"tutorialtitlecolor"                 | red = "255", green = "255", blue = "255", alpha = "1"
"userinfo"                           | "Please, check that your document is focused, without cuts and without any kind of brightness that prevents its perfect reading.""Por favor, compruebe que su documento está enfocado, sin cortes y sin ningún tipo de brillo que impida su perfecta lectura."
"validationbackgroundcolor"          | red = "66", green = "66", blue = "66", alpha = "1"

