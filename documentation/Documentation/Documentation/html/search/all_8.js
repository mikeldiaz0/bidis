var searchData=
[
  ['shortdescription',['shortDescription',['../interface_v_d_document.html#a96b3a1069c3dd19ec04443fae27963cb',1,'VDDocument']]],
  ['startwithdelegate_3aanddocumentids_3a',['startWithDelegate:andDocumentIds:',['../interface_v_d_document_capture.html#a75825a8bc2eb96fbe2c7e11fc4935656',1,'VDDocumentCapture']]],
  ['startwithdelegate_3aanddocumentids_3aandconfiguration_3a',['startWithDelegate:andDocumentIds:andConfiguration:',['../interface_v_d_document_capture.html#a1b15aa9237b2613a9e3aa8352d80b3ec',1,'VDDocumentCapture']]],
  ['startwithdelegate_3aanddocuments_3a',['startWithDelegate:andDocuments:',['../interface_v_d_document_capture.html#ac2ad546389cbefe0018cc2ff97bfcd48',1,'VDDocumentCapture']]],
  ['startwithdelegate_3aanddocuments_3aandconfiguration_3a',['startWithDelegate:andDocuments:andConfiguration:',['../interface_v_d_document_capture.html#a823c564a6a8db87ac5b0874b09e2a3fe',1,'VDDocumentCapture']]],
  ['stop',['stop',['../interface_v_d_document_capture.html#aefa403d21289b43caa50896a00e58edc',1,'VDDocumentCapture']]]
];
