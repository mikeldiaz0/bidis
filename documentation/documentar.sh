#!bin/bash
if [[ "$#" -ne 1 ]]; then
  echo "Numero de argumentos inválido. Necesario 1 argumento."
  echo "Uso:" $0 "PathFinal"
  exit -1
fi

READLINK=$(which readlink)
GREADLINK=$(which greadlink)

if [[ -z $GREADLINK ]]; then
  if [[ -z $READLINK ]]; then
    echo "Readlink o greadlink no instalado."
    exit -1
  else
    cd $(dirname $($READLINK -f $0))/Documentation
  fi
else
  cd $(dirname $($GREADLINK -f $0))/Documentation
fi

# comprobaciones de entorno
VERSION=$(/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" ../../VDDocumentCapture/Info.plist )
if [[ $VERSION == "\$(MARKETING_VERSION)" ]]; then
    echo "EQUAL"
    VERSION=`sed -n '/MARKETING_VERSION/{s/MARKETING_VERSION = //;s/;//;s/^[[:space:]]*//;p;q;}' ../../VDDocumentCapture.xcodeproj/project.pbxproj`
fi

RUTAFINAL="$1"
RUTADOCUMENTO=$(pwd)
DOXYGENEXE=$(which doxygen)
LSEXE=$(which ls)
MKDIREXE=$(which mkdir)
RMEXE=$(which rm)
CPEXE=$(which cp)
MVEXE=$(which mv)
LATEXEXE=$(which latex)
FINDEXE=$(which find)
MAKEEXE=$(which make)
CDEXE=$(which cd)
SEDEXE=$(which sed)

# Cambios de versión
PROJECT_NUMBER=$(egrep "^PROJECT_NUMBER*" ./Doxyfile | awk '{ print $3 }')
echo 'La versión anterior es '$PROJECT_NUMBER
echo 'La versión nueva es '$VERSION

$SEDEXE -i '' -e "s/$PROJECT_NUMBER/$VERSION/g" ./Doxyfile #> ./DoxyfileTemp

# comprobaciones de DOXYGEN y LATEX
if [[ -z $DOXYGENEXE ]]; then
  echo "Doxygen NO instalado terminando..."
  exit -1
else
  echo "Doxygen instalado."
fi
if [[ -z $LATEXEXE ]]; then
  echo "Latex NO instalado terminando..."
  exit -1
else
  echo "Latex instalado."
fi
# Comprueba si existe la carpeta de entrega
if $FINDEXE -d "$RUTAFINAL" &> /dev/null; then
  echo "La carpeta de entrega ya existe"
else
  $MKDIREXE "$RUTAFINAL"
fi
# Borra la carpeta de Developer_Documentation si ya está para hacerla de 0
if cd "$RUTAFINAL";then
  if $FINDEXE -f  "$RUTAFINAL""/Developer_Documentation" &> /dev/null;then
    $RMEXE -rf Developer_Documentation
  fi
fi
cd $RUTADOCUMENTO
# generar documentacion
if $DOXYGENEXE Doxyfile &> /dev/null; then
  echo "El DOXYGEN ha finalizado"
else
  echo "El DOXYGEN no ha podido finalizar"
fi
# prepara arbol de carpetas
if $FINDEXE -d Developer_Documentation &> /dev/null;
then
  $RMEXE -rf Developer_Documentation
fi
$MKDIREXE Developer_Documentation/
$MKDIREXE Developer_Documentation/Documentation
$CPEXE -rf Documentation Developer_Documentation/Documentation
cd $RUTADOCUMENTO"/"Developer_Documentation/

$RMEXE -rf Images/
$RMEXE -rf md/

$CPEXE ../../Documentation.html ./
# Genera pdf de latex y lo mueve
cd Documentation/Documentation/latex
if $MAKEEXE -j8 &> /dev/null;
then
  echo "El LATEX ha finalizado"
  $MVEXE refman.pdf ../../../Documentation.pdf
else
  echo "Error al generar pdf"
  exit -1
fi
cd ../
$RMEXE -rf latex
# Mueve la carpeta al destino final
cd $RUTADOCUMENTO
if mv $RUTADOCUMENTO"/Developer_Documentation" "$RUTAFINAL""/Developer_Documentation";then
  echo "TODO TERMINADO CORRECTAMENTE"
  echo "Revise "$RUTAFINAL" para ver la documentación"
else
  echo "Ha habido un error al generar la documentación, revise los pasos"
fi
