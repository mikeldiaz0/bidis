#!/bin/bash

MARKDOWN=markdown
FILES=files
IS_MAC=false

# Entering to correct path
if [ -z $(which greadlink) ]; then
    if [ -z $(which readlink) ]; then
        echo "Imposible to read link"
        exit 1
    else
        cd $(dirname $(readlink -f $0))
    fi
else
    IS_MAC=true
    cd $(dirname $(greadlink -f $0))
fi

# root files
cp $FILES/Changelog.md ../../CHANGELOG.md

# Create forlder if it does not exists
if [ ! -d "$MARKDOWN" ]; then
    mkdir $MARKDOWN
fi

# markdown folder files
cp $FILES/Configuration.md $MARKDOWN/CONFIGURATION.md


if [ "$IS_MAC" = true ]; then
    sed -i '' 's/\/n//g' $MARKDOWN/*.md
    sed -i '' 's/\\n/ /g' $MARKDOWN/*.md
    sed -i '' 's/\\t/ /g' $MARKDOWN/*.md
    sed -i '' 's/<br>/\-/g' $MARKDOWN/*.md
    sed -i '' -e 's/<[^>]*>//g' $MARKDOWN/*.md
else
    sed -i'' 's/\/n//g' $MARKDOWN/*.md
    sed -i'' 's/\\n/ /g' $MARKDOWN/*.md
    sed -i'' 's/\\t/ /g' $MARKDOWN/*.md
    sed -i'' 's/<br>/\-/g' $MARKDOWN/*.md
    sed -i'' -e 's/<[^>]*>//g' $MARKDOWN/*.md
fi

cd -
