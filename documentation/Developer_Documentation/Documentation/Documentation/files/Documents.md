# Documents {#Documents}

- The recognised documents that can be automatically classified are:

  * ES_IDCard_2006: Spanish DNI version 2.0
  * ES_IDCard_2015: Spanish DNI version 3.0
  * ES_ResidencePermit_2010: Spanish NIE version 2.0
  * ES_ResidencePermit_2011: Spanish NIE version 3.0
  * ES_ResidencePermit_2020: Spanish NIE version 4.0
  * MX_IDCard_2008: Mexico IFE  version C
  * MX_IDCard_2014: Mexico IFE version D, E or F
  * MX_IDCard_2019: Mexico IFE version G
  * AR_IDCard_2009: Argentina DNI version 1.0
  * AR_IDCard_2012: Argentina DNI version 2.0
  * PE_IDCard_2007: Peru CUI version 1.0
  * PE_IDCard_2013: Peru CUI  version 2.0
  * PE_IDCard_2019: Peru CUI version 3.0
  * CO_IDCard_2000: Cédula de ciudadanía Colombiana
  * MYS2001: Malaysian IDCard
  * AT_DrivingLicense_2006: Austria DrivingLicense 2006
  * AT_DrivingLicense_2014: Austria DrivingLicense 2014
  * AT_IDCard_2002: Austrian IDCard 2002
  * AT_IDCard_2010: Austria IDCard 2010
\n\n
- There are some documents which image is taken automatically but should be introduced individually:

    * AT_DrivingLicense_2004: Austria DrivingLicense 2004
    * XX_Passport_YYYY: Generic Passport
\n\n
- The detection of any of the following documents is done by a generic TD1 or TD3  format document detector:

    * AD_DrivingLicense_1990: Andorra DrivingLicense 1990
    * AL_IDCard_2009: Albania IDCard 2009
    * AT_ResidencePermit_2005: Austria ResidencePermit 2005
    * AT_ResidencePermit_2011: Austria ResidencePermit 2011
    * AU-ACT_DrivingLicense_2011: Australia Australian Capital Territory DrivingLicense 2011
    * AU-NSW_DrivingLicense_2013: Australia New South Wales DrivingLicense 2013
    * AU-NT_DrivingLicense_2006: Australia Northern Territory DrivingLicense 2006
    * AU-QLD_DrivingLicense_2011: Australia Queensland DrivingLicense 2011
    * AU-QLD_DrivingLicense_2016: Australia Queensland DrivingLicense 2016
    * AU-SA_DrivingLicense_2014: Australia South Australia DrivingLicense 2014
    * AU-TAS_DrivingLicense_2015: Australia Tasmania DrivingLicense 2015
    * AU-VIC_DrivingLicense_2009: Australia Victoria DrivingLicense 2009
    * AU-WA_DrivingLicense_2011: Australia Western DrivingLicense 2011
    * AU-WA_DrivingLicense_2014: Australia Western DrivingLicense 2014
    * BA_IDCard_2003: Bosnia and Herzegovina IDCard 2003
    * BA_IDCard_2013: Bosnia and Herzegovina IDCard 2013
    * BE_DrivingLicense_2010: Belgium DrivingLicense 2010
    * BE_DrivingLicense_2013: Belgium DrivingLicense 2013
    * BE_IDCard_2008: Belgium IDCard 2008
    * BE_IDCard_2010: Belgium IDCard 2010
    * BG_DrivingLicense_2002: Bulgaria DrivingLicense 2002
    * BG_DrivingLicense_2013: Bulgaria DrivingLicense 2013
    * BG_IDCard_2006: Bulgaria IDCard 2000 and 2006
    * BG_IDCard_2010: Bulgaria IDCard 2010
    * BR_DrivingLicense_2017: Brasil DrivingLicense 2017
    * BR_DrivingLicense_2019: Brasil DrivingLicense 2019
    * BR_IDCard_2014: Brasil IDCard 2014
    * BY_DrivingLicense_2010: Belarus DrivingLicense 2010
    * CA-AB_DrivingLicense_2009: Canada Alberta State DrivingLicense 2009
    * CA-BC_DrivingLicense_2013: Canada British Columbia DrivingLicense 2013
    * CA-MB_DrivingLicense_2014: Canada Manitoba DrivingLicense 2014
    * CA-NB_IDCard_2020: Canada New Brunswick IDCard 2014
    * CA-NB_DrivingLicense_2017: Canada New Brunswick DrivingLicense 2014
    * CA-NL_DrivingLicense_2017: Canada Newfoundland and Labrador DrivingLicense 2017
    * CA-NS_DrivingLicense_2017: Canada Nova Scotia DrivingLicense 2017
    * CA-NT_DrivingLicense_2005: Canada Northwest Territories DrivingLicense 2005
    * CA-NU_DrivingLicense_2009: Canada Nunavut DrivingLicense 2009
    * CA-ON_DrivingLicense_2007: Canada Ontario DrivingLicense 2007
    * CA-PE_DrivingLicense_2017: Canada Prince Edward Island DrivingLicense 2017
    * CA-QC_DrivingLicense_2015: Canada Quebec State DrivingLicense 2015
    * CA-SK_DrivingLicense_2016: Canada Saskatchewan State DrivingLicense 2016
    * CA-YT_DrivingLicense_2010: Canada Yukon DrivingLicense 2010
    * CH_DrivingLicense_2003: Switzerland DrivingLicense 2003
    * CH_IDCard_2003: Switzerland IDCard 2003 and 2005
    * CL_IDCard_2002: Chile IDCard 2002
    * CL_IDCard_2013: Chile IDCard 2013
    * CN_IDCard_2004: China IDCard 2004
    * CO_ResidencePermit_2016: Colombian Residence Permit 2016
    * CY_DrivingLicense_2015: Cyprus DrivingLicense 2015
    * CY_IDCard_2008: Cyprus IDCard 2000 and 2008
    * CY_IDCard_2015: Cyprus IDCard 2015
    * CZ_DrivingLicense_2013: Czechia DrivingLicense 2013
    * CZ_IDCard_2003: Czechia IDCard 2003
    * CZ_IDCard_2014: Czechia IDCard 2012 and 2014
    * DE_DrivingLicense_2004: Germany DrivingLicense 2004
    * DE_DrivingLicense_2013: Germany DrivingLicense 2013
    * DE_IDCard_2007: Germany IDCard 2007
    * DE_IDCard_2010: Germany IDCard 2010
    * DK_DrivingLicense_1997: Denmark DrivingLicense 1997
    * DK_DrivingLicense_2013: Denmark DrivingLicense 2013
    * DO_IDCard_1998: Dominican Republic IDCard 1998
    * DO_IDCard_2014: Dominican Republic IDCard 1998
    * EE_DrivingLicense_2004: Estonia DrivingLicense 2004
    * EE_DrivingLicense_2013: Estonia DrivingLicense 2013
    * EE_IDCard_2011: Estonia IDCard 2011
    * ES_DrivingLicense_2004: Spain DrivingLicense 2004
    * ES_DrivingLicense_2013: Spain DrivingLicense 2013
    * FI_DrivingLicense_1992: Finland DrivingLicense 1992
    * FI_DrivingLicense_2010: Finland DrivingLicense 2010
    * FI_DrivingLicense_2013: Finland DrivingLicense 2013
    * FI_IDCard_2011: Finland IDCard 2011
    * FI_IDCard_2017: Finland IDCard 2017
    * FR_DrivingLicense_2013: France DrivingLicense 2013
    * FR_IDCard_1994: France IDCard
    * GB_DrivingLicense_1998: United Kingdom DrivingLicense 1998
    * GB_DrivingLicense_2007: United Kingdom DrivingLicense 2007
    * GB_DrivingLicense_2014: United Kingdom DrivingLicense 2014
    * GB_DrivingLicense_2015: United Kingdom DrivingLicense 2015
    * GB_DrivingLicense-PL_1998: United Kingdom Provisional DrivingLicense 1998
    * GB_DrivingLicense-PL_2007: United Kingdom Provisional DrivingLicense 2007
    * GB_DrivingLicense-PL_2014: United Kingdom Provisional DrivingLicense 2014
    * GB_DrivingLicense-PL_2015: United Kingdom Provisional DrivingLicense 2015
    * GR_DrivingLicense_2013: Greece DrivingLicense 2013
    * GT_IDCard_2009: Guatemala ID Card 2009
    * HR_DrivingLicense_2013: Croatia DrivingLicense 2013
    * HR_IDCard_2003: Croatia IDCard 2003
    * HR_IDCard_2015: Croatia IDCard 2015
    * HU_DrivingLicense_2013: Hungary DrivingLicense 2013
    * HU_IDCard_2000: Hungary IDCard 2000
    * HU_IDCard_2015: Hungary IDCard 2015
    * IE_DrivingLicense_2013: Ireland DrivingLicense 2013
    * IE_Passport_2015: Ireland Passport 2015
    * IS_DrivingLicense_2001: Iceland DrivingLicense 2001
    * IS_DrivingLicense_2013: Iceland DrivingLicense 2013
    * IT_DrivingLicense_2000: Italy DrivingLicense 2000
    * IT_DrivingLicense_2013: Italy DrivingLicense 2013
    * IT_IDCard_2004: Italy IDCard 2004
    * IT_IDCard_2016: Italy IDCard 2016
    * LI_DrivingLicense_2003: Liechtenstein DrivingLicense 2003
    * LI_IDCard_1995: Liechtenstein IDCard 1995
    * LI_IDCard_2009: Liechtenstein IDCard 2009
    * LT_DrivingLicense_2007: Lithuania DrivingLicense 2007
    * LT_DrivingLicense_2016: Lithuania DrivingLicense 2016
    * LT_IDCard_2002: Lithuania IDCard 2002
    * LT_IDCard_2009: Lithuania IDCard 2009 and 2012
    * LU_DrivingLicense_2013: Luxembourg DrivingLicense 2013
    * LU_IDCard_2014: Luxembourg IDCard 2014
    * LV_DrivingLicense_2004: Latvia DrivingLicense 2004
    * LV_DrivingLicense_2013: Latvia DrivingLicense 2013
    * LV_IDCard_2012: Latvia IDCard 2012
    * MC_IDCard_2009: Monaco IDCard 2009
    * MD_IDCard_2015: Moldava IDCard 2015
    * ME_IDCard_2008: Montenegro IDCard 2008
    * MK_IDCard_2007: Macedonia IDCard 2007
    * MT_DrivingLicense_2003: Malta DrivingLicense 2003
    * MT_DrivingLicense_2013: Malta DrivingLicense 2013
    * MT_IDCard_2002: Malta IDCard 2002
    * MT_IDCard_2014: Malta IDCard 2014
    * MY_IDCard_2012: Malaysian IDCard 2012
    * NL_DrivingLicense_2006: Netherlands DrivingLicense 2006
    * NL_DrivingLicense_2013: Netherlands DrivingLicense 2013
    * NL_DrivingLicense_2014: Netherlands DrivingLicense 2014
    * NL_IDCard_2011: Netherlands IDCard
    * NL_IDCard_2014: Netherlands IDCard 2014 and 2017
    * NO_DrivingLicense_1998: Norway DrivingLicense 1998
    * NO_DrivingLicense_2007: Norway DrivingLicense 2004 and 2007
    * NO_DrivingLicense_2013: Norway DrivingLicense 2013
    * NZ_DrivingLicense_2007: New Zealand DrivingLicense 2007
    * PA_IDCard_2010: Panama ID Card 2010
    * PH_DrivingLicense_2017: Philippines DrivingLicense 2017
    * PH_IDCard_2011: Philippines IDCard 2011
    * PH_IDCard_2015: Philippines IDCard 2015
    * PH_IDCard_2016: Philippines IDCard 2016
    * PH_IDCard-PO_2016: Philippines postal IDCard 2016
    * PL_DrivingLicense_1999: Poland DrivingLicense 1999
    * PL_DrivingLicense_2004: Poland DrivingLicense 2004
    * PL_DrivingLicense_2013: Poland DrivingLicense 2013
    * PL_IDCard_2001: Poland IDCard 2001 and 2013
    * PL_IDCard_2015: Poland IDCard 2015
    * PL_IDCard_2019: Poland IDCard 2019
    * PT_DrivingLicense_1999: Portugal DrivingLicense 1999
    * PT_DrivingLicense_2013: Portugal DrivingLicense 2013
    * PT_IDCard_2015: Portugal IDCard
    * PY_IDCard_2007: Paraguay IDCard 2007
    * PY_IDCard_2009: Paraguay IDCard 2009
    * RO_DrivingLicense_2013: Romania DrivingLicense 2013
    * RO_IDCard_2009: Romania IDCard 2009 and 2017
    * RS_IDCard_2008: Serbia IDCard 2008
    * RU_DrivingLicense_2011: Russia Driving License 2011
    * SE_DrivingLicense_2013: Sweden DrivingLicense 2013
    * SE_DrivingLicense_2016: Sweden DrivingLicense 2016
    * SE_IDCard_2012: Sweden IDCard 2012
    * SG_IDCard_2011: Singapore IDCard 2011
    * SI_DrivingLicense_2009: Slovenia DrivingLicense 2009
    * SI_DrivingLicense_2013: Slovenia DrivingLicense 2013
    * SI_IDCard_1998: Slovenia IDCard 1998
    * SK_DrivingLicense_2008: Slovakia DrivingLicense 2008
    * SK_DrivingLicense_2013: Slovakia DrivingLicense 2013
    * SK_IDCard_2015: Slovakia IDCard 2008,2013 and 2015
    * TR_IDCard_2016: Turkey Identity Card 2016
    * UA_IDCard_2016: Ukraine IDCard 2016
    * US-AK_DrivingLicense_2005: United States Alaska DrivingLicense 2005
    * US-AK_DrivingLicense_2014: United States Alaska DrivingLicense 2014
    * US-AK_DrivingLicense_2018: United States Alaska DrivingLicense 2018
    * US-AL_DrivingLicense_2013: United States Alabama DrivingLicense 2013
    * US-AL_IDCard_2013: United States Alabama IDCard 2013
    * US-AR_DrivingLicense_2016: United States Arkansas DrivingLicense 2016
    * US-AR_DrivingLicense_2018: United States Arkansas DrivingLicense 2018
    * US-AZ_DrivingLicense_1990: United States Arizona DrivingLicense 1990
    * US-AZ_DrivingLicense_1996: United States Arizona DrivingLicense 1996
    * US-AZ_DrivingLicense_2004: United States Arizona DrivingLicense 2004
    * US-AZ_DrivingLicense_2016: United States Arizona DrivingLicense 2016
    * US-CA_DrivingLicense_2008: United States California DrivingLicense 2008
    * US-CA_DrivingLicense_2018: United States California DrivingLicense 2018
    * US-CO_DrivingLicense_2011: United States Colorado DrivingLicense 2011
    * US-CO_DrivingLicense_2016: United States Colorado DrivingLicense 2016
    * US-CT_DrivingLicense_2009: United States Connecticut DrivingLicense 2009
    * US-CT_DrivingLicense_2017: United States Connecticut DrivingLicense 2017
    * US-DC_DrivingLicense_2017: United States District of Columbia DrivingLicense 2017
    * US-DE_DrivingLicense_2010: United States Delaware DrivingLicense 2010
    * US-DE_DrivingLicense_2018: United States Delaware DrivingLicense 2018
    * US-FL_DrivingLicense_2010: United States Florida DrivingLicense 2010
    * US-FL_DrivingLicense_2017: United States Florida DrivingLicense 2017
    * US-GA_DrivingLicense_2007: United States Georgia DrivingLicense 2007
    * US-GA_DrivingLicense_2012: United States Georgia DrivingLicense 2012
    * US-HI_DrivingLicense_2012: United States Hawaii DrivingLicense 2012
    * US-HI_DrivingLicense_2018: United States Hawaii DrivingLicense 2018
    * US-IA_DrivingLicense_2013: United States Iowa DrivingLicense 2013
    * US-IA_DrivingLicense_2018: United States Iowa DrivingLicense 2018
    * US-ID_DrivingLicense_2004: United States Idaho DrivingLicense 2004
    * US-ID_DrivingLicense_2010: United States Idaho DrivingLicense 2010
    * US-ID_DrivingLicense_2017: United States Idaho DrivingLicense 2017
    * US-IL_DrivingLicense_2007: United States Illinois DrivingLicense 2007
    * US-IL_DrivingLicense_2016: United States Illinois DrivingLicense 2016
    * US-IN_DrivingLicense_2010: United States Indiana DrivingLicense 2010
    * US-IN_DrivingLicense_2017: United States Indiana DrivingLicense 2017
    * US-KS_DrivingLicense_2004: United States Kansas DrivingLicense 2004
    * US-KS_DrivingLicense_2012: United States Kansas DrivingLicense 2012
    * US-KS_DrivingLicense_2017: United States Kansas DrivingLicense 2017
    * US-KY_DrivingLicense_2012: United States Kentuky DrivingLicense 2012
    * US-KY_DrivingLicense_2019: United States Kentuky DrivingLicense 2019
    * US-LA_DrivingLicense_2011: United States Louisiana DrivingLicense 2011
    * US-LA_DrivingLicense_2014: United States Louisiana DrivingLicense 2014
    * US-LA_DrivingLicense_2016: United States Louisiana DrivingLicense 2016
    * US-MA_DrivingLicense_2010: United States Massachusetts DrivingLicense 2010
    * US-MA_DrivingLicense_2018: United States Massachusetts DrivingLicense 2018
    * US-MD_DrivingLicense_2013: United States Maryland DrivingLicense 2013
    * US-MD_DrivingLicense_2016: United States Maryland DrivingLicense 2016
    * US-ME_DrivingLicense_2011: United States Maine DrivingLicense 2011
    * US-MI_DrivingLicense_2010: United States Michigan DrivingLicense 2010
    * US-MI_DrivingLicense_2017: United States Michigan DrivingLicense 2017
    * US-MN_DrivingLicense_2004: United States Minnesota DrivingLicense 2004
    * US-MN_DrivingLicense_2014: United States Minnesota DrivingLicense 2014
    * US-MN_DrivingLicense_2018: United States Minnesota DrivingLicense 2018
    * US-MO_DrivingLicense_2004: United States Missouri DrivingLicense 2004
    * US-MO_DrivingLicense_2012: United States Missouri DrivingLicense 2012
    * US-MS_DrivingLicense_2001: United States Mississippi DrivingLicense 2001
    * US-MS_DrivingLicense_2017: United States Mississippi DrivingLicense 2017
    * US-MT_DrivingLicense_2000: United States Montana DrivingLicense 2000
    * US-MT_DrivingLicense_2008: United States Montana DrivingLicense 2008
    * US-NC_DrivingLicense_2007: United States North Carolina DrivingLicense 2007
    * US-NC_DrivingLicense_2017: United States North Carolina DrivingLicense 2017
    * US-ND_DrivingLicense_2006: United States North Dakota DrivingLicense 2006
    * US-ND_DrivingLicense_2018: United States North Dakota DrivingLicense 2018
    * US-NE_DrivingLicense_2013: United States Nebraska DrivingLicense 2013
    * US-NE_DrivingLicense_2017: United States Nebraska DrivingLicense 2017
    * US-NH_DrivingLicense_2006: United States New Hampshire DrivingLicense 2006
    * US-NH_DrivingLicense_2017: United States New Hampshire DrivingLicense 2017
    * US-NJ_DrivingLicense_2011: United States New Jersey DrivingLicense 2011
    * US-NM_DrivingLicense_2014: United States New Mexico DrivingLicense 2014
    * US-NM_DrivingLicense_2016: United States New Mexico DrivingLicense 2016
    * US-NV_DrivingLicense_2010: United States Nevada DrivingLicense 2010
    * US-NV_DrivingLicense_2014: United States Nevada DrivingLicense 2014
    * US-NY_DrivingLicense_2017: United States New York DrivingLicense 2017
    * US-OH_DrivingLicense_2013: United States Ohio DrivingLicense 2013
    * US-OH_DrivingLicense_2014: United States Ohio DrivingLicense 2014
    * US-OH_DrivingLicense_2018: United States Ohio DrivingLicense 2018
    * US-OK_DrivingLicense_2013: United States Oklahoma DrivingLicense 2013
    * US-OK_DrivingLicense_2018: United States Oklahoma DrivingLicense 2018
    * US-OR_DrivingLicense_2007: United States Oregon DrivingLicense 2007
    * US-OR_DrivingLicense_2018: United States Oregon DrivingLicense 2018
    * US-PA_DrivingLicense_2011: United States Pennsylvania DrivingLicense 2011
    * US-PA_DrivingLicense_2017: United States Pennsylvania DrivingLicense 2017
    * US-RI_DrivingLicense_2008: United States Rhode Island DrivingLicense 2008
    * US-RI_DrivingLicense_2018: United States Rhode Island DrivingLicense 2018
    * US-SC_DrivingLicense_2011: United States South Carolina DrivingLicense 2011
    * US-SC_DrivingLicense_2018: United States South Carolina DrivingLicense 2018
    * US-SD_DrivingLicense_2009: United States South Dakota DrivingLicense 2009
    * US-SD_DrivingLicense_2010: United States South Dakota DrivingLicense 2010
    * US-TN_DrivingLicense_2003: United States Tennessee DrivingLicense 2003
    * US-TN_DrivingLicense_2012: United States Tennessee DrivingLicense 2012
    * US-TX_DrivingLicense_2016: United States Texas DrivingLicense 2016
    * US-TX_IDCard_2016: United States Texas IDCard 2016
    * US-UT_DrivingLicense_2006: United States Utah DrivingLicense 2006
    * US-UT_DrivingLicense_2016: United States Utah DrivingLicense 2016
    * US-VA_DrivingLicense_2018: United States Virginia DrivingLicense 2018
    * US-VA_IDCard_2018: United States Virginia IDCard 2018
    * US-VT_DrivingLicense_2014: United States Vermont DrivingLicense 2014
    * US-VT_DrivingLicense_2018: United States Vermont DrivingLicense 2018
    * US-WA_DrivingLicense_2010: United States Washington DrivingLicense 2019
    * US-WA_DrivingLicense_2017: United States Washington DrivingLicense 2017
    * US-WA_DrivingLicense_2019: United States Washington DrivingLicense 2019
    * US-WI_DrivingLicense_2005: United States Wisconsin DrivingLicense 2005
    * US-WI_DrivingLicense_2012: United States Wisconsin DrivingLicense 2008
    * US-WI_DrivingLicense_2015: United States Wisconsin DrivingLicense 2015
    * US-WV_DrivingLicense_2005: United States West Virginia DrivingLicense 2005
    * US-WV_DrivingLicense_2013: United States West Virginia DrivingLicense 2013
    * US-WY_DrivingLicense_2014: United States Wyoming DrivingLicense 2014
    * UY_IDCard_1999: Uruguay Identity Card 1999
    * UY_IDCard_2015: Uruguay Identity Card 2015
    * VE_IDCard_2011: Venezuela IDCard 2011
    * XX_XX_XXXX: Undefined documents
