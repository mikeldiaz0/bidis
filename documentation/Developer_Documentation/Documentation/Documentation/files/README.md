# Presentation

The goal of this framework is to capture four document images guiding the user in a self-contained and accessible system.

This SDK captures the document images with the device rear camera.

This SDK retrieves this information:

- Document type.
- Document obverse (front side) image without flash.
- Document obverse image with flash.
- Document reverse (back side) image without flash.

Some permissions are needed and have to be written in the info.plist of the app using the framework:

- Camera.

In order to improve the user experience, the camera takes the document photo automatically (without touching the screen), this is done due to its automatic document recognition.

The SDK can be configured using a dictionary. All possible keys and values can be checked in [Configuration](Configuration.html). See integration example below.

# Specifications

- iOS minimum operating system version: 10.0.
- Flash camera and torch mode can be required for several functionalities.
- SDK size (with simulator/non_simulator): 6.67 / 5.85 MB (aprox.).
- Additionally, some dependencies are needed.
    - Internal dependencies. Total size with these dependencies: 40.96 / 30.82 MB.
        - VDLibrary: 5.93 / 5.30 MB.
        - ImageProcessing: 28.36 / 19.67 MB.

        - Full SDK implementation size with the most common architecture: (All sizes are in MB)

            Architecture  |   VDLibrary  | ImageProcessing  |   SDK  | Total  
            :-------------------:|:------------:|:----------------:|:------:|:------:  
            arm64                |  4.33        | 10.09             |  4.6  | 19.02

            The armv7, i386 and x86_64 architectures are also supported.

    - Pods dependencies.
        - SSZipArchive.

- Image is resized to 8MPx and file size is normally smaller than 1.5MB.

Each mobile device on the market uses a unique software architecture. Thus, each mobile device does NOT install the 40 MB
of the VDLibrary and ImageProcessing libraries, but installs the part of the weight associated with its architecture. In fact,
this process of dividing the weight according to the architecture also takes place when the APP is uploaded to the APP Store.

# Integration
- Create a new Xcode project.
- Add permissions required into Info.plist:
    - Camera Usage.
- Drag and drop all .framework into Xcode and copy inside project.
- Add all .framework into the targets general tab, inside Embedded Binaries and Linked Frameworks and Libraries (if they do not appear yet).
- Install pods in the project and add the following lines in the Podfile for SSZipArchive dependencies,
  ```ruby
  target 'your target' do
      use_frameworks!
      pod 'SSZipArchive', '2.2.3'
  end
  ```
- Import VDDocumentCapture/VDDocumentCapture.h (Objective-C) or VDDocumentCapture (Swift) and implement VDDocumentCaptureProtocol in  the class that uses it.
- Write down all the required delegated methods into the class that uses the framework. Call the methods of the SDK as needed.

# SDK update
Once a new SDK is released the following steps have to be followed to properly update the SDK.
- Remove **all updating .framework** located in the project. Do not remove only the reference; remove the actual file if the deletion is done with the help of Xcode. Just in case, empty the bin too.
- Compile and/or run to force errors. If no error occurs then a cached framework is in memory. Clean cache and project.
- Add **all new .framework** and implement new interfaces if necessary.
- Compile and run.

In case of doubt, also the following can be done.
- Check the installed version by calling the `getVersion()` method.
- Check the md5/rsa values against the ones in the `CHECKSUMS.md` file.

# Example of use

Objective-c

```java
#import "VDDocumentCapture/VDDocumentCapture.h"
@‌interface ViewController () <VDDocumentCaptureProtocol>

- (void) viewDidAppear:(BOOL)animated {

   // The recommended work mode is with document ids, which can be found in the Description section.
   NSMutableArray<NSString *> * documents = [NSMutableArray new];
   [documents addObject:@"ES_IDCard_2006"]; or [documents addObject:VDDOCUMENT_ID_ES_IDCard_2006];
   [documents addObject:@"ES_IDCard_2015"]; or [documents addObject:VDDOCUMENT_ID_ES_IDCard_2015];
   if (![VDDocumentCapture isStarted]) {
      // Here a call with options can be made, if no configuration is set the default values are used. 
      // See below for configuration example.
      [VDDocumentCapture startWithDelegate:self andDocumentIds: documents];
   }
}

// In another place you can stop the process (not recommended).
- (void) stopFramework {
  // Stop it whenever you want.
  [VDDocumentCapture stop];
}

// Protocol methods.
// Called when a document image has been captured
- (void) VDDocumentCaptured:(NSData *) imageData withCaptureType:(VDCaptureType) captureType andDocument:(NSArray<VDDocument *> *) document {
  // Do with image as needed.
}

// Called when the framework has finished.
// processFinished (Boolean) Indicates if the process has finished (true) or has been interrupted (false)
- (void) VDDocumentAllFinished:(Boolean)processFinished {
    // When the framework ends, proceed as needed.
}

// Called when the number of seconds passed without taking the photo.
- (void) VDTimeWithoutPhotoTaken:(int)seconds withCaptureType:(VDCaptureType)capture {
   // Action may be needed.
}
```

Swift

```java
import VDDocumentCapture
class MyClass: VDDocumentCaptureProtocol {
    // PROTOCOL IMPLEMENTATION.
    // Called when a document image has been captured
    func vdDocumentCaptured(_ imageData: Data!, with captureType: VDCaptureType, andDocument document: [VDDocument]!)  {
        // Do with image as needed.
    }
    // Called when the framework has finished.
    // processFinished (Boolean) Indicates if the process has finished (true) or has been interrupted (false)
    func vdDocumentAllFinished(_ processFinished: Bool) {
        // When the framework ends, proceed as needed
    }
    // Called when the number of seconds passed without taking the photo.
    func vdTimeWithoutPhotoTaken(_ seconds: Int32, with capture: VDCaptureType) {
        // Action may be needed.
    }

    ....

   // Call something like this to start the document capture
    let configuration : [String : Any] = [:]
    configuration["closebutton"] = "YES"
    configuration["continuebuttonbackgroundcolor"] = ["red": "255", "green": "0", "blue": "0", "alpha": "1"]

    let documents : [String] = ["ES_IDCard_2006", "ES_IDCard_2015"]
    if !VDDocumentCapture.isStarted() {
        VDDocumentCapture.start(withDelegate: self, andDocumentIds: documents, andConfiguration: configuration)
    }
}
```

# Workflow
The next diagram represents de current workflow of the framework. Some method's names may de shortened due to legibility issues.

![Workflow](workflow.png "Workflow")

# Third party libraries
- OpenCV 4.1.0 ([main page](https://opencv.org/opencv-4-1/). [documentation](https://docs.opencv.org/4.1.0/)). Used for image manipulation.
- SSZipArchive ([source code](https://github.com/ZipArchive/ZipArchive)). Used for zip manipulation.
