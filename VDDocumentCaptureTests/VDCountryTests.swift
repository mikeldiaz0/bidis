//
//  VDCountryTests.swift
//  VDDocumentCaptureTests
//
//  Copyright © 2021 Veridas. All rights reserved.
//

import XCTest
@testable import VDDocumentCapture

class VDCountryTests: XCTestCase {

    func testCountryName() {
        let expected = "name"
        
        let country = VDCountry()
        country.countryName = expected
        
        XCTAssertEqual(expected, country.countryName)
    }
    
    func testCountryToItselfIsEqual() {
        let countryName = "name"

        let country = VDCountry()
        country.countryName = countryName

        XCTAssertEqual(country, country)
    }
    
    func testCountryNotEqual() {
        let countryName = "name"

        let country = VDCountry()
        country.countryName = countryName

        let countryCopy = country.copy() as! VDCountry
        countryCopy.countryName = "no name"

        XCTAssertNotEqual(country.countryName, countryCopy.countryName)
    }
    
    func testCountryIsEqual() {
        let countryName = "name"
        
        let country = VDCountry()
        country.countryName = countryName
        
        let countryCopy = country.copy() as! VDCountry
        
        XCTAssertEqual(country, countryCopy)
    }
    
    func testHash() {
        let countryName = "name"
        
        let country = VDCountry()
        country.countryName = countryName
        
        let countryCopy = country.copy() as! VDCountry
        
        XCTAssertEqual(country.hash, countryCopy.hash)
    }
    
    func testDescription() {
        let expected = "Name: name and enum: 0"
        
        let country = VDCountry()
        country.countryName = "name"
        
        XCTAssertEqual(expected, country.description)
    }
}
