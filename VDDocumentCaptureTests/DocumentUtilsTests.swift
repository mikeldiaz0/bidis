//
//  DocumentUtilsTests.swift
//  VDDocumentCaptureTests
//
//  Copyright © 2021 veridas. All rights reserved.
//

import XCTest
@testable import VDDocumentCapture

class DocumentUtilsTests: XCTestCase {
    let listWithNotchJanuary2021 = ["iPhone10,3",
                                    "iPhone10,6",
                                    "iPhone11,2",
                                    "iPhone11,4",
                                    "iPhone11,6",
                                    "iPhone11,8",
                                    "iPhone12,1",
                                    "iPhone12,3",
                                    "iPhone12,5",
                                    "iPhone12,8",
                                    "iPhone13,1",
                                    "iPhone13,2",
                                    "iPhone13,3",
                                    "iPhone13,4"]
    func testVersionIsNotNil(){
        XCTAssertNotNil(VDDocumentCapture.getVersion())
        XCTAssertNotNil(Bundle(for: VDDocumentCapture.self).infoDictionary?["CFBundleShortVersionString"])
        XCTAssertNotNil(Bundle(for: VDDocumentCapture.self).infoDictionary?["CFBundleVersion"])
    }
    
    func testColorIsValid() {
        let red = ["red": "255", "green": "0", "blue": "0", "alpha": "1"]
        let expected = UIColor.red
        
        XCTAssertEqual(expected, DocumentUtils.transformRGBColor(red))
    }
    
    func testColorIsNotValid() {
        let red = ["red": "lol", "green": "0", "blue": "0", "alpha": "1"]
        let expected = UIColor.red
        
        XCTAssertNotEqual(expected, DocumentUtils.transformRGBColor(red))
    }
    func testInvalidColorIsValid() {
        let red = ["red": "255", "green": "lol", "blue": "0", "alpha": "1"]
        let expected = UIColor.red
        
        XCTAssertEqual(expected, DocumentUtils.transformRGBColor(red))
    }
    
    func testResizeImageBiggerWidth() {
        let expected: CGFloat = 200
        let image = UtilsForTests.getTestImage("test100x100")
        let resizedImage = DocumentUtils.resize(image, toWidth: Float(expected))
        
        XCTAssertEqual(expected, resizedImage!.size.width)
    }
    
    func testResizeImageBiggerHeight() {
        let expected: CGFloat = 200
        let image = UtilsForTests.getTestImage("test100x100")
        let resizedImage = DocumentUtils.resize(image, toHeight: Float(expected))
        
        XCTAssertEqual(expected, resizedImage!.size.height)
    }
    
    func testResizeImageSmallerWidth() {
        let expected: CGFloat = 50
        let image = UtilsForTests.getTestImage("test100x100")
        let resizedImage = DocumentUtils.resize(image, toWidth: Float(expected))
        
        XCTAssertEqual(expected, resizedImage!.size.width)
    }
    
    func testResizeImageSmallerHeight() {
        let expected: CGFloat = 50
        let image = UtilsForTests.getTestImage("test100x100")
        let resizedImage = DocumentUtils.resize(image, toHeight: Float(expected))
        
        XCTAssertEqual(expected, resizedImage!.size.height)
    }
    
    func testResizeImageBiggerSize() {
        let expected: CGSize = CGSize.init(width: 150, height: 150)
        let image = UtilsForTests.getTestImage("test100x100")
        let resizedImage = DocumentUtils.resize(image, toSmallerSize: expected)
        
        XCTAssertNotEqual(expected, resizedImage!.size)
        XCTAssertEqual(image?.size, resizedImage!.size)
    }
    
    func testResizeImageSmallerSize() {
        let expected: CGSize = CGSize.init(width: 50, height: 50)
        let image = UtilsForTests.getTestImage("test100x100")
        let resizedImage = DocumentUtils.resize(image, toSmallerSize: expected)
        
        XCTAssertEqual(expected, resizedImage!.size)
    }
    
    func testComputeDistanceY() {
        let expected: CGFloat = 100
        let origin = CGPoint.init(x: 0, y: 0)
        let end = CGPoint.init(x: 0, y: 100)
        
        XCTAssertEqual(expected, DocumentUtils.computeDistanceBetween(origin, and: end))
    }
    
    func testComputeDistanceX() {
        let expected: CGFloat = 100
        let origin = CGPoint.init(x: 0, y: 0)
        let end = CGPoint.init(x: 100, y: 0)
        
        XCTAssertEqual(expected, DocumentUtils.computeDistanceBetween(origin, and: end))
    }
    
    func testComputeDistanceXY() {
        let expected: CGFloat = 500
        let origin = CGPoint.init(x: 0, y: 0)
        let end = CGPoint.init(x: 300, y: 400)
        
        XCTAssertEqual(expected, DocumentUtils.computeDistanceBetween(origin, and: end))
    }
    
    func testImageFromFilename() {
        let expected = UtilsForTests.getVDDocumentCaptureImage("circle")
        let image = DocumentUtils.getImageFromFilename("circle.png")
        
        XCTAssertTrue(expected!.pngData()!.elementsEqual((image!.pngData())!))
        XCTAssertNil(DocumentUtils.getImageFromFilename("what?"))
    }
    
    func testCheckNotch() {
        let notch = DocumentUtils.checkIfNotchExist()
        let model = UtilsForTests.modelIdentifier()
        
        if listWithNotchJanuary2021.contains(model) {
            XCTAssertTrue(notch)
        } else {
            XCTAssertFalse(notch)
        }
    }
    
    func testCheckNotchSize() {
        let notch = DocumentUtils.getNochtSize()
        let model = UtilsForTests.modelIdentifier()
        
        if listWithNotchJanuary2021.contains(model) {
            XCTAssertTrue(notch > 0.0)
        } else {
            XCTAssertTrue(notch == 0.0)
        }
    }
    
    func testValidPath() {
        XCTAssertNotNil(DocumentUtils.getPathFromFilename("circle.png"))
        XCTAssertNil(DocumentUtils.getPathFromFilename("circle.jpg"))
    }
    
    func testSampleBuffer() {
        let invalidSampleBuffer: CMSampleBuffer? = nil
        XCTAssertNil(DocumentUtils.image(from: invalidSampleBuffer))
        
        let sampleBuffer = UtilsForTests.getCMSampleBuffer()
        XCTAssertNotNil(DocumentUtils.image(from: sampleBuffer))
    }
    
    func testValidStringDoesNotCrashInLabel() {
        XCTAssertNoThrow(DocumentUtils.convert(toHTMLString: "string", in: UILabel()))
    }
    
    func testArrowFrom() {
        let origin = CGPoint(x: -5, y: -4)
        let end = CGPoint(x: 10, y: 10)
        let number = Int32(10)
        XCTAssertNotNil(DocumentUtils.getArrowFrom(origin, to: end, arrowNumber: number))
    }
    
    func testCornerArrowFrom() {
        let origin = CGRect(x: -2, y: -10, width: -10, height: 10)
        let end = CGRect(x: 10, y: 10, width: 10, height: 10)
        let radious = CGFloat(10)
        let color = ArrowColors.init()
        
        XCTAssertNotNil(DocumentUtils.getCornerArrows(from: origin, to: end, withBorderRadius: radious, arrowColors: color))
    }
    
    func testCornerArrowFromOriginInterchanged() {
        let origin = CGRect(x: 10, y: 10, width: 10, height: 10)
        let end = CGRect(x: -10, y: -5, width: 10, height: 10)
        let radious = CGFloat(10)
        let color = ArrowColors.init()
        
        XCTAssertNotNil(DocumentUtils.getCornerArrows(from: origin, to: end, withBorderRadius: radious, arrowColors: color))
    }
}
