//
//  VDDocumentTests.swift
//  VDDocumentCaptureTests
//
//  Copyright © 2021 veridas. All rights reserved.
//

import XCTest
@testable import VDDocumentCapture

class VDDocumentTests: XCTestCase {
    func testDocumentName() {
        let expected = "name"
        
        let document = VDDocument()
        document.documentName = expected
        
        XCTAssertEqual(expected, document.documentName)
    }
    
    func testDocumentToItselfIsEqual() {
        let documentName = "name"

        let document = VDDocument()
        document.documentName = documentName

        XCTAssertEqual(document, document)
    }
    
//    func testDocumentsNotEqual() {
//        let documentName = "name"
//
//        let document = VDDocument()
//        document.documentName = documentName
//
//        let documentCopy = document.copy() as! VDDocument
//        documentCopy.documentName = "no name"
//
//        XCTAssertNotEqual(document.documentName, documentCopy.documentName)
//    }

    func testDocumentIsEqual() {
        let documentName = "name"

        let document = VDDocument()
        document.documentName = documentName

        let documentCopy = document.copy() as! VDDocument

        XCTAssertNotEqual(document, documentCopy)
    }

    func testDescription() {
        let expected = "Name: name, country: country and enum: 0"

        let document = VDDocument()
        document.documentName = "name"
        document.documentCountry = "country"

        XCTAssertEqual(expected, document.description)
    }
}
