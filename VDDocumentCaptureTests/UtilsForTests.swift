//
//  UtilsForTests.swift
//  VDDocumentCaptureTests
//
//  Copyright © 2021 veridas. All rights reserved.
//

import Foundation
@testable import VDDocumentCapture

class UtilsForTests {
    static func getTestImage(_ name: String!) -> UIImage! {
        return UIImage.init(contentsOfFile: Bundle.init(for: self).path(forResource: name, ofType: "jpg")!)!
    }
    
    static func getVDDocumentCaptureImage(_ name: String!) -> UIImage! {
        return UIImage.init(contentsOfFile: Bundle.init(for: VDDocument.self).path(forResource: name, ofType: "png")!)!
    }
    
    static func modelIdentifier() -> String {
        if let simulatorModelIdentifier = ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] { return simulatorModelIdentifier }
        var sysinfo = utsname()
        uname(&sysinfo) // ignore return value
        return String(bytes: Data(bytes: &sysinfo.machine, count: Int(_SYS_NAMELEN)), encoding: .ascii)!.trimmingCharacters(in: .controlCharacters)
    }
    
    static func getCMSampleBuffer() -> CMSampleBuffer {
        var pixelBuffer : CVPixelBuffer? = nil
        CVPixelBufferCreate(kCFAllocatorDefault, 100, 100, kCVPixelFormatType_32BGRA, nil, &pixelBuffer)

        var info = CMSampleTimingInfo()
        info.presentationTimeStamp = CMTime.zero
        info.duration = CMTime.invalid
        info.decodeTimeStamp = CMTime.invalid


        var formatDesc: CMFormatDescription? = nil
        CMVideoFormatDescriptionCreateForImageBuffer(allocator: kCFAllocatorDefault, imageBuffer: pixelBuffer!, formatDescriptionOut: &formatDesc)

        var sampleBuffer: CMSampleBuffer? = nil

        CMSampleBufferCreateReadyWithImageBuffer(allocator: kCFAllocatorDefault,
                                                 imageBuffer: pixelBuffer!,
                                                 formatDescription: formatDesc!,
                                                 sampleTiming: &info,
                                                 sampleBufferOut: &sampleBuffer);

        return sampleBuffer!
    }
}
