//
//  BidiCodeReaderTests.swift
//  VDDocumentCaptureTests
//
//  Copyright © 2020 das-nano. All rights reserved.
//

import XCTest
@testable import VDDocumentCapture
import VDLibrary

class BidiCodeReaderTests: XCTestCase
{
    /* BIDI CODE LIST: Colombia, mexico2008, mexico2019 , texas, argentina, georgia, canada, peru2007, florida, california, massachusetts, newzeland, nueva york */

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    override func setUp() {
        super.setUp()
    }
    
    
    func testSearchForBidiCodesInImageEmpty()throws
    {
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [], tryharder: true)
        bidiReader.searchForBidiCodes(in: getImageWithName(name: ""))
        XCTAssertEqual(bidiReader.bidiTypesToSearch.count,0)
        XCTAssertEqual(bidiReader.combinationsToSearch.count,0)
        XCTAssertEqual(bidiReader.bidiCodesRead.count, 0)
    }
    
    
    //MARK: Check documents set
    func testSetDocumentsWithNoBidiCodesDoesNotInitializeVariables()
    {
        //GIVEN Bidi Reader
        //WHEN setting documents with no Bidi codes
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_ES_IDCard_2006, VDDOCUMENT_ID_ES_IDCard_2015], tryharder: true)
        
        //THEN variables are not set
        XCTAssertEqual(bidiReader.bidiTypesToSearch.count,0)
        XCTAssertEqual(bidiReader.combinationsToSearch.count,0)
        XCTAssertEqual(bidiReader.bidiCodesRead.count, 0)
       // XCTAssertTrue(bidiReader.bidiCodesRead.count == 0)
        XCTAssertTrue(bidiReader.allCodesRead())
    }
    
    func testSetColombiaDocuments() throws
    {
        //GIVEN Bidi Reader
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_CO_IDCard_2000], tryharder: true)
        
        //THEN variables are correctly set
        XCTAssertEqual(bidiReader.bidiTypesToSearch , [4])
        XCTAssertEqual(bidiReader.bidiCodesRead , [[]])
        XCTAssertEqual(bidiReader.combinationsToSearch , [[1]])
    }
    
    func testSetMexicoDocuments() throws
    {
        //GIVEN Bidi Reader
        //WHEN setting Mexico document
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_MX_IDCard_2008, VDDOCUMENT_ID_MX_IDCard_2014], tryharder: true)
        
        //THEN variables are correctly set
        XCTAssertEqual(bidiReader.bidiTypesToSearch , [1, 4, 5])
        XCTAssertEqual(bidiReader.bidiCodesRead , [[], [], []])
        XCTAssertEqual(bidiReader.combinationsToSearch , [[1,1,0], [1,1,1]])
    }
    
    func testSetMexico2019Documents() throws
    {
        //Creo que no esta bien definido deberían ser 3 QR viendo la imagen
        
        //GIVEN Bidi Reader
        //WHEN setting Mexico document
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_MX_IDCard_2019], tryharder: true)
        
        //THEN variables are correctly set
        XCTAssertEqual(bidiReader.bidiTypesToSearch , [1, 5])
        XCTAssertEqual(bidiReader.bidiCodesRead , [[], []])
        XCTAssertEqual(bidiReader.combinationsToSearch , [[1,3]])
    }
    
    func testSetTexasDocuments() throws
    {
        //GIVEN Bidi Reader
        //WHEN setting TExas document
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_US_TX_DrivingLicense_2016], tryharder: true)
        
        //THEN variables are correctly set
        XCTAssertEqual(bidiReader.bidiTypesToSearch , [1, 4])
        XCTAssertEqual(bidiReader.bidiCodesRead , [[], []])
        XCTAssertEqual(bidiReader.combinationsToSearch , [[1,1]])
    }
    
    func testSetArgentinaDocuments() throws
    {
        //GIVEN Bidi Reader
        //WHEN setting Argentina document
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_AR_IDCard_2009, VDDOCUMENT_ID_AR_IDCard_2012], tryharder: true)
        
        //THEN variables are correctly set
        XCTAssertEqual(bidiReader.bidiTypesToSearch , [4])
        XCTAssertEqual(bidiReader.bidiCodesRead , [[]])
        XCTAssertEqual(bidiReader.combinationsToSearch , [[1]])
    }
    
    func testSetGeorgiaDocuments() throws
    {
        //GIVEN Bidi Reader
        //WHEN setting Georgia document
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_US_GA_DrivingLicense_2012], tryharder: true)
        
        //THEN variables are correctly set
        XCTAssertEqual(bidiReader.bidiTypesToSearch , [1,4])
        XCTAssertEqual(bidiReader.bidiCodesRead , [[],[]])
        XCTAssertEqual(bidiReader.combinationsToSearch , [[1,1]])
    }
    
    func testSetCanadaDocuments() throws
    {
        //GIVEN Bidi Reader
        //WHEN setting Canada document
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_CA_ON_DrivingLicense_2007], tryharder: true)
        
        //THEN variables are correctly set
        XCTAssertEqual(bidiReader.bidiTypesToSearch , [0,4])
        XCTAssertEqual(bidiReader.bidiCodesRead , [[],[]])
        XCTAssertEqual(bidiReader.combinationsToSearch , [[1,1]])
    }
    
    
    func testSetPeruDocuments() throws
    {
        //GIVEN Bidi Reader
        //WHEN setting Peru document
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_PE_IDCard_2007, VDDOCUMENT_ID_PE_IDCard_2013, VDDOCUMENT_ID_PE_IDCard_2013], tryharder: true)
        
        //THEN variables are correctly set
        XCTAssertEqual(bidiReader.bidiTypesToSearch , [0, 1, 4])
        XCTAssertEqual(bidiReader.bidiCodesRead , [[], [], []])
        XCTAssertEqual(bidiReader.combinationsToSearch , [[0,1,1], [1,0,0]])
       // XCTAssertTrue(bidiReader.combinationsToSearch == [[0,1,1], [1,0,0]])
    }
    
    func testSetFloridaDocuments() throws
    {
        //GIVEN Bidi Reader
        //WHEN setting Florida document
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_US_FL_DrivingLicense_2010, VDDOCUMENT_ID_US_FL_DrivingLicense_2017], tryharder: true)
        
        //THEN variables are correctly set
        XCTAssertEqual(bidiReader.bidiTypesToSearch , [1, 4])
        XCTAssertEqual(bidiReader.bidiCodesRead , [[], []])
        XCTAssertEqual(bidiReader.combinationsToSearch , [[1,1]])
    }
    
    func testSetCaliforniaDocuments() throws
    {
        //GIVEN Bidi Reader
        //WHEN setting California document
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_US_CA_DrivingLicense_2008, VDDOCUMENT_ID_US_CA_DrivingLicense_2018], tryharder: true)
        
        //THEN variables are correctly set
        XCTAssertEqual(bidiReader.bidiTypesToSearch , [1, 4])
        XCTAssertEqual(bidiReader.bidiCodesRead , [[], []])
        XCTAssertEqual(bidiReader.combinationsToSearch , [[1,1]])
    }
    
    func testSetMassachusettsDocuments() throws
    {
        //GIVEN Bidi Reader
        //WHEN setting Massachusetts document
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_US_MA_DrivingLicense_2018], tryharder: true)
        
        //THEN variables are correctly set
        XCTAssertEqual(bidiReader.bidiTypesToSearch , [1, 4])
        XCTAssertEqual(bidiReader.bidiCodesRead , [[], []])
        XCTAssertEqual(bidiReader.combinationsToSearch , [[1,1]])
    }
    
    func testSetNewzelandDocuments() throws
    {
        //GIVEN Bidi Reader
        //WHEN setting Newzeland document
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_NZ_DrivingLicense_2007], tryharder: true)
        
        //THEN variables are correctly set
        XCTAssertEqual(bidiReader.bidiTypesToSearch , [1])
        XCTAssertEqual(bidiReader.bidiCodesRead , [[]])
        XCTAssertEqual(bidiReader.combinationsToSearch , [[1]])
    }
    
    func testSetNewYorkDocuments() throws
    {
        //GIVEN Bidi Reader
        //WHEN setting Newzeland document
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_US_NY_DrivingLicense_2008, VDDOCUMENT_ID_US_NY_DrivingLicense_2017], tryharder: true)
        
        //THEN variables are correctly set
        XCTAssertEqual(bidiReader.bidiTypesToSearch , [1,4])
        XCTAssertEqual(bidiReader.bidiCodesRead , [[],[]])
        XCTAssertEqual(bidiReader.combinationsToSearch , [[1,1]])
    }
    
    //MARK: Check read codes
    func testSearchBidiCodesInGeorgiaDocument() throws
    {
        //GIVEN expected Georgia Id
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_US_GA_DrivingLicense_2012], tryharder: true)
        //WHEN reading Georgia Id
        bidiReader.searchForBidiCodes(in: getImageWithName(name: "reverse_georgia_150.jpg"))
        
        //THEN codes CODE128 & PDF417 are read and process is finished
        XCTAssertEqual(bidiReader.bidiCodesRead.count, 2)
        XCTAssertTrue((bidiReader.bidiCodesRead[0] as AnyObject).count == 1)
        XCTAssertTrue((bidiReader.bidiCodesRead[1] as AnyObject).count == 1)
        XCTAssertEqual(bidiReader.bidiTypesToSearch, [1,4])
        XCTAssertEqual(((bidiReader.allResults[0] as! ValiDasBidiResult).bidiType as BidiType) , PDF417)
        XCTAssertTrue((bidiReader.allResults[0] as! ValiDasBidiResult).bidiString.count > 0)
        XCTAssertEqual(((bidiReader.allResults[1] as! ValiDasBidiResult).bidiType as BidiType) , CODE128)
        XCTAssertTrue((bidiReader.allResults[1] as! ValiDasBidiResult).bidiString.count > 0)
        XCTAssertTrue(bidiReader.allCodesRead())
    }
    
    func testSearchBidiCodesInTexasDocument() throws
    {
        //GIVEN expected Texas Id
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_US_TX_DrivingLicense_2016], tryharder: true)
        //WHEN reading Texas Id
        //works with reverse_texas2
        bidiReader.searchForBidiCodes(in: getImageWithName(name: "reverse_texas_50.jpg"))
        
        //THEN codes CODE128 & PDF417 are read and process is finished
        XCTAssertEqual(bidiReader.bidiCodesRead.count , 2)
        XCTAssertEqual((bidiReader.bidiCodesRead[0] as AnyObject).count , 1)
        XCTAssertEqual((bidiReader.bidiCodesRead[1] as AnyObject).count , 1)
        XCTAssertEqual(bidiReader.bidiTypesToSearch , [1,4])
        XCTAssertEqual(((bidiReader.allResults[0] as! ValiDasBidiResult).bidiType as BidiType) , PDF417)
        XCTAssertTrue((bidiReader.allResults[0] as! ValiDasBidiResult).bidiString.count > 0)
        XCTAssertEqual(((bidiReader.allResults[1] as! ValiDasBidiResult).bidiType as BidiType) , CODE128)
        XCTAssertTrue((bidiReader.allResults[1] as! ValiDasBidiResult).bidiString.count > 0)
        
        XCTAssertTrue(bidiReader.allCodesRead())
    }
    
    func testSearchBidiCodesInColombiaDocument() throws
    {
        //GIVEN expected Colombia Id
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_CO_IDCard_2000], tryharder: true)
        //WHEN reading Colombia Id
        bidiReader.searchForBidiCodes(in: getImageWithName(name: "reverse_colombia_50.jpg"))
        
        //THEN code PDF417 are read and process is finished
        XCTAssertEqual(bidiReader.bidiCodesRead.count , 1)
        XCTAssertEqual((bidiReader.bidiCodesRead[0] as AnyObject).count , 1)
        XCTAssertEqual(bidiReader.bidiTypesToSearch , [4])
        XCTAssertEqual(((bidiReader.allResults[0] as! ValiDasBidiResult).bidiType as BidiType) , PDF417)
        XCTAssertTrue((bidiReader.allResults[0] as! ValiDasBidiResult).bidiString.count > 0)
    
        XCTAssertTrue(bidiReader.allCodesRead())
    }
    
    func testSearchBidiCodesInArgentinaDocument() throws
    {
        //GIVEN expected Argentina Ids
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_AR_IDCard_2009, VDDOCUMENT_ID_AR_IDCard_2012], tryharder: true)
    
        //WHEN reading Argentina Id
        bidiReader.searchForBidiCodes(in: getImageWithName(name: "reverse_argentina_150.jpg"))
        waitForCompletion(timeout: 0.1)

        //THEN code PDF417 is read and process is finished
        XCTAssertEqual(bidiReader.bidiCodesRead.count , 1)
        XCTAssertEqual((bidiReader.bidiCodesRead[0] as AnyObject).count , 1)
        XCTAssertEqual(bidiReader.bidiTypesToSearch , [4])
        XCTAssertEqual(((bidiReader.allResults[0] as! ValiDasBidiResult).bidiType as BidiType) , PDF417)
        XCTAssertTrue((bidiReader.allResults[0] as! ValiDasBidiResult).bidiString.count > 0)
        XCTAssertTrue(bidiReader.allCodesRead())
    }
    
    func testSearchBidiCodesInFrontArgentinaDocument() throws
    {
        //GIVEN expected Argentina Ids
        let bidiReader = getBidiReaderForDocumentsInFront(documents: [VDDOCUMENT_ID_AR_IDCard_2009, VDDOCUMENT_ID_AR_IDCard_2012], tryharder: true)
    
        //WHEN reading Argentina Id
        bidiReader.searchForBidiCodes(in: getImageWithName(name: "front_argentina_100.jpg"))
        waitForCompletion(timeout: 0.1)

        //THEN code PDF417 is read and process is finished
        XCTAssertEqual(bidiReader.bidiCodesRead.count , 1)
        XCTAssertEqual((bidiReader.bidiCodesRead[0] as AnyObject).count , 1)
        XCTAssertEqual(bidiReader.bidiTypesToSearch , [4])
        XCTAssertEqual(((bidiReader.allResults[0] as! ValiDasBidiResult).bidiType as BidiType) , PDF417)
        XCTAssertTrue((bidiReader.allResults[0] as! ValiDasBidiResult).bidiString.count > 0)
        XCTAssertTrue(bidiReader.allCodesRead())
    }
    
    func testSearchBidiCodesInCanadaDocument() throws
    {
        //GIVEN expected Canada Id
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_CA_ON_DrivingLicense_2007], tryharder: true)

        //WHEN reading Canada Id
        bidiReader.searchForBidiCodes(in: getImageWithName(name: "reverse_canada_50.jpg"))
        waitForCompletion(timeout: 0.1)

        //THEN codes CODE39 & PDF417 are read and process is finished
        XCTAssertEqual(bidiReader.bidiCodesRead.count , 2)
        XCTAssertEqual((bidiReader.bidiCodesRead[0] as AnyObject).count , 1)
        XCTAssertEqual((bidiReader.bidiCodesRead[1] as AnyObject).count , 1)
        XCTAssertEqual(bidiReader.bidiTypesToSearch , [0,4])
        XCTAssertEqual(((bidiReader.allResults[0] as! ValiDasBidiResult).bidiType as BidiType) , PDF417)
        XCTAssertTrue((bidiReader.allResults[0] as! ValiDasBidiResult).bidiString.count > 0)
        XCTAssertEqual(((bidiReader.allResults[1] as! ValiDasBidiResult).bidiType as BidiType) , CODE39)
        XCTAssertTrue((bidiReader.allResults[1] as! ValiDasBidiResult).bidiString.count > 0)
        XCTAssertTrue(bidiReader.allCodesRead())
    }
    
    func testSearchBidiCodesInPeruDocument() throws
    {
        //Creo que no esta bien definido,en la imagen veo 3 códigos pero me dice que tendría que leer 3.
        
        //GIVEN expected Peru Ids
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_PE_IDCard_2007, VDDOCUMENT_ID_PE_IDCard_2013], tryharder: true)
        bidiReader.searchForBidiCodes(in: getImageWithName(name: "reverse_peru_2007_150.jpg"))
        waitForCompletion(timeout: 0.1)
        
      /*  let img:UIImage = getImageWithName(name: "reverse_peru_50.jpg");
        //Quiero empezar con un width de 650 y llegar hasta 3500 mas o meno
        let div = img.size.width / 650;
        var width = img.size.width/div;
        var height = img.size.height/div;
        
        for _ in 1...5{
            let cg = CGSize(width: width, height:height);
            let img_resized = resizeImage(image: img,targetSize: cg);
            
            bidiReader.searchForBidiCodes(in: img_resized!)
            
            width = width * 1.2;
            height = height * 1.2;
        }*/

        
        //THEN codes CODE128 & PDF417 are read and process is finished
        XCTAssertEqual(bidiReader.bidiCodesRead.count , 3)
        XCTAssertEqual((bidiReader.bidiCodesRead[0] as AnyObject).count , 1)
        XCTAssertEqual((bidiReader.bidiCodesRead[1] as AnyObject).count , 0)
        XCTAssertEqual((bidiReader.bidiCodesRead[2] as AnyObject).count , 1)
        XCTAssertEqual(bidiReader.bidiTypesToSearch , [0,1,4])
        XCTAssertEqual(((bidiReader.allResults[0] as! ValiDasBidiResult).bidiType as BidiType) , CODE128)
        XCTAssertTrue((bidiReader.allResults[0] as! ValiDasBidiResult).bidiString.count > 0)
        XCTAssertEqual(((bidiReader.allResults[1] as! ValiDasBidiResult).bidiType as BidiType) , CODE39)
        XCTAssertTrue((bidiReader.allResults[1] as! ValiDasBidiResult).bidiString.count > 0)
        XCTAssertEqual(((bidiReader.allResults[2] as! ValiDasBidiResult).bidiType as BidiType) , PDF417)
        XCTAssertTrue((bidiReader.allResults[2] as! ValiDasBidiResult).bidiString.count > 0)
        XCTAssertTrue(bidiReader.allCodesRead())
    }
    
    func testSearchBidiCodesInMexico2008Document() throws
    {
        //GIVEN expected Mexico Ids
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_MX_IDCard_2008, VDDOCUMENT_ID_MX_IDCard_2014], tryharder: true)
        //WHEN reading Mexico Id
        bidiReader.searchForBidiCodes(in: getImageWithName(name: "reverse_mexico_2008b.jpg"))
        
        //Code 128 && PDF417
        XCTAssertEqual(bidiReader.bidiCodesRead.count , 3)
        
        XCTAssertEqual((bidiReader.bidiCodesRead[0] as AnyObject).count , 1)
        XCTAssertEqual((bidiReader.bidiCodesRead[1] as AnyObject).count , 1)
        XCTAssertEqual(bidiReader.bidiTypesToSearch , [1,4,5])
        
        XCTAssertEqual(((bidiReader.allResults[0] as! ValiDasBidiResult).bidiType as BidiType) , PDF417)
        XCTAssertTrue((bidiReader.allResults[0] as! ValiDasBidiResult).bidiString.count > 0)
        XCTAssertEqual(((bidiReader.allResults[1] as! ValiDasBidiResult).bidiType as BidiType) , CODE128)
        XCTAssertTrue((bidiReader.allResults[1] as! ValiDasBidiResult).bidiString.count > 0)
    }
    
    
    func testSearchBidiCodesInMexico2020Document() throws
    {
        //GIVEN expected Mexico Ids
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_MX_IDCard_2019], tryharder: true)
        //WHEN reading Mexico Id
        bidiReader.searchForBidiCodes(in: getImageWithName(name: "reverse_mexico_2020.jpg"))
        
        XCTAssertEqual(bidiReader.bidiCodesRead.count , 2)
        
        XCTAssertEqual((bidiReader.bidiCodesRead[0] as AnyObject).count , 0)
        XCTAssertEqual((bidiReader.bidiCodesRead[1] as AnyObject).count , 2)
        XCTAssertEqual(bidiReader.bidiTypesToSearch , [1,5])
        
        XCTAssertEqual(((bidiReader.allResults[0] as! ValiDasBidiResult).bidiType as BidiType) , QR)
        XCTAssertTrue((bidiReader.allResults[0] as! ValiDasBidiResult).bidiString.count > 0)
        XCTAssertEqual(((bidiReader.allResults[1] as! ValiDasBidiResult).bidiType as BidiType) , QR)
        XCTAssertTrue((bidiReader.allResults[1] as! ValiDasBidiResult).bidiString.count > 0)
    }
    
    
    func testSearchBidiCodesInCaliforniaDocument() throws
    {
        //NO detecta ninguno de los 2 códigos bidis, no se por qué
        //GIVEN expected California Ids
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_US_CA_DrivingLicense_2008, VDDOCUMENT_ID_US_CA_DrivingLicense_2018], tryharder: true)
        //WHEN reading California Id
        bidiReader.searchForBidiCodes(in: getImageWithName(name: "reverse_california2.jpg"))
        
        //THEN codes CODE128 & PDF417 are read
        XCTAssertEqual(bidiReader.bidiCodesRead.count , 2)
        XCTAssertEqual((bidiReader.bidiCodesRead[0] as AnyObject).count , 1)
        XCTAssertEqual((bidiReader.bidiCodesRead[1] as AnyObject).count , 1)
        XCTAssertEqual(bidiReader.bidiTypesToSearch , [1,4])
        
        XCTAssertEqual(((bidiReader.allResults[0] as! ValiDasBidiResult).bidiType as BidiType) , CODE128)
        XCTAssertTrue((bidiReader.allResults[0] as! ValiDasBidiResult).bidiString.count > 0)
        XCTAssertEqual(((bidiReader.allResults[1] as! ValiDasBidiResult).bidiType as BidiType) , PDF417)
        XCTAssertTrue((bidiReader.allResults[1] as! ValiDasBidiResult).bidiString.count > 0)
    }
    
    func testSearchBidiCodesInNewZelandDocument() throws
    {
        //GIVEN expected New Zeland Id
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_NZ_DrivingLicense_2007], tryharder: true)
        //WHEN reading New Zeland Id
        bidiReader.searchForBidiCodes(in: getImageWithName(name: "reverse_newzeland_50.jpg"))
        
        //THEN code CODE128 is read and process is finished
        XCTAssertEqual(bidiReader.bidiCodesRead.count , 1)
        XCTAssertEqual((bidiReader.bidiCodesRead[0] as AnyObject).count , 1)
        XCTAssertEqual(bidiReader.bidiTypesToSearch , [1])
        XCTAssertEqual(((bidiReader.allResults[0] as! ValiDasBidiResult).bidiType as BidiType) , CODE128)
        XCTAssertTrue((bidiReader.allResults[0] as! ValiDasBidiResult).bidiString.count > 0)
        
        XCTAssertTrue(bidiReader.allCodesRead())
    }

    func testSearchBidiCodesInFloridaDocument() throws
    {
        //No entiendo por qué no detecta el codigo 128 , debería de leerlo rápido
        //GIVEN expected Florida Ids
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_US_FL_DrivingLicense_2010, VDDOCUMENT_ID_US_FL_DrivingLicense_2017], tryharder: true)
       
        let img:UIImage = getImageWithName(name: "reverse_florida5.jpg");
        //Quiero empezar con un width de 650 y llegar hasta 3500 mas o menos
        let div = img.size.width / 650;
        var width = img.size.width/div;
        var height = img.size.height/div;
        
        for _ in 1...5{
            let cg = CGSize(width: width, height:height);
            let img_resized = resizeImage(image: img,targetSize: cg);
            
            bidiReader.searchForBidiCodes(in: img_resized!)
            
            width = width * 1.2;
            height = height * 1.2;
        }
        
        
        //THEN codes CODE128 & PDF417 are read and process is finished
        XCTAssertEqual(bidiReader.bidiCodesRead.count , 2)
        XCTAssertEqual((bidiReader.bidiCodesRead[0] as AnyObject).count , 1)
        XCTAssertEqual((bidiReader.bidiCodesRead[1] as AnyObject).count , 1)
        XCTAssertEqual(bidiReader.bidiTypesToSearch , [1,4])
        XCTAssertEqual(((bidiReader.allResults[1] as! ValiDasBidiResult).bidiType as BidiType) , CODE128)
        XCTAssertTrue((bidiReader.allResults[1] as! ValiDasBidiResult).bidiString.count > 0)
        XCTAssertEqual(((bidiReader.allResults[0] as! ValiDasBidiResult).bidiType as BidiType) , PDF417)
        XCTAssertTrue((bidiReader.allResults[0] as! ValiDasBidiResult).bidiString.count > 0)
        XCTAssertTrue(bidiReader.allCodesRead())
    }
    
    func testSearchBidiCodesInMassachusettsDocument() throws
    {
        //No entiendo por qué no detecta el codigo 128 , debería de leerlo rápido
        //GIVEN expected Massachusetts Id
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_US_MA_DrivingLicense_2018], tryharder: true)
        let img:UIImage = getImageWithName(name: "reverse_massachusetts.jpg");
        
        //Quiero empezar con un width de 650 y llegar hasta 3500 mas o menos
        let div = img.size.width / 650;
        var width = img.size.width/div;
        var height = img.size.height/div;
        
        for _ in 1...5{
            let cg = CGSize(width: width, height:height);
            let img_resized = resizeImage(image: img,targetSize: cg);
            
            bidiReader.searchForBidiCodes(in: img_resized!)
            if (bidiReader.allResults.count == bidiReader.bidiCodesRead.count){
                break
            }
            width = width * 1.2;
            height = height * 1.2;
        }
        
        //THEN codes CODE128 & PDF417 are read and process is finished
        XCTAssertEqual(bidiReader.bidiCodesRead.count , 2)
        XCTAssertEqual((bidiReader.bidiCodesRead[0] as AnyObject).count , 1)
        XCTAssertEqual(bidiReader.bidiTypesToSearch , [1,4])
        XCTAssertEqual(((bidiReader.allResults[0] as! ValiDasBidiResult).bidiType as BidiType) , CODE128)
        XCTAssertTrue((bidiReader.allResults[0] as! ValiDasBidiResult).bidiString.count > 0)
        XCTAssertEqual(((bidiReader.allResults[1] as! ValiDasBidiResult).bidiType as BidiType) , PDF417)
        XCTAssertTrue((bidiReader.allResults[1] as! ValiDasBidiResult).bidiString.count > 0)
        XCTAssertTrue(bidiReader.allCodesRead())
    }
    
    func testSearchBidiCodesInNewYorkDocument() throws
    {
        //No entiendo por qué no detecta el codigo 128 , debería de leerlo rápido
        //GIVEN expected Florida Ids
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_US_NY_DrivingLicense_2008, VDDOCUMENT_ID_US_NY_DrivingLicense_2017], tryharder: true)
        
        let img:UIImage = getImageWithName(name: "reverse_ny2.jpg");
        
        //Quiero empezar con un width de 650 y llegar hasta 3500 mas o menos
        let div = img.size.width / 650;
        var width = img.size.width/div;
        var height = img.size.height/div;
        
        for _ in 1...5{
            let cg = CGSize(width: width, height:height);
            let img_resized = resizeImage(image: img,targetSize: cg);
            
            bidiReader.searchForBidiCodes(in: img_resized!)
            if (bidiReader.allResults.count == bidiReader.bidiCodesRead.count){
                break
            }
            width = width * 1.2;
            height = height * 1.2;
        }
        
        //Tiene que detectar el codigo code128 el pdf417 no se reconoce con el escaner normal de vision.
        //con reverse_ny.png no detecta, pero con reverse_ny2 si.
        
        //THEN codes CODE128 & PDF417 are read and process is finished
        XCTAssertEqual(bidiReader.bidiCodesRead.count , 2)
        XCTAssertEqual((bidiReader.bidiCodesRead[0] as AnyObject).count , 1)
        XCTAssertEqual(bidiReader.bidiTypesToSearch , [1,4])
        XCTAssertEqual(((bidiReader.allResults[0] as! ValiDasBidiResult).bidiType as BidiType) , PDF417)
        XCTAssertTrue((bidiReader.allResults[0] as! ValiDasBidiResult).bidiString.count > 0)
        XCTAssertEqual(((bidiReader.allResults[1] as! ValiDasBidiResult).bidiType as BidiType) , CODE128)
        XCTAssertTrue((bidiReader.allResults[1] as! ValiDasBidiResult).bidiString.count > 0)
        XCTAssertTrue(bidiReader.allCodesRead())
    }
    
    //MARK: Check complete process
   /* func testReadingAllCaBidiCodesWhenCheckingIfAllAreReadReturnsTrue() throws
    {
        //GIVEN expected California Ids
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_US_CA_DrivingLicense_2008, VDDOCUMENT_ID_US_CA_DrivingLicense_2018], tryharder: true)
        //WHEN reading California Id
        bidiReader.searchForBidiCodes(in: getImageWithName(name: "reverse_california.jpg"))
        
        //THEN process is finished
        XCTAssertTrue(bidiReader.allCodesRead())
    }
    
    func testReadingPartOfMxBidiCodesWhenCheckingIfAllAreReadReturnsFalse() throws
    {
        //GIVEN expected Mexico Ids
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_MX_IDCard_2008, VDDOCUMENT_ID_MX_IDCard_2014], tryharder: true)
        //WHEN reading Mexico 2008 Id (CODE128 & PDF417)
        bidiReader.searchForBidiCodes(in: getImageWithName(name: "reverse_mexico_2008.jpg"))
        
        //THEN in first search it must return not finished because it could be MX_2013 (CODE128 & PDF417 & QR)
        XCTAssertFalse(bidiReader.allCodesRead())
    }

    func testNotReadingAllBidiCodesWhenCheckingIfAllAreReadReturnsFalse() throws
    {
        //GIVEN expected Mexico Ids
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_MX_IDCard_2008, VDDOCUMENT_ID_MX_IDCard_2014], tryharder: true)
        //WHEN reading Peru Id
        bidiReader.searchForBidiCodes(in: getImageWithName(name: "reverse_peru_2007.jpg"))
        
        //THEN process is not finished
        XCTAssertFalse(bidiReader.allCodesRead())
    }*/
    
    func testReadingNotExpectedFormatReturnsFalse() throws
    {
        //GIVEN expected PDF417 code in VDDOCUMENT_ID_US_OH_DrivingLicense_2018
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_US_OH_DrivingLicense_2018], tryharder: true)
        //WHEN reading a code128 not expected
        bidiReader.searchForBidiCodes(in: getImageWithName(name: "code128.jpg"))
        
        //THEN app does not crash, does not save the read code and return false
        XCTAssertFalse(bidiReader.allCodesRead())
        XCTAssertTrue(bidiReader.bidiCodesRead.count == 1)
        XCTAssertTrue((bidiReader.bidiCodesRead[0] as AnyObject).count == 0)
    }
    
    func testReadingObverseWithNoBidiCodesReturnsSearchFinished() throws
    {
        //GIVEN California Ids obverse codes
        let bidiReader = getBidiReaderForDocuments(documents: [VDDOCUMENT_ID_US_CA_DrivingLicense_2008, VDDOCUMENT_ID_US_CA_DrivingLicense_2018], tryharder: true, side: OBVERSE)
        //WHEN reading reverse image
        bidiReader.searchForBidiCodes(in: getImageWithName(name: "reverse_california.jpg"))
        
        //THEN app does not crash, does not save the read code and return false
        XCTAssertTrue(bidiReader.allCodesRead())
        XCTAssertTrue(bidiReader.bidiTypesToSearch == [])
        XCTAssertTrue(bidiReader.combinationsToSearch == nil)
        XCTAssertTrue(bidiReader.bidiCodesRead == nil)
    }

    //MARK: Check if process is finished
    func testCheckFinishSearchWithUniquePossibleResult() throws {
        //GIVEN expected combinations
        let bidiReader = BidiReaderController.init()
        bidiReader.combinationsToSearch = [[1, 1, 0], [1, 1, 1], [0, 3, 1], [1, 2, 0]]
        
        //WHEN reading a unique combination and checking allCodesRead()
        bidiReader.bidiTypesToSearch = [1, 4, 5]
        bidiReader.bidiCodesRead = [[], ["res1", "res2", "res3"], ["type5"]] // [0, 3, 1]
        bidiReader.searchForBidiCodes(in: UIImage.init())
        
        //THEN app does not crash, does not save the read code and return false
        XCTAssertTrue(bidiReader.allCodesRead())
        XCTAssertTrue(bidiReader.combinationsToSearch == [[0, 3, 1]])
    }

    func testCheckFinishSearchWithMultiplePossibleResults() throws {
        //GIVEN expected combinations
        let bidiReader = BidiReaderController.init()
        bidiReader.combinationsToSearch = [[1, 1, 0], [1, 1, 1], [0, 3, 1], [1, 2, 0]]
        
        //WHEN reading a unique combination and checking allCodesRead()
        bidiReader.bidiTypesToSearch = [1, 4, 5]
        bidiReader.bidiCodesRead = [["res1"], ["res2"], []] // [1, 1, 0]
        bidiReader.searchForBidiCodes(in: UIImage.init())
        
        //THEN app does not crash, does not save the read code and return false
        XCTAssertFalse(bidiReader.allCodesRead())
        XCTAssertTrue(bidiReader.combinationsToSearch == [[1, 1, 0], [1, 1, 1], [1, 2, 0]])
    }
    
    func testCheckFinishSearchWithTwoPossibleResults() throws {
        //GIVEN expected combinations
        let bidiReader = BidiReaderController.init()
        bidiReader.combinationsToSearch = [[1, 1, 0], [1, 1, 1], [0, 3, 1], [1, 2, 0]]
        
        //WHEN reading a unique combination and checking searchFinished
        bidiReader.bidiTypesToSearch = [1, 4, 5]
        bidiReader.bidiCodesRead = [[], ["res1", "res2"], []] // [0, 2, 0]
        bidiReader.searchForBidiCodes(in: UIImage.init())
        
        //THEN app does not crash, does not save the read code and return false
        XCTAssertFalse(bidiReader.allCodesRead())
        XCTAssertTrue(bidiReader.combinationsToSearch == [[0, 3, 1], [1, 2, 0]])
    }
    
    func testCheckFinishSearchWithOnePossibleResultAndDifferentCodeReadsReturnsFalse() throws {
        //GIVEN expected combinations
        let bidiReader = BidiReaderController.init()
        bidiReader.combinationsToSearch = [[2]]
           
        //WHEN reading a unique combination and checking allCodesRead()
        bidiReader.bidiTypesToSearch = [4]
        bidiReader.bidiCodesRead = [["res1"]] // [1]
        bidiReader.searchForBidiCodes(in: UIImage.init())
           
        //THEN app does not crash, does not save the read code and return false
        XCTAssertFalse(bidiReader.allCodesRead())
        XCTAssertTrue(bidiReader.combinationsToSearch == [[2]])
    }

    
    //MARK: Performance
    func testPerformanceSetDocs() throws {
        let bidiReader = BidiReaderController.init()
        
        self.measure {
            let docs = getArrayOfDocumentsForIds(ids: [VDDOCUMENT_ID_US_CA_DrivingLicense_2008, VDDOCUMENT_ID_US_CA_DrivingLicense_2018])
            bidiReader.setBidiCodesForDocuments(getBidiCodesToReadFor(documents: docs, side: REVERSE))
        }
    }

    func testPerformanceSearchProcessWithTryHarder() throws {
        let bidiReader = getBidiReaderForDocumentsInReverse(documents: [VDDOCUMENT_ID_US_CA_DrivingLicense_2008, VDDOCUMENT_ID_US_CA_DrivingLicense_2018], tryharder: true)
        
        self.measure {
            bidiReader.searchForBidiCodes(in: getImageWithName(name: "reverse_california.jpg"))
        }
    }
    
    
    func testCheckStartWithDelegate() throws {
        let bidiReader = BidiReaderController.init()
        bidiReader.setSearchFinishedValue(true);
        print(bidiReader.setSearchFinished());
       // if (bidiReader.setSearchFinished()){
        //    XCTAssertTrue(bidiReader.setSearchFinished())
       // }
    }
    
    
    //MARK: Helpers
    func getBidiReaderForDocuments(documents: [String], tryharder: Bool, side: SideType) -> BidiReaderController
    {
        let bidiReader = BidiReaderController.init()
        let docs = getArrayOfDocumentsForIds(ids: documents)
        bidiReader.setBidiCodesForDocuments(getBidiCodesToReadFor(documents: docs, side: side))
        bidiReader.setConfiguration(["biditryharder": tryharder ? "YES" : "NO"])
        
        return bidiReader
    }
    
    func getBidiReaderForDocumentsInReverse(documents: [String], tryharder: Bool) -> BidiReaderController
    {
        let bidiReader = BidiReaderController.init()
        let docs = getArrayOfDocumentsForIds(ids: documents)
        bidiReader.setBidiCodesForDocuments(getBidiCodesToReadFor(documents: docs, side: REVERSE))
        bidiReader.setConfiguration(["biditryharder": tryharder ? "YES" : "NO"])
        
        return bidiReader
    }
    
    func getBidiReaderForDocumentsInFront(documents: [String], tryharder: Bool) -> BidiReaderController
    {
        let bidiReader = BidiReaderController.init()
        let docs = getArrayOfDocumentsForIds(ids: documents)
        bidiReader.setBidiCodesForDocuments(getBidiCodesToReadFor(documents: docs, side: OBVERSE))
        bidiReader.setConfiguration(["biditryharder": tryharder ? "YES" : "NO"])
        
        return bidiReader
    }
    
    func getBidiCodesToReadFor(documents: [ValiDasDocumento], side: SideType) -> [[ValiDasBidiCode]]
    {
        var allBidiCodes = [[ValiDasBidiCode]]()
        
        for doc in documents {
            let bidiCodes = ValiDas.getBidiCodes(forDocument: doc.type, side: side)
                        
            if bidiCodes != nil {
                allBidiCodes.append(bidiCodes!)
            }
        }
        
        return allBidiCodes;
    }
    
    func getImageWithName(name: String) -> UIImage
    {
        let currentBundle = Bundle.init(for: object_getClass(self)!)
        return UIImage.init(contentsOfFile: currentBundle.path(forResource: name, ofType: nil) ?? "") ?? UIImage.init()
    }
    
    func getArrayOfDocumentsForIds(ids: [String]) -> [ValiDasDocumento]
    {
        var docs = [ValiDasDocumento]()
        
        for docId in ids {
            let doc = ValiDasDocumento.init()
            doc.type = docId
            docs.append(doc)
        }
        
        return docs
    }
    
    func waitForCompletion(timeout: TimeInterval)
    {
        let timeoutDate = NSDate(timeIntervalSinceNow: timeout)
        RunLoop.current.run(until: timeoutDate as Date)
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage? {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        }
        
        let rect = CGRect(origin: .zero, size: newSize)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
  
}
