//
//  VDDocumentDBTests.swift
//  VDDocumentCaptureTests
//
//  Copyright © 2021 veridas. All rights reserved.
//

import XCTest
@testable import VDDocumentCapture

class VDDocumentDBTests: XCTestCase {
    func testDatabaseIsNotEmpty() {
        let database = VDDocumentDB.createDB()
        
        XCTAssertNotNil(database)
    }
    
    func testOriginalDbIsNotEmpty() {
        let database = VDDocumentDB.createOriginalDB()
        
        XCTAssertNotNil(database)
    }
    
    func testDocumentExists() {
        XCTAssertNotNil(VDDocumentDB.getDocumentFrom("ES_IDCard_2015"))
    }
    
    func testDocumentDoesNotExist() {
        XCTAssertNil(VDDocumentDB.getDocumentFrom("ES_IDCard_2016")?.documentName)
    }
    
    func testDocumentHasReverse() {
        XCTAssertTrue(VDDocumentDB.someDocumentHasReverse(["ES_IDCard_2015", "Passport"]))
    }
    
    func testDocumentDoesNotHaveReverse() {
        XCTAssertFalse(VDDocumentDB.someDocumentHasReverse(["Passport"]))
    }
}
